﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Mitsubishi.UI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        
        [Required(ErrorMessage ="Vui lòng nhập Email!")]
        [RegularExpression(@"^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", ErrorMessage ="Email không đúng định dạng!")]
        public string Email { get; set; }
        
        public string FullName { get; set; }
        
        public string Address { get; set; }


        public string role { get; set; }

    
    }

    public class RegisterModel: ApplicationUser
    {
        [NotMapped]
        [Required(ErrorMessage ="Vui lòng nhập mật khẩu!")]
        [MinLength(8,ErrorMessage ="Mật khẩu tối thiểu 8 kí tự")]
        public string RegisterPassword { get; set; }
    }

    public class EditUserModel 
    {
        public string Id { get; set; }
        public  string UserName { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập Email!")]
        [RegularExpression(@"^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", ErrorMessage = "Email không đúng định dạng!")]
        public string Email { get; set; }

        public string FullName { get; set; }

        public string Address { get; set; }


        public string role { get; set; }

       
        [MinLength(8, ErrorMessage = "mat khau tu 8 - 20 ktu tu")]
        [MaxLength(20, ErrorMessage = "mat khau tu 8 - 20 ktu tu")]
        public string EditPassword { get; set; }
    }


    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {

            //public DbSet<model> models { get; set; }
        }
    }
}