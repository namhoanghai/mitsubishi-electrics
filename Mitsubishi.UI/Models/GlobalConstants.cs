﻿namespace Mitsubishi.UI.Models
{

    public class GlobalConstants
    {
        public const int COMMAND_TIMEOUT = 600;
        public const string LANGUAGE_VN = "vi-VN";
        public const string LANGUAGE_EN = "en-US";

        public const string SESS_LANGUAGE = "Sess_Language";
        public const string SESS_ADMIN_LANGUAGE = "Sess_AdminLanguage";
        public const string SESS_PRODUCT_VIEWED = "Sess_ProductViewed";
        public const string SESS_PRODUCT_CURRENT = "Sess_ProductCurrent";

        public const string APPLICATION_LOG = "MitsubishiLogger";
        public const string LIST_PRODUCT_TYPE = "ListProductType";
        public const string LIST_PRODUCT_CATEGORY = "ListProductCategory";
        public const string LIST_CONTENT_CATEGORY = "ListContentCategory";
        public const string LIST_ROOM_AIR_CONDITIONER = "ListRoomAirConditioner"; //Điều hòa không khí dân dụng
        public const string LIST_PACKAGE_AIR_CONDITIONER = "ListPackageAirConditioners"; //Điều hòa không khí cục bộ công nghiệp: Mr. SLIM
        public const string LIST_VRF = "ListVariableRefriegantFlowSystem"; //Điều hòa không khí trung tâm (VRF) – City Multi        
        public const string LIST_REFRIGERATOR = "ListRefrigerator"; //Tủ lạnh
        public const string LIST_ELECTRICFANS = "ListElectricFans"; //Quạt điện
        public const string LIST_VENTILATOR = "ListVentilator"; //Thông gió
        public const string LIST_JETTOWEL = "ListJetTowel";
        public const string LIST_LOSSNAY = "ListLossnay";
        public const string LIST_MULTISPLIT = "ListMultiSplit";
        public const string LIST_FACTORY_AUTOMATION = "ListFactoryAutomation"; //Hệ thống tự động hóa công nghiệp

        public const string USER_LOGIN = "UserLogin";
        public const string USER_ID = "UserID";

        public const int PRODUCT_TYPE_HOME_APPLIANCES = 1;
        public const int PRODUCT_TYPE_AIR_CONDITIONER = 12;
        public const int PRODUCT_TYPE_FACTORY_AUTOMATION = 13;

        public const int CATEGORY_AIR_CONDITIONER = 12;
        public const int CATEGORY_FACTORY_AUTOMATION = 13;

        public const string PRODUCT_LINE_ID = "14,2,4,3,43,5,110,105,123,125,107,17,18,19,20,165";

        public const int NAV_HOME = 1;
        public const int NAV_PRODUCT = 2;
        public const int NAV_COMPANY = 3;
        public const int NAV_CUSTOMER_SERVICE = 4;
        public const int NAV_ABOUT_MITSU = 5;
        public const int NAV_ABOUT_US = 6;
        public const int NAV_NEWS_EVENT = 7;

        public const int NAV_INQUIRY = 9;
        public const int NAV_WHERETOBUY = 10;
        public const int NAV_CONTACT_US = 13;

        public const string CACHED_NAV_SUB = "Cached_Nav_Sub_";
        public const string CACHED_LOCALIZATION = "Cached_Localization";
        public const string SESS_PRODUCT_IMAGES = "Sess_ProductImages";
    }
}