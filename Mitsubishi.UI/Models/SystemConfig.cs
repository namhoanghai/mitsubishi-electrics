﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mitsubishi.UI.Models
{
    [Table("SystemConfig")]
    public class SystemConfig
    {
        [Key]
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
        public Nullable<int> SortOrder { get; set; }
    }
}