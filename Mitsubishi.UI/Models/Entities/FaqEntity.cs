﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class FaqEntity
    {
        [DisplayName("ItemID")]
        public int ItemID { get; set; }


        [DisplayName("Question")]
        public string Question { get; set; }

        [DisplayName("Answer")]
        public string Answer { get; set; }

        public int Order { get; set; }

        [DisplayName("IsActive")]
        public bool IsActive { get; set; }

        [DisplayName("Name")]
        public string Name { set; get; }

        [DisplayName("Email")]
        public string Email { set; get; }

        [DisplayName("CreateDate")]
        public DateTime CreateDate { set; get; }

       
    }
}