﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class VideoEntity
    {
        [DisplayName("ItemID")]
        public int ItemID { get; set; }
        

        [DisplayName("Title")]
        public string Title { get; set; }


        [DisplayName("Description")]
        public string Description { get; set; }

        public int Order { get; set; }

        [DisplayName("Thumbnail")]
        public string Thumbnail { set; get; }

        [DisplayName("Link Video")]
        public string LinkVideo { set; get; }


         [DisplayName("IsActive")]
        public bool IsActive { get; set; }
       

        public VideoEntity()
        {
            Title = string.Empty;
            Description = string.Empty;
            LinkVideo = string.Empty;
            Order = 1;
            IsActive = true;
        }
    }
}