﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class ProductTypeEntity
    {
        public int ItemID { set; get; }
        public string DataTextField { set; get; }

        [DisplayName("Name")]
        public string TitleEN { set; get; }

        [DisplayName("Name")]
        public string Title { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Is Active")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }

        [DisplayName("Image")]
        public string ImageName { set; get; }

        [DisplayName("List Imagess")]
        public List<string> ListImages { set; get; }

        public string ImagesContent { set; get; }
    }
}