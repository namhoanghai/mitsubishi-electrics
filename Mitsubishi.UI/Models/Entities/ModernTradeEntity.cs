﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class ModernTradeEntity
    {
        public int MtID { get; set; }
        [DisplayName("ModernTrade")]
        public string ModernTrade { get; set; }
        [DisplayName("Image")]
        public string Image { get; set; }
       
    }
}