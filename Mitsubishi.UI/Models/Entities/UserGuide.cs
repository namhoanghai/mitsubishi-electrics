﻿using System.ComponentModel;

namespace Mitsubishi.UI.Models.Entities
{
    public class UserGuide
    {
        public int ItemID { set; get; }

        [DisplayName("Product Type")]
        public int ProductTypeID { set; get; }

        [DisplayName("Product Category")]
        public int ProductCategoryID { set; get; }

        [DisplayName("Product Serie")]
        public int ProductSerieID { set; get; }

        [DisplayName("Model")]
        public int ModelID { set; get; }

        [DisplayName("Model Name")]
        public string ModelName { set; get; }

        [DisplayName("File Names")]
        public string FileNames { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }
    }
}