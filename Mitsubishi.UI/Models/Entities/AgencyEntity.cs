﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class AgencyEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Location")]
        public int LocationID { set; get; }

        public LocationEntity Location { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Address")]
        public string Address { set; get; }

        [DisplayName("Phone")]
        public string Phone { set; get; }

        [DisplayName("Logo")]
        public string Logo { set; get; }

        [DisplayName("Product Type")]
        public int ProductTypeID { set; get; }

        [DisplayName("Product Category")]
        public int ProductCategoryID { set; get; }

        public string ProductCategory { set; get; }
        public List<ListItemEntity> ListProductCategory { set; get; }
            
        [DisplayName("Website")]
        public string Website { set; get; }

        [DisplayName("Is Authorized")]
        public bool IsAuthorized { set; get; }

        public Double Longitude { set; get; }
        public Double Latitude { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Map")]
        public bool IsMap { set; get; }

        [DisplayName("Delivery")]
        public bool IsDelivery { set; get; }

        [DisplayName("Accept payment card")]
        public bool IsPaymentCard { set; get; }

        [DisplayName("Warranty")]
        public bool IsWarranty { set; get; }

        [DisplayName("Car parking")]
        public bool IsCarParking { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }

       

        public AgencyEntity()
        {
            IsDelivery = true;
            IsPaymentCard = true;
            IsWarranty = true;
            IsCarParking = true;
            Logo = string.Empty;
            IsAuthorized = false;
            IsMap = false;
            SortOrder = 1;
            IsActive = true;
            ListProductCategory = new List<ListItemEntity>();
            Location = new LocationEntity();
            LocationID = 0;
            //Cata = 1;
            
        }
    }
}