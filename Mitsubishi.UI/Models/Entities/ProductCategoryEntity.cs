﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class ProductCategoryEntity
    {
        public int ItemID { set; get; }
        public string DataTextField { set; get; }

        [DisplayName("Product Code")]
        public string ProductCode { set; get; }

        [DisplayName("Product Type")]
        public int ProductTypeId { set; get; }

        [DisplayName("Group")]
        public int GroupID { set; get; }

        [DisplayName("Title")]
        public string TitleEN { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Parent Category")]
        public int ParentID { set; get; }

        public string IconName { set; get; }

        [DisplayName("Icon 2")]
        public string IconFooter { set; get; }

        [DisplayName("Thumbnail")]
        public string ImageName { set; get; }

        [DisplayName("Brochure Name")]
        public string BrochureFileName { set; get; }

        [DisplayName("Specifications")]
        public string Specifications { set; get; }

        [DisplayName("Technical")]
        public string TechnicalDescription { set; get; }

        [DisplayName("Slide Images")]
        public string SlideImages { set; get; }

        [DisplayName("List Imagess")]
        public List<string> ListImages { set; get; }

        [DisplayName("Slide Product Images")]
        public string SlideProductImages { set; get; }

         [DisplayName("List Product Images")]
        public List<string> ListProductImages { set; get; }
        

        [DisplayName("Banners")]
        public string Banners { set; get; }

        [DisplayName("Allow show banner")]
        public bool IsAllowShowBanner { set; get; }

        public List<string> ListBanners { set; get; }


        [DisplayName("LinkDienMayXanh")]
        public string LinkDienMayXanh { set; get; }

        [DisplayName("LinkNguyenKim")]
        public string LinkNguyenKim { set; get; }

        [DisplayName("LinkChoLon")]
        public string LinkChoLon { set; get; }

        [DisplayName("LinkVideo")]
        public string LinkVideo { set; get; }

        [DisplayName("ThumbVideo")]
        public string ThumbVideo { set; get; }

        [DisplayName("LinkThienHoa")]
        public string LinkThienHoa { set; get; }
        
        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }

        [AllowHtml]
        [DisplayName("Long Description")]
        public string LongDescriptionEN { set; get; }

        [AllowHtml]
        [DisplayName("Spec summary")]
        public string SpecSummary { set; get; }

        [AllowHtml]
        [DisplayName("Spec summary")]
        public string SpecSummaryEN { set; get; }

        [AllowHtml]
        [DisplayName("Long Description")]
        public string LongDescription { set; get; }

        [AllowHtml]
        [DisplayName("Short Description")]
        public string ShortDescriptionEN { set; get; }

        [AllowHtml]
        [DisplayName("Short Description")]
        public string ShortDescription { set; get; }

        public ProductTypeEntity ProductType { set; get; }
        public ProductCategoryEntity ParentCategory { set; get; }
        public List<ProductColorEntity> Colors { set; get; }

        [DisplayName("Allow Show Index")]
        public bool IsAllowShowIndex { set; get; }
        public string RelatedTo { set; get; }
        
        public List<ProductCategoryEntity> ListRelated { set; get; }
        public string SubModel { set; get; }
        public List<ProductModelEntity> ListSubModel { set; get; } 
        public IEnumerable<int> ListRelatedId { set; get; }
        public bool IsSubModel { set; get; }
        public string RedirectUrl { set; get; }

        [DisplayName("Link Banners")]
        public string LinkBanners { set; get; }

        public List<BannerEntity> ListLinkBanners { set; get; }
            
        [DisplayName("Email Support")]
        public string EmailSupport { set; get; }

        [DisplayName("Meta Key word")]
        public string MetaKeyword { set; get; }

        [DisplayName("Meta Description")]
        public string MetaDescription { set; get; }


        [DisplayName("Price")]
        public string Price { set; get; }

        [DisplayName("Promotion")]
        public string Promotion { set; get; }
        [DisplayName("Custome Fields")]
        public List<CustomField> CustomFields { get; set; }
        public List<ProductMerchantEntity> Merchants { get; set; }
        
        public ProductCategoryEntity()
        {
            IsAllowShowBanner = true;
            IsAllowShowIndex = true;
            ProductType = new ProductTypeEntity();
            ParentCategory = null;
            ParentID = 0;
            Banners = string.Empty;
            ListBanners = new List<string>();
            Title = string.Empty;
            TitleEN = string.Empty;
            ShortDescription = string.Empty;
            ShortDescriptionEN = string.Empty;
            LongDescription = string.Empty;
            LongDescriptionEN = string.Empty;
            TechnicalDescription = string.Empty;
            SpecSummary = string.Empty;
            SpecSummaryEN = string.Empty;

            ProductTypeId = 0;
            IsActive = true;
            SortOrder = 1;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            Colors = new List<ProductColorEntity>();
            SlideImages = string.Empty;
            ListImages = new List<string>();

            SlideProductImages = string.Empty;
            ListProductImages = new List<string>();
            
            IsAllowShowIndex = true;
            RelatedTo = string.Empty;
            ListRelated = null;
            ListSubModel = new List<ProductModelEntity>();
            GroupID = 1;
            IsSubModel = false;
            RedirectUrl = string.Empty;
            EmailSupport = string.Empty;
            ListLinkBanners = new List<BannerEntity>();

            LinkDienMayXanh = string.Empty;
            LinkNguyenKim = string.Empty;
            LinkThienHoa = string.Empty;
            LinkChoLon = string.Empty;

            LinkVideo = string.Empty;
            ThumbVideo = string.Empty;

            Price = string.Empty;
            Promotion = string.Empty;
            CustomFields = new List<CustomField>();
            Merchants = new List<ProductMerchantEntity>();
        }
    }
}