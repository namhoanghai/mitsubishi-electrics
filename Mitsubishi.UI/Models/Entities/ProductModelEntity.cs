﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class ProductModelEntity
    {
        public int ItemID { set; get; }
        public string DataTextField { set; get; }

        [DisplayName("Product Code")]
        public string ProductCode { set; get; }

        [DisplayName("Product Type")]
        public int ProductTypeId { set; get; }

        [DisplayName("Group")]
        public int GroupID { set; get; }

        [DisplayName("Title")]
        public string TitleEN { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Parent Category")]
        public int ParentID { set; get; }

        public string IconName { set; get; }

        [DisplayName("Icon 2")]
        public string IconFooter { set; get; }

        [DisplayName("Thumbnail")]
        public string ImageName { set; get; }

        [DisplayName("Brochure Name")]
        public string BrochureFileName { set; get; }

        [DisplayName("Specitifications")]
        public string Specifications { set; get; }

        [DisplayName("Slide Images")]
        public string SlideImages { set; get; }

        public List<string> ListImages { set; get; }

        [DisplayName("Banners")]
        public string Banners { set; get; }

        [DisplayName("Allow show banner")]
        public bool IsAllowShowBanner { set; get; }

        public List<string> ListBanners { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }

        [AllowHtml]
        [DisplayName("Long Description")]
        public string LongDescriptionEN { set; get; }

        [AllowHtml]
        [DisplayName("Long Description")]
        public string LongDescription { set; get; }

        [AllowHtml]
        [DisplayName("Short Description")]
        public string ShortDescriptionEN { set; get; }

        [AllowHtml]
        [DisplayName("Short Description")]
        public string ShortDescription { set; get; }

        public List<ProductColorEntity> Colors { set; get; }

        public ProductModelEntity()
        {
            IsAllowShowBanner = true;
            ParentID = 0;
            Banners = string.Empty;
            ListBanners = new List<string>();
            Title = string.Empty;
            TitleEN = string.Empty;
            ShortDescription = string.Empty;
            ShortDescriptionEN = string.Empty;
            LongDescription = string.Empty;
            LongDescriptionEN = string.Empty;
            ProductTypeId = 0;
            IsActive = true;
            SortOrder = 1;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            SlideImages = string.Empty;
            ListImages = new List<string>();
            GroupID = 1;
            Colors = new List<ProductColorEntity>();
        }
    }
}