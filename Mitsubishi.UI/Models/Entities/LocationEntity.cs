﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mitsubishi.UI.Models.Entities
{
    public class LocationEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        public DateTime? ModifiedDate { set; get; }

        public LocationEntity()
        {
            Title = string.Empty;
            SortOrder = 1;
            IsActive = false;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
        }
    }
}