﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class LastIDsEntity
    {
        public int Id { set; get; }

        [DisplayName("TableName")]
        public string TableName { set; get; }

        [DisplayName("LastID")]
        public int LastID { set; get; }
    }
}