﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class ManualCategoryEntity
    {
        public int ItemID { set; get; }
        public string DataTextField { set; get; }

        [DisplayName("Level")]
        public int Level { set; get; }

        [DisplayName("Parent Category")]
        public int ParentID { set; get; }

        [DisplayName("TaxonomyId")]
        public int TaxonomyId { set; get; }

        [DisplayName("Title")]
        public string TitleEN { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Thumbnail")]
        public string Thumbnail { set; get; }
       
        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

       
        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }      
       
        [DisplayName("Meta Key word")]
        public string MetaKeyword { set; get; }

        [DisplayName("Meta Description")]
        public string MetaDescription { set; get; }

        public ManualCategoryEntity ParentCategory { set; get; }

        public ManualCategoryEntity()
        {
            
            ParentID = 0;
            Title = string.Empty;
            TitleEN = string.Empty;
            Level = 0;
            IsActive = true;
            SortOrder = 1;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            ParentCategory = null;
            Thumbnail = string.Empty;
            TaxonomyId = 1;
        }
    }
}