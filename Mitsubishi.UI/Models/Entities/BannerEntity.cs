﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class BannerEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Banner")]
        public string ImageName { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        public DateTime? ModifiedDate { set; get; }

        [DisplayName("Enable Link")]
        public bool IsEnableLink { set; get; }

        [DisplayName("Redirect Url")]
        public string RedirectUrl { set; get; }

        public BannerEntity()
        {
            Title = string.Empty;
            ImageName = string.Empty;
            SortOrder = 1;
            IsActive = true;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            IsEnableLink = false;
            RedirectUrl = string.Empty;
        }
    }
}