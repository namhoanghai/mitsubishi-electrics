﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class InquiryEntity
    {
        public int ItemID { set; get; }

        public string Name { set; get; }

        public string Company { set; get; }

        public string Comment { set; get; }

        public string Address { set; get; }
        public string City { set; get; }
        public string Country { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }

    
        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        public InquiryEntity()
        {
            Name = string.Empty;
            Company = string.Empty;
            Comment = string.Empty;
            Address = string.Empty;
            City = string.Empty;
            Country = string.Empty;
            Phone = string.Empty;
            Email = string.Empty;
            CreatedDate = DateTime.Now;
        }
    }
}