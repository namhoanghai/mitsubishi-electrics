﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mitsubishi.UI.Models.Entities
{
    public class LocalizationEntity
    {
        public int ItemID { set; get; }
        public string LanguageID { set; get; }
        public string ItemKey { set; get; }
        public string Title { set; get; }
    }
}