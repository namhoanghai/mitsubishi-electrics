﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class ContentCategoryEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Title")]
        public string TitleEN { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Sort #")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }

        [DisplayName("Parent Category")]
        public int ParentID { set; get; }

        public string DataTextField { set; get; }

        public ContentCategoryEntity()
        {
            Title = string.Empty;
            TitleEN = string.Empty;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            ParentID = 0;
        }
    }
}