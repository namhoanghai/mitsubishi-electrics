﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Mitsubishi.UI.Models.Entities
{
    public class ProductColorEntity
    {
        public int ItemID { set; get; }
        public int ProductID { set; get; }

        [DisplayName("Color")]
        public string Title { set; get; }

       

        public string TitleEN { set; get; }

        public string Code { set; get; }

        [DisplayName("Images")]
        public string Images { set; get; }
        public List<string> ListImages { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        public ProductColorEntity()
        {
            ListImages = new List<string>();
            Title = string.Empty;
            TitleEN = string.Empty;
            Code = string.Empty;
            SortOrder = 1;
        }
    }
}