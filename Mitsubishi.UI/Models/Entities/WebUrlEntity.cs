﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Mitsubishi.UI.Models.Entities
{
    public class WebUrlEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Banner")]
        public string Url { set; get; }
    }
}