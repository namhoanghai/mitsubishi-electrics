﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class CommentEntity
    {
        public int ItemID { set; get; }

        public int ProductID { set; get; }

      

        [AllowHtml]
        [DisplayName("Content")]
        public string Description { set; get; }

        [DisplayName("Name")]
        public string Name { set; get; }

        [DisplayName("Email")]
        public string Email { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        public CommentEntity()
        {
            ProductID = 0;
            
            Description = string.Empty;
            Name = string.Empty;
            Email = string.Empty;
            IsActive = false;
            CreatedDate = DateTime.Now;
            
        }
    }
}