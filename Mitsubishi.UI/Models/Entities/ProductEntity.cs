﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class ProductEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Category")]
        public int CategoryID { set; get; }

        [DisplayName("Name")]
        public string NameEng { set; get; }

        [DisplayName("Name")]
        public string Name { set; get; }

        [DisplayName("Long Description")]
        public string LongDescriptionEng { set; get; }

        [DisplayName("Long Description")]
        public string LongDescription { set; get; }

        [DisplayName("Short Description")]
        public string ShortDescriptionEng { set; get; }

        [DisplayName("Short Description")]
        public string ShortDescription { set; get; }

        [DisplayName("ImageName")]
        public string ImageName { set; get; }


        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }


        /*
        public ProductEntity()
        {
            ItemID = 0;
            CategoryID = 0;
            NameEng = string.Empty;
            Name = string.Empty;
            LongDescriptionEng = string.Empty;
            LongDescription = string.Empty;
            ShortDescriptionEng = string.Empty;
            ShortDescription = string.Empty;
            ImageName = string.Empty;
            SortOrder = 1;

            IsActive = true;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
        }
         * */
    }

     
}