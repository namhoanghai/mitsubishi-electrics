﻿namespace Mitsubishi.UI.Models.Entities
{
    public class ListItemEntity
    {
        public string DataValueField { set; get; }
        public string DataTextField { set; get; }

        public ListItemEntity()
        {
            DataTextField = string.Empty;
            DataValueField = string.Empty;
        }

        public ListItemEntity(string value, string text)
        {
            DataValueField = value;
            DataTextField = text;
        }
    }
}