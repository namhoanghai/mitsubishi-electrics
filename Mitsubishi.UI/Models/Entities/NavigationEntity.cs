﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class NavigationEntity
    {
        public int ItemID { set; get; }
        public int ParentID { set; get; }

        public string NavigateUrl { set; get; }
        public string BannerImage { set; get; }

        [DisplayName("Title")]
        public string TitleEng { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Is Active")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }
    }
}