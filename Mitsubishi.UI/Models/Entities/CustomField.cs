﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class CustomField
    {
        public int Id { get; set; }
        [DisplayName("Alias")]
        public string Alias { get; set; }
        [DisplayName("Title")]
        public string TitleEN { get; set; }
        [DisplayName("Title English")]
        public string Title { get; set; }
        [DisplayName("Description")]
        public string DescriptionEN { get; set; }
        [DisplayName("Description English")]
        public string Description { get; set; }
        public int Order { get; set; }
        public int ProductId { get; set; }
    }
}