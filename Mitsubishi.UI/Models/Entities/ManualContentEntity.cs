﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class ManualContentEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Title")]
        public string TitleEN { set; get; }

        [AllowHtml]
        [DisplayName("Description")]
        public string ShortDescriptionEN { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [AllowHtml]
        [DisplayName("Description")]
        public string ShortDescription { set; get; }

        [AllowHtml]
        [DisplayName("Content")]
        public string LongDescription { set; get; }

        [AllowHtml]
        [DisplayName("Content")]
        public string LongDescriptionEN { set; get; }

        [DisplayName("Thumbnail")]
        public string Thumbnail { set; get; }


        public string RelatedTo { set; get; }
    
        public string Video { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("ManualCategory")]
        //[Range(1, int.MaxValue, ErrorMessage = "Category is required")]
        [UIHint("GridForeignKey")]
        public int ManualCategoryID { set; get; }

        [DisplayName("ManualCategoryExtraID")]
        public int ManualCategoryExtraID { set; get; }

        public ManualCategoryEntity Category { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Attach File")]
        public string AttachFileName { set; get; }

        [DisplayName("List Imagess")]
        public List<string> ListImages { set; get; }

        [DisplayName("Slide Imagess")]
        public string SlideImages { set; get; }

        public string ImagesContent { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }

        [DisplayName("Allow Show Gallery")]
        public bool IsAllowShowGallery { set; get; }

        public ManualContentEntity()
        {
            Title = string.Empty;
            TitleEN = string.Empty;
            ShortDescription= string.Empty;
            ShortDescriptionEN = string.Empty;
            LongDescriptionEN = string.Empty;
            LongDescription = string.Empty;
            Thumbnail = string.Empty;
            ManualCategoryID = 0;
            ManualCategoryExtraID = 0;
            SortOrder = 1;
            IsActive = true;
            CreatedDate = DateTime.Now;
            ModifiedDate = DateTime.Now;
            ListImages = new List<string>();
            IsAllowShowGallery = false;
            SlideImages = string.Empty;
            Video = string.Empty;
            Category = new ManualCategoryEntity();
            RelatedTo = string.Empty;
        }
    }
}