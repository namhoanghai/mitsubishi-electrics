﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Mitsubishi.UI.Models.Entities
{
    public class ParamConfig
    {
        [DisplayName("Param Name")]
        public string ParamName { set; get; }

        [DisplayName("Param Value")]
        public string ParamValue { set; get; }

        public ParamConfig()
        {
            ParamName = string.Empty;
            ParamValue = string.Empty;
        }
    }
}