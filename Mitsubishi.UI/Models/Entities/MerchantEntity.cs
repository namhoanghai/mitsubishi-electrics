﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class MerchantEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int Order { get; set; }

        public MerchantEntity()
        {
            Name = string.Empty;
            Image = string.Empty;
            Order = 1;
        }
    }

    public class ProductMerchantEntity
    {
        public int ProductId { get; set; }
        public int MerchantId { get; set; }
        public string Link { get; set; }
        public MerchantEntity Merchant { get; set; }
        public ProductMerchantEntity()
        {
            Link = string.Empty;
            Merchant = new MerchantEntity();
        }
    }
}