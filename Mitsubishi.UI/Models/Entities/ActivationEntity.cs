﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class ActivationEntity
    {
        [DisplayName("ItemID")]
        public int ItemID { get; set; }
        

        [DisplayName("Title")]
        public string Title { get; set; }
       

        [DisplayName("Description")]
        public string Description { get; set; }

        public int Order { get; set; }

        [DisplayName("Thumbnail")]
        public string Thumbnail { set; get; }


        [DisplayName("StartDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd HH:mm}")]
        public DateTime StartDate { set; get; }

        [DisplayName("Address")]
        public string Address { set; get; }


        [DisplayName("EndDate")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd HH:mm}")]
        public DateTime EndDate { set; get; }

         [DisplayName("IsActive")]
        public bool IsActive { get; set; }

        public ActivationEntity()
        {
            Title = string.Empty;
            Description = string.Empty;
            Address = string.Empty;
            Order = 1;
            IsActive = true;
            StartDate = DateTime.Now;
            EndDate = DateTime.Now;
        }
    }
}