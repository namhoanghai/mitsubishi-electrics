﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mitsubishi.UI.Models.Entities
{
    public class MemberEntity
    {
        public int ItemID { set; get; }

        [Required]
        [DisplayName("UserName")]
        public string UserName { set; get; }

        [Required]
        [DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password { set; get; }

        [DisplayName("FullName")]
        public string FullName { set; get; }

        [DisplayName("Is Admin")]
        public bool IsAdmin { set; get; }

        [DisplayName("Is Publisher")]
        public bool IsPublisher { set; get; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }
    }
}