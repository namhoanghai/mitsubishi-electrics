﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mitsubishi.UI.Models.Entities
{
    public class CareersEntity
    {
        public int ItemID { set; get; }

        [DisplayName("Title")]
        public string Title { set; get; }

        [DisplayName("Title")]
        public string TitleEN { set; get; }

        [DisplayName("Description")]
        public string LongDescription { set; get; }

        [DisplayName("Description")]
        public string LongDescriptionEN { set; get; }

        [DisplayName("Sort Order")]
        public int SortOrder { set; get; }

        [DisplayName("Publish")]
        public bool IsActive { set; get; }

        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { set; get; }

        [DisplayName("Modified Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedDate { set; get; }

        public CareersEntity()
        {
            SortOrder = 1;
            IsActive = true;
        }
    }
}