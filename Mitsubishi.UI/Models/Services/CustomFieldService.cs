﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using log4net;
using Mitsubishi.UI.Models.Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Mitsubishi.UI.Models.Services
{
    public class CustomFieldService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "CustomField";
        private const string ID = "Id";
        private const string TITLE = "Title";
        private const string TITLE_EN = "TitleEN";
        private const string DESCRIPTION = "Description";
        private const string DESCRIPTION_EN = "DescriptionEN";
        private const string PRODUCTID = "ProductId";
        private const string ALIAS = "Alias";
        private const string ORDER = "Order";

        public CustomFieldService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public CustomField GetCustomFieldByAlias(string Alias)
        {
            var entity = new CustomField();

            try
            {
                var sql = "SELECT * FROM {0} WHERE Alias = @Alias";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@Alias", DbType.String, Alias);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public CustomField GetCustomField(int Id)
        {
            var entity = new CustomField();

            try
            {
                var sql = "SELECT * FROM {0} WHERE Id = @ItemId";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemId", DbType.Int32, Id);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<CustomField> GetListCustomFields(int productId)
        {
            var lst = new List<CustomField>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ProductId = {1} ORDER BY [Order]";

                sql = string.Format(sql, TABLE_NAME, productId);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public bool Create(CustomField cf)
        {
            try
            {
                var sql = "INSERT INTO {0} VALUES (N'{1}', N'{2}', N'{3}', N'{4}', N'{5}', {6}, {7})";

                sql = string.Format(sql, TABLE_NAME, cf.Alias, cf.TitleEN, cf.Title, cf.DescriptionEN, cf.Description, cf.ProductId.ToString(), cf.Order.ToString());

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        public bool Update(CustomField cf)
        {
            try
            {
                var sql = "update {0} set Alias = N'{1}', Description = N'{2}', DescriptionEN = N'{3}', [Order] = {4}, Title = N'{5}', TitleEN = N'{6}' where Id = {7}";

                sql = string.Format(sql, TABLE_NAME, cf.Alias, cf.Description, cf.DescriptionEN, cf.Order, cf.Title, cf.TitleEN, cf.Id);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        public bool Delete(int cfId)
        {
            try
            {
                var sql = "delete from {0} where Id = {1}";

                sql = string.Format(sql, TABLE_NAME, cfId);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        private CustomField ConvertToEntity(DataRow row)
        {
            var entity = new CustomField();

            try
            {
                if (row != null)
                {
                    entity = new CustomField();

                    if (row.Table.Columns.Contains(ID) && !row.IsNull(ID))
                    {
                        entity.Id = Convert.ToInt32(row[ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(DESCRIPTION) && !row.IsNull(DESCRIPTION))
                    {
                        entity.Description = row[DESCRIPTION].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(DESCRIPTION_EN) && !row.IsNull(DESCRIPTION_EN))
                    {
                        entity.DescriptionEN = row[DESCRIPTION_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(PRODUCTID) && !row.IsNull(PRODUCTID))
                    {
                        entity.ProductId = Convert.ToInt32(row[PRODUCTID]);
                    }

                    if (row.Table.Columns.Contains(ALIAS) && !row.IsNull(ALIAS))
                    {
                        entity.Alias = row[ALIAS].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(ORDER) && !row.IsNull(ORDER))
                    {
                        entity.Order = Convert.ToInt32(row[ORDER]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }


    }
}