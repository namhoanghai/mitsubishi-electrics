﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class AgencyService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Agency";
        private const string ITEM_ID = "ItemID";
        private const string LOCATION_ID = "LocationID";
    
        private const string TITLE = "Title";
        private const string ADDRESS = "Address";
        private const string PHONE = "Phone";
        private const string LOGO = "Logo";
        private const string IS_AUTHORIZED = "IsAuthorized";
        private const string IS_DELIVERY = "IsDelivery";
        private const string IS_PAYMENT_CARD = "IsPaymentCard";
        private const string IS_WARRANTY = "IsWarranty";
        private const string IS_CAR_PARKING = "IsCarParking";
        private const string LONGITUDE = "Longitude";
        private const string LATITUDE = "Latitude";
        private const string SORT_ORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";
        private const string PRODUCT_TYPE = "ProductTypeID";
        private const string PRODUCT_CATEGORY_ID = "ProductCategoryID";
        private const string PRODUCT_CATEGORY = "ProductCategory";
        private const string WEBSITE = "Website";

        public AgencyService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public List<AgencyEntity> Read(string searchCriteria = "")
        {
            var lst = new List<AgencyEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC";

                if (!string.IsNullOrEmpty(searchCriteria.Trim()))
                {
                    sql = "SELECT * FROM {0} WHERE Title LIKE '%' + @Criteria + '%' " +
                          "OR Address LIKE '%' + @Criteria + '%' OR Phone LIKE '%' + @Criteria +'%' " +
                          "ORDER BY SortOrder ASC"; 
                }

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@Criteria", DbType.String, searchCriteria);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<AgencyEntity> GetList(int? typeId, int? categoryId, string myPosition, int distance)
        {
            var lst = new List<AgencyEntity>();

            try
            {
                var whereClause = " IsActive = 1 AND IsAuthorized = 0 ";

                if (typeId != null)
                {
                    whereClause += " AND (ProductTypeID = @ProductTypeID) ";
                }

                if (categoryId != null)
                {
                    whereClause += " AND (ProductCategory LIKE '%' + @ProductCategoryID + '%') ";
                }

                var sql = "DECLARE @g geography; " +
                          "DECLARE @h geography; " +
                          "SET @g = geography::STGeomFromText('POINT(' + @MyPoint + ')', 4326); " +
                          "SELECT * FROM Agency WHERE {0} AND (@g.STDistance(geography::STGeomFromText('POINT(' + CONVERT(varchar(100), Longitude) + ' ' + CONVERT(varchar(100), Latitude) + ')', 4326)) <= @Distance)";

                sql = String.Format(sql, whereClause);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ProductTypeID", DbType.Int32, typeId);
                _db.AddInParameter(cmd, "@ProductCategoryID", DbType.String, categoryId);
                _db.AddInParameter(cmd, "@MyPoint", DbType.String, myPosition);
                _db.AddInParameter(cmd, "@Distance", DbType.Int32, distance);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<AgencyEntity> GetList(int? locationId)
        {
            var lst = new List<AgencyEntity>();

            try
            {
               
              
                var sql = "";

                if (locationId != 0)
                {

                    sql = "SELECT * FROM {0} WHERE LocationID = {1} AND IsActive = 1 AND IsAuthorized = 0 ORDER BY SortOrder ASC";
                    sql = string.Format(sql, TABLE_NAME, locationId);
                }
                else
                {

                    sql = "SELECT * FROM {0} WHERE IsActive = 1 AND IsAuthorized = 0 ORDER BY SortOrder ASC";
                    sql = string.Format(sql, TABLE_NAME);
                }


                var cmd = _db.GetSqlStringCommand(sql);

              //  _db.AddInParameter(cmd, "@ProductTypeID", DbType.Int32, typeId);
              //  _db.AddInParameter(cmd, "@ProductCategoryID", DbType.String, categoryId);
                _db.AddInParameter(cmd, "@LocationID", DbType.Int32, locationId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<AgencyEntity> GetList(int categoryId, string criteria = "")
        {
            var lst = new List<AgencyEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE IsActive = 1 AND IsAuthorized = 0 AND ProductCategory LIKE '%' + @ProductCategoryID + '%' ORDER BY SortOrder ASC";

                if (categoryId == 0 && string.IsNullOrEmpty(criteria))
                {
                    sql = "SELECT * FROM {0} WHERE IsActive = 1 AND IsAuthorized = 0 ORDER BY SortOrder ASC";
                }
                else if (categoryId == 0 && !string.IsNullOrEmpty(criteria))
                {
                    sql = "SELECT * FROM {0} WHERE (IsActive = 1) AND ( IsAuthorized = 0 ) AND " +
                          "(Title LIKE '%' + @Criteria + '%' OR Address LIKE '%' + @Criteria + '%' OR " +
                          "Phone LIKE '%' + @Criteria + '%') ORDER BY SortOrder ASC, Title ASC";
                }
                else if (categoryId != 0 && string.IsNullOrEmpty(criteria))
                {
                    sql = "SELECT * FROM {0} WHERE IsActive = 1 AND IsAuthorized = 0 AND ProductCategory LIKE '%' + @ProductCategoryID + '%' ORDER BY SortOrder ASC";
                }
                else if (categoryId != 0 && !string.IsNullOrEmpty(criteria))
                {
                    sql = "SELECT * FROM {0} WHERE (IsActive = 1 AND IsAuthorized = 0 AND ProductCategory LIKE '%' + @ProductCategoryID + '%' ) AND " +
                          "(Title LIKE '%' + @Criteria + '%' OR Address LIKE '%' + @Criteria + '%' OR " +
                          "Phone LIKE '%' + @Criteria + '%') ORDER BY SortOrder ASC, Title ASC";
                }

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                if (criteria != null)
                {
                    criteria = criteria.Trim();
                }

                _db.AddInParameter(cmd, "@ProductCategoryID", DbType.String, categoryId);
                _db.AddInParameter(cmd, "@Criteria", DbType.String, criteria);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<AgencyEntity> GetListByLocationId(int LocationId )
        {
            var lst = new List<AgencyEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE IsActive = 1 AND IsAuthorized = 0 AND LocationID = {1} ORDER BY SortOrder ASC ";



                sql = string.Format(sql, TABLE_NAME, LocationId);

                var cmd = _db.GetSqlStringCommand(sql);
              
                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }



        public List<AgencyEntity> GetListAuthorized(string myPosition, int distance)
        {
            var lst = new List<AgencyEntity>();

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Agency_GetListAuthorizedInDistance");

                _db.AddInParameter(cmd, "@MyPoint", DbType.String, myPosition);
                _db.AddInParameter(cmd, "@Distance", DbType.Int32, distance);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<AgencyEntity> GetListAuthorized(int categoryId, string criteria = "")
        {
            var lst = new List<AgencyEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE IsActive = 1 AND ProductCategory LIKE '%' + @ProductCategoryID + '%' " +
                          "AND IsAuthorized = 1 ORDER BY SortOrder ASC";

                if (!string.IsNullOrEmpty(criteria))
                {
                    sql = "SELECT * FROM {0} WHERE (IsActive = 1 AND ProductCategory LIKE '%' + @ProductCategoryID + '%') AND " +
                          "(Title LIKE '%' + @Criteria + '%' OR Address LIKE '%' + @Criteria + '%' OR " +
                          "Phone LIKE '%' + @Criteria + '%') ORDER BY SortOrder ASC, Title ASC";
                }

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ProductCategoryID", DbType.Int32, categoryId);
                _db.AddInParameter(cmd, "@Criteria", DbType.String, criteria);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<AgencyEntity> GetListAuthorized(int locationId)
        {
            var lst = new List<AgencyEntity>();

            try
            {
                var sql = "";
                if(locationId == 0){
                    sql = "SELECT * FROM {0} WHERE (IsActive = 1) AND IsAuthorized = 1  ORDER BY SortOrder ASC, Title ASC";
                    sql = string.Format(sql, TABLE_NAME);

                }
                else
                {
                    sql = "SELECT * FROM {0} WHERE (IsActive = 1) AND IsAuthorized = 1 AND LocationID = {1} ORDER BY SortOrder ASC, Title ASC";
                    sql = string.Format(sql, TABLE_NAME, locationId);

                }
               
                var cmd = _db.GetSqlStringCommand(sql);

               // _db.AddInParameter(cmd, "@ProductCategoryID", DbType.String, categoryId);
              //  _db.AddInParameter(cmd, "@LocationID", DbType.Int32, locationId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public int Create(AgencyEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Agency_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + LOCATION_ID, DbType.Int32, e.LocationID);
                _db.AddInParameter(cmd, "@" + ADDRESS, DbType.String, e.Address);
                _db.AddInParameter(cmd, "@" + PHONE, DbType.String, e.Phone);
                _db.AddInParameter(cmd, "@" + LOGO, DbType.String, e.Logo);
                _db.AddInParameter(cmd, "@" + IS_AUTHORIZED, DbType.Boolean, e.IsAuthorized);
                _db.AddInParameter(cmd, "@" + IS_DELIVERY, DbType.Boolean, e.IsDelivery);
                _db.AddInParameter(cmd, "@" + IS_PAYMENT_CARD, DbType.Boolean, e.IsPaymentCard);
                _db.AddInParameter(cmd, "@" + IS_WARRANTY, DbType.Boolean, e.IsWarranty);
                _db.AddInParameter(cmd, "@" + IS_CAR_PARKING, DbType.Boolean, e.IsCarParking);
                
                _db.AddInParameter(cmd, "@" + LONGITUDE, DbType.Double, e.Longitude);
                _db.AddInParameter(cmd, "@" + LATITUDE, DbType.Double, e.Latitude);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.String, e.IsActive);
                _db.AddInParameter(cmd, "@" + PRODUCT_TYPE, DbType.Int32, e.ProductTypeID);
                _db.AddInParameter(cmd, "@" + PRODUCT_CATEGORY, DbType.String, e.ProductCategory);
                _db.AddInParameter(cmd, "@" + WEBSITE, DbType.String, e.Website);

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result);
        }

        public bool Update(AgencyEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Agency_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + LOCATION_ID, DbType.Int32, e.LocationID);
                _db.AddInParameter(cmd, "@" + ADDRESS, DbType.String, e.Address);
                _db.AddInParameter(cmd, "@" + PHONE, DbType.String, e.Phone);
                _db.AddInParameter(cmd, "@" + LOGO, DbType.String, e.Logo);
                _db.AddInParameter(cmd, "@" + IS_AUTHORIZED, DbType.Boolean, e.IsAuthorized);
                _db.AddInParameter(cmd, "@" + IS_DELIVERY, DbType.Boolean, e.IsDelivery);
                _db.AddInParameter(cmd, "@" + IS_PAYMENT_CARD, DbType.Boolean, e.IsPaymentCard);
                _db.AddInParameter(cmd, "@" + IS_WARRANTY, DbType.Boolean, e.IsWarranty);
                _db.AddInParameter(cmd, "@" + IS_CAR_PARKING, DbType.Boolean, e.IsCarParking);
                _db.AddInParameter(cmd, "@" + LONGITUDE, DbType.String, e.Longitude);
                _db.AddInParameter(cmd, "@" + LATITUDE, DbType.String, e.Latitude);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.String, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.String, e.IsActive);
                _db.AddInParameter(cmd, "@" + PRODUCT_TYPE, DbType.Int32, e.ProductTypeID);
                _db.AddInParameter(cmd, "@" + PRODUCT_CATEGORY, DbType.String, e.ProductCategory);
                _db.AddInParameter(cmd, "@" + WEBSITE, DbType.String, e.Website);
               

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        public bool Destroy(AgencyEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE Agency SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public AgencyEntity GetItem(int itemId)
        {
            AgencyEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        private AgencyEntity ConvertToEntity(DataRow row)
        {
            AgencyEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new AgencyEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(LOCATION_ID) && !row.IsNull(LOCATION_ID))
                    {
                        entity.LocationID = Convert.ToInt32(row[LOCATION_ID].ToString());

                        if (entity.LocationID != 0)
                        {
                            entity.Location = new LocationService().GetItem(entity.LocationID);
                        }
                    }
                    
                    
                    if (row.Table.Columns.Contains(PRODUCT_TYPE) && !row.IsNull(PRODUCT_TYPE))
                    {
                        entity.ProductTypeID = Convert.ToInt32(row[PRODUCT_TYPE].ToString());
                    }

                    //if (row.Table.Columns.Contains(PRODUCT_CATEGORY) && !row.IsNull(PRODUCT_CATEGORY))
                    //{
                    //    entity.ProductCategoryID = Convert.ToInt32(row[PRODUCT_CATEGORY].ToString());
                    //}

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    if (row.Table.Columns.Contains(ADDRESS) && !row.IsNull(ADDRESS))
                    {
                        entity.Address = row[ADDRESS].ToString();
                    }

                    if (row.Table.Columns.Contains(PHONE) && !row.IsNull(PHONE))
                    {
                        entity.Phone = row[PHONE].ToString();
                    }

                    if (row.Table.Columns.Contains(LOGO) && !row.IsNull(LOGO))
                    {
                        entity.Logo = row[LOGO].ToString();
                    }

                    if (row.Table.Columns.Contains(IS_AUTHORIZED) && !row.IsNull(IS_AUTHORIZED))
                    {
                        entity.IsAuthorized = Convert.ToBoolean(row[IS_AUTHORIZED]);
                    }

                    if (row.Table.Columns.Contains(IS_DELIVERY) && !row.IsNull(IS_DELIVERY))
                    {
                        entity.IsDelivery = Convert.ToBoolean(row[IS_DELIVERY]);
                    }

                    if (row.Table.Columns.Contains(IS_PAYMENT_CARD) && !row.IsNull(IS_PAYMENT_CARD))
                    {
                        entity.IsPaymentCard = Convert.ToBoolean(row[IS_PAYMENT_CARD]);
                    }

                    if (row.Table.Columns.Contains(IS_WARRANTY) && !row.IsNull(IS_WARRANTY))
                    {
                        entity.IsWarranty = Convert.ToBoolean(row[IS_WARRANTY]);
                    }

                    if (row.Table.Columns.Contains(IS_CAR_PARKING) && !row.IsNull(IS_CAR_PARKING))
                    {
                        entity.IsCarParking = Convert.ToBoolean(row[IS_CAR_PARKING]);
                    }

                    if (row.Table.Columns.Contains(LONGITUDE) && !row.IsNull(LONGITUDE))
                    {
                        entity.Longitude = Convert.ToDouble(row[LONGITUDE]);
                    }

                    if (row.Table.Columns.Contains(LATITUDE) && !row.IsNull(LATITUDE))
                    {
                        entity.Latitude = Convert.ToDouble(row[LATITUDE]);
                    }

                    if (entity.Longitude > 0D && entity.Latitude > 0D)
                    {
                        entity.IsMap = true;
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER].ToString());
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE].ToString());
                    }

                    if (row.Table.Columns.Contains(PRODUCT_CATEGORY) && !row.IsNull(PRODUCT_CATEGORY))
                    {
                        entity.ProductCategory = row[PRODUCT_CATEGORY].ToString();

                        if (!string.IsNullOrEmpty(entity.ProductCategory))
                        {
                            var lst = entity.ProductCategory.Split(',').ToList();

                            foreach (var itemId in lst)
                            {
                                if (!string.IsNullOrEmpty(itemId))
                                {
                                    var e = new ProductCategoryService().GetListItemEntity(Convert.ToInt32(itemId));

                                    entity.ListProductCategory.Add(e);
                                }
                            }
                        }
                    }

                    if (row.Table.Columns.Contains(WEBSITE) && !row.IsNull(WEBSITE))
                    {
                        entity.Website = row[WEBSITE].ToString();
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

      
    }
}