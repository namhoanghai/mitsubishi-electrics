﻿using System;
using System.Data;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Mitsubishi.UI.Models.Services
{
    public class DbHelper
    {
        private static readonly ILog Logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);

        public static DataTable ExecuteDataTable(Database db, string sql)
        {
            try
            {
                var cmd = db.GetSqlStringCommand(sql);

                var dt = db.ExecuteDataSet(cmd).Tables[0];

                return dt;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return null;
        }
    }
}