﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ProductService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Product";
        private const string ITEM_ID = "ItemID";
        private const string CATEGORY_ID = "CategoryID";
        private const string NAME_ENG = "NameEng";
        private const string NAME = "Name";
        private const string SHORT_DESCIPTION_EN = "ShortDescriptionEng";
        private const string LONG_DESCIPTION_EN = "LongDescriptionEng";
        private const string SHORT_DESCIPTION = "ShortDescription";
        private const string LONG_DESCIPTION = "LongDescription";
        private const string IMAGE_NAME = "ImageName";
        private const string SORT_ORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";



        public ProductService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public IEnumerable<ProductEntity> Read()
        {
            var lst = new List<ProductEntity>();

            try
            {
                var sql = "SELECT * FROM {0}";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductEntity> GetList()
        {
            var lst = new List<ProductEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC, Name ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public  List<ProductEntity> GetListProductByCategory(int CategoryID)
        {
            var lst = new List<ProductEntity>();

            try
            {
                var sql = "SELECT * FROM {0} where CategoryID = {1} ORDER BY SortOrder ASC, Name ASC";

                sql = string.Format(sql, TABLE_NAME, CategoryID);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public int Create(ProductEntity e)
        {

            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Product_Save");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + CATEGORY_ID, DbType.Int32, e.CategoryID);
                _db.AddInParameter(cmd, "@" + NAME_ENG, DbType.String, e.NameEng.Trim());
                _db.AddInParameter(cmd, "@" + NAME, DbType.String, e.Name.Trim());
                _db.AddInParameter(cmd, "@" + SHORT_DESCIPTION, DbType.String, e.ShortDescription);
                _db.AddInParameter(cmd, "@" + SHORT_DESCIPTION_EN, DbType.String, e.ShortDescriptionEng);
                _db.AddInParameter(cmd, "@" + LONG_DESCIPTION, DbType.String, e.LongDescription);
                _db.AddInParameter(cmd, "@" + LONG_DESCIPTION_EN, DbType.String, e.LongDescriptionEng);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + IMAGE_NAME, DbType.String, e.ImageName);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result);
        }

        public bool Destroy(ProductEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE Product SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public ProductEntity GetItem(int itemId)
        {
            ProductEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        private List<ProductEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<ProductEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new ProductEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ProductEntity ConvertToEntity(DataRow row)
        {
            ProductEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ProductEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(CATEGORY_ID) && !row.IsNull(CATEGORY_ID))
                    {
                        entity.CategoryID = Convert.ToInt32(row[CATEGORY_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(NAME_ENG) && !row.IsNull(NAME_ENG))
                    {
                        entity.NameEng = row[NAME_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(NAME) && !row.IsNull(NAME))
                    {
                        entity.Name = row[NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCIPTION) && !row.IsNull(SHORT_DESCIPTION))
                    {
                        entity.ShortDescription = row[SHORT_DESCIPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCIPTION_EN) && !row.IsNull(SHORT_DESCIPTION_EN))
                    {
                        entity.ShortDescriptionEng = row[SHORT_DESCIPTION_EN].ToString();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCIPTION) && !row.IsNull(LONG_DESCIPTION))
                    {
                        entity.LongDescription = row[LONG_DESCIPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCIPTION_EN) && !row.IsNull(LONG_DESCIPTION_EN))
                    {
                        entity.LongDescriptionEng = row[LONG_DESCIPTION_EN].ToString();
                    }
                   
                    if (row.Table.Columns.Contains(IMAGE_NAME) && !row.IsNull(IMAGE_NAME))
                    {
                        entity.ImageName = row[IMAGE_NAME].ToString();
                    }

                   

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}