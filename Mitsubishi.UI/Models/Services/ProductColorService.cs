﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ProductColorService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "ProductColor";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Title";
        private const string TITLE_EN = "TitleEN";
        private const string CODE = "Code";
        private const string PRODUCT_ID = "ProductID";
        private const string IMAGES = "Images";
        private const string SORT_ORDER = "SortOrder";

        public ProductColorService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public int Create(ProductColorEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ProductColor_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + PRODUCT_ID, DbType.Int32, e.ProductID);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + CODE, DbType.String, e.Code);
                _db.AddInParameter(cmd, "@" + IMAGES, DbType.String, e.Images);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.Int32, e.SortOrder);

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public bool Update(ProductColorEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ProductColor_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + CODE, DbType.String, e.Code);
                _db.AddInParameter(cmd, "@" + IMAGES, DbType.String, e.Images);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.Int32, e.SortOrder);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        public bool Destroy(int id)
        {
            try
            {
                var sql = "DELETE FROM ProductColor WHERE ItemID = @ItemID; ";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, id);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public ProductColorEntity GetItem(int colorId)
        {
            ProductColorEntity entity = null;

            try
            {
                var sql = "SELECT * FROM ProductColor WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, colorId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ProductColorEntity> GetList(int productId)
        {
            var lst = new List<ProductColorEntity>();

            try
            {
                var sql = "SELECT * FROM ProductColor WHERE ProductID = @ProductID ORDER BY SortOrder ASC";

                var cmd = _db.GetSqlStringCommand(sql);
                
                _db.AddInParameter(cmd, "@ProductID", DbType.Int32, productId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ProductColorEntity ConvertToEntity(DataRow row)
        {
            ProductColorEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ProductColorEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(PRODUCT_ID) && !row.IsNull(PRODUCT_ID))
                    {
                        entity.ProductID = Convert.ToInt32(row[PRODUCT_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString().Trim();
                    }
                    if (row.Table.Columns.Contains(CODE) && !row.IsNull(CODE))
                    {
                        entity.Code = row[CODE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(IMAGES) && !row.IsNull(IMAGES))
                    {
                        entity.Images = row[IMAGES].ToString().Trim();

                        if (!string.IsNullOrEmpty(entity.Images))
                        {
                            var temp = entity.Images.Split(';').ToList();

                            foreach (var image in temp)
                            {
                                if (!string.IsNullOrEmpty(image))
                                {
                                    entity.ListImages.Add(image);
                                }
                            }
                        }
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}