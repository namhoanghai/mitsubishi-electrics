﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ManualService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "ManualContent";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Title";
        private const string TITLE_EN = "TitleEN";
        private const string LONG_DESCRIPTION = "LongDescription";
        private const string LONG_DESCRIPTION_EN = "LongDescriptionEN";
        private const string THUMBNAIL = "Thumbnail";
        private const string SORTORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";
        private const string MANUAL_CATEGORY_ID = "ManualCategoryID";
        private const string MANUAL_CATEGORY_EXTRA_ID = "ManualCategoryExtraID";
        private const string VIDEO = "Video";
        private const string RELATED_TO = "RelatedTo";


        public ManualService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public ManualContentEntity GetItem(int itemId)
        {
            ManualContentEntity entity = null;

            try
            {
                var sql = "SELECT * FROM ManualContent WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToFullEntity(data.Rows[0]);
                }
            }
            catch (Exception)
            {
                
                throw;
            }

            return entity;
        }

        public IEnumerable<ManualContentEntity> Read()
        {
            var lst = new List<ManualContentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        public ManualContentEntity GetFirstItem()
        {
            ManualContentEntity entity = null;

            try
            {
                var sql = "SELECT TOP 1 * FROM {0} WHERE IsActive = 1 ORDER BY SortOrder ASC";
                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);
              
                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToFullEntity(data.Rows[0]);
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ManualContentEntity> GetListVideo(string catIDs, string count)
        {
            var lst = new List<ManualContentEntity>();

            try
            {
                var sql = "";
                var str_where = "";
                var str_select = "";
                if (catIDs != "")
                {
                    str_where = " AND ManualCategoryID IN ({0}) ";
                    str_where = string.Format(str_where, catIDs);
                }
                if (count != "" &&  count != "0")
                {
                    str_select = " TOP ({0}) ";
                    str_select = string.Format(str_select, count);
                }

                sql = "SELECT {0}* FROM {1} WHERE Video != ''  {2}  ORDER BY SortOrder ASC, ItemID DESC";

                sql = string.Format(sql, str_select, TABLE_NAME, str_where);



                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToFullEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ManualContentEntity> GetListVideoRelated(string catIDs, string count, int exID)
        {
            var lst = new List<ManualContentEntity>();

            try
            {
                var sql = "";
                var str_where = "";
                var str_select = "";
                if (catIDs != "")
                {
                    str_where = " AND ManualCategoryID IN ({0}) ";
                    str_where = string.Format(str_where, catIDs);
                }
                if (count != "" && count != "0")
                {
                    str_select = " TOP ({0}) ";
                    str_select = string.Format(str_select, count);
                }

                sql = "SELECT {0}* FROM {1} WHERE Video != ''  {2} AND ItemId != {3} ORDER BY SortOrder ASC, ItemID DESC";

                sql = string.Format(sql, str_select, TABLE_NAME, str_where , exID);



                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToFullEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        public List<ManualContentEntity> GetListAll()
        {
            var lst = new List<ManualContentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY  ModifiedDate DESC, SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToFullEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }


        public List<ManualContentEntity> GetList(string catIDs, int count = 0, string ManualCategoryExtraID = "0", int Isvideo = 0)
        {
            var lst = new List<ManualContentEntity>();

            try
            {
                var sql = string.Empty;
                var str_select = string.Empty;
                var str_where = string.Empty;
                
                if (catIDs != "")
                {
                    str_where = " WHERE ManualCategoryID IN ({0}) ";
                    str_where = string.Format(str_where, catIDs);
                }
                if (ManualCategoryExtraID != "" && ManualCategoryExtraID != "0")
                {
                    str_where += " AND ManualCategoryExtraID IN ({0}) ";
                    str_where = string.Format(str_where, ManualCategoryExtraID);
                }


                if (Isvideo == 0)
                {
                    str_where += " AND Video IS NULL ";
                }

                if (count != 0)
                {
                    str_select  = " TOP ({0}) ";
                    str_select = string.Format(str_select, count);
                }

                sql = "SELECT {0}* FROM {1}  {2} ORDER BY SortOrder ASC, ItemID DESC";

                sql = string.Format(sql, str_select, TABLE_NAME, str_where);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToFullEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ManualContentEntity> GetListWithCount(string filter , int count)
        {
            var lst = new List<ManualContentEntity>();

            try
            {
                var sql = "SELECT TOP {0} * FROM {1} WHERE ManualCategoryID IN ({2}) AND IsActive = 1 ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, count, TABLE_NAME, filter);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToFullEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }


        public List<ManualContentEntity> GetListRelated(int exclude, int count , int catID)
        {

          

           // sql = string.Format(sql, count, TABLE_NAME, cat, exclude);

            var lst = new List<ManualContentEntity>();

            try
            {
                var sql = "SELECT TOP {0} * FROM {1} WHERE ManualCategoryID = {2} AND IsActive = 1 AND ItemID != {3} ORDER BY RAND() ";

                sql = string.Format(sql, count, TABLE_NAME, catID, exclude );

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToFullEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

       
        
        public bool Destroy(ManualContentEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE ManualContent SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public int Create(ManualContentEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Manual_Content_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + MANUAL_CATEGORY_ID, DbType.Int32, e.ManualCategoryID);           
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);            
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION, DbType.String, e.LongDescription);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION_EN, DbType.String, e.LongDescriptionEN);
                _db.AddInParameter(cmd, "@" + THUMBNAIL, DbType.String, e.Thumbnail);
                _db.AddInParameter(cmd, "@" + SORTORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + VIDEO, DbType.String, e.Video);
                _db.AddInParameter(cmd, "@" + MANUAL_CATEGORY_EXTRA_ID, DbType.Int32, e.ManualCategoryExtraID);
                _db.AddInParameter(cmd, "@" + RELATED_TO, DbType.String, e.RelatedTo);
                

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public bool Update(ManualContentEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Manual_Content_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + MANUAL_CATEGORY_ID, DbType.Int32, e.ManualCategoryID);
                _db.AddInParameter(cmd, "@" + MANUAL_CATEGORY_EXTRA_ID, DbType.Int32, e.ManualCategoryExtraID);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION, DbType.String, e.LongDescription);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION_EN, DbType.String, e.LongDescriptionEN);
                _db.AddInParameter(cmd, "@" + THUMBNAIL, DbType.String, e.Thumbnail);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + SORTORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + RELATED_TO, DbType.String, e.RelatedTo);
                _db.AddInParameter(cmd, "@" + VIDEO, DbType.String, e.Video);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        private List<ManualContentEntity> ConvertToEnumerable(DataTable dt)
        {
            var lst = new List<ManualContentEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    lst.AddRange(from DataRow row in dt.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ManualContentEntity ConvertToEntity(DataRow row)
        {
            ManualContentEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ManualContentEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(MANUAL_CATEGORY_ID) && !row.IsNull(MANUAL_CATEGORY_ID))
                    {
                        entity.ManualCategoryID = Convert.ToInt32(row[MANUAL_CATEGORY_ID].ToString());

                        entity.Category = new ManualCategoryService().GetItem(entity.ManualCategoryID);
                    }
                    if (row.Table.Columns.Contains(MANUAL_CATEGORY_EXTRA_ID) && !row.IsNull(MANUAL_CATEGORY_EXTRA_ID))
                    {
                        entity.ManualCategoryExtraID = Convert.ToInt32(row[MANUAL_CATEGORY_EXTRA_ID].ToString());

                        entity.Category = new ManualCategoryService().GetItem(entity.ManualCategoryID);
                    }

                    if (row.Table.Columns.Contains(RELATED_TO) && !row.IsNull(RELATED_TO))
                    {
                        entity.RelatedTo = row[RELATED_TO].ToString();
                        
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString().Trim();
                    }

                    
                    if (row.Table.Columns.Contains(LONG_DESCRIPTION) && !row.IsNull(LONG_DESCRIPTION))
                    {
                       entity.LongDescription = row[LONG_DESCRIPTION].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCRIPTION_EN) && !row.IsNull(LONG_DESCRIPTION_EN))
                    {
                        entity.LongDescriptionEN = row[LONG_DESCRIPTION_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(SORTORDER) && !row.IsNull(SORTORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORTORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                 
                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        private ManualContentEntity ConvertToFullEntity(DataRow row)
        {
            ManualContentEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ManualContentEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(MANUAL_CATEGORY_ID) && !row.IsNull(MANUAL_CATEGORY_ID))
                    {
                        entity.ManualCategoryID = Convert.ToInt32(row[MANUAL_CATEGORY_ID].ToString());

                        entity.Category = new ManualCategoryService().GetItem(entity.ManualCategoryID);
                    }
                    if (row.Table.Columns.Contains(MANUAL_CATEGORY_EXTRA_ID) && !row.IsNull(MANUAL_CATEGORY_EXTRA_ID))
                    {
                        entity.ManualCategoryExtraID = Convert.ToInt32(row[MANUAL_CATEGORY_EXTRA_ID].ToString());

                        entity.Category = new ManualCategoryService().GetItem(entity.ManualCategoryID);
                    }

                    if (row.Table.Columns.Contains(RELATED_TO) && !row.IsNull(RELATED_TO))
                    {
                        entity.RelatedTo = row[RELATED_TO].ToString();

                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString().Trim();
                    }

                 

                    if (row.Table.Columns.Contains(LONG_DESCRIPTION) && !row.IsNull(LONG_DESCRIPTION))
                    {
                        entity.LongDescription = row[LONG_DESCRIPTION].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCRIPTION_EN) && !row.IsNull(LONG_DESCRIPTION_EN))
                    {
                        entity.LongDescriptionEN = row[LONG_DESCRIPTION_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(SORTORDER) && !row.IsNull(SORTORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORTORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                
                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                    if (row.Table.Columns.Contains(THUMBNAIL) && !row.IsNull(THUMBNAIL))
                    {
                        entity.Thumbnail = row[THUMBNAIL].ToString().Trim();
                    }
                    if (row.Table.Columns.Contains(VIDEO) && !row.IsNull(VIDEO))
                    {
                        entity.Video = row[VIDEO].ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}