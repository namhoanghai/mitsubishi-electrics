﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class VideoService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Video";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Title";
        private const string DESCRIPTION = "Description";
        private const string THUMBNAIL = "Thumbnail";
        private const string LINKVIDEO = "LinkVideo";
        private const string ORDER = "Order";
        private const string ISACTIVE = "IsActive";

        public VideoService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public VideoEntity GetItem(int itemId)
        {
            VideoEntity entity = null;

            try
            {
                var sql = "SELECT * FROM Video WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertEntity(data.Rows[0]);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return entity;
        }

        public IEnumerable<VideoEntity> Read()
        {
            var lst = new List<VideoEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY [Order] ASC, ItemID DESC ";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private List<VideoEntity> ConvertToEnumerable(DataTable dt)
        {
            var lst = new List<VideoEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    lst.AddRange(from DataRow row in dt.Rows select ConvertEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<VideoEntity> GetListAll()
        {
            var lst = new List<VideoEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY  Order ASC, ItemID ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<VideoEntity> GetList(int count, bool IsActive = true)
        {
            var lst = new List<VideoEntity>();


            try
            {
                var sql = "SELECT top {0} * FROM {1} WHERE IsActive ='1' ORDER BY  [Order] ASC, ItemID ASC";

                sql = string.Format(sql, count, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }


     
        public bool Destroy(VideoEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public int Create(VideoEntity e)
        {
            try
            {
                var sql = "INSERT INTO {0} VALUES ({1} , N'{2}', N'{3}', {4}, '{5}','{6}','{7}')";

                if (e.Thumbnail == null) e.Thumbnail = "";
                int lastID = LastID() + 1;
                sql = string.Format(sql, TABLE_NAME, lastID, e.Title, e.Description, e.Order, e.LinkVideo, e.Thumbnail, e.IsActive);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);

                return LastID();

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return 0;
        }

        public int LastID()
        {
            try
            {
                var sql = "SELECT TOP(1) ItemID FROM  {0} ORDER BY ItemID DESC";

                sql = string.Format(sql,TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);
                string LastID = _db.ExecuteScalar(cmd).ToString();
                return int.Parse(LastID);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return 0;
        }

        public bool Update(VideoEntity e)
        {
            try
            {
                var sql = "update {0} set Title = N'{1}', Description = N'{2}', [Order] = {3}, Thumbnail = N'{4}' , LinkVideo = N'{5}' ,  IsActive = '{6}'  where ItemID = {7}";

                sql = string.Format(sql, TABLE_NAME, e.Title, e.Description, e.Order, e.Thumbnail, e.LinkVideo, e.IsActive, e.ItemID);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        private VideoEntity ConvertEntity(DataRow row)
        {
            VideoEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new VideoEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }


                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(DESCRIPTION) && !row.IsNull(DESCRIPTION))
                    {
                        entity.Description = row[DESCRIPTION].ToString().Trim();
                    }

                  



                    if (row.Table.Columns.Contains(ORDER) && !row.IsNull(ORDER))
                    {
                        entity.Order = Convert.ToInt32(row[ORDER]);
                    }


                    if (row.Table.Columns.Contains(THUMBNAIL) && !row.IsNull(THUMBNAIL))
                    {
                        entity.Thumbnail = row[THUMBNAIL].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(LINKVIDEO) && !row.IsNull(LINKVIDEO))
                    {
                        entity.LinkVideo = row[LINKVIDEO].ToString().Trim();
                    }


                    if (row.Table.Columns.Contains(ISACTIVE) && !row.IsNull(ISACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[ISACTIVE]);
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}