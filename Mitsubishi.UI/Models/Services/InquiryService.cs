﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class InquiryService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Inquiry";
        private const string ITEM_ID = "ItemID";
        private const string NAME = "Name";
        private const string COMPANY = "Company";
        private const string COMMENT = "Comment";
        private const string ADDRESS = "Address";
        private const string CITY = "City";
        private const string COUNTRY = "Country";
        private const string PHONE = "Phone";
        private const string EMAIL = "Email";
        private const string CREATED_DATE = "CreatedDate";

        public InquiryService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public IEnumerable<InquiryEntity> Read()
        {
            var lst = new List<InquiryEntity>();

            try
            {
                var sql = "SELECT * FROM {0}";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<InquiryEntity> GetList()
        {
            var lst = new List<InquiryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY CreatedDate ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public int Create(string comment, string name, string company, string address, string city,
            string country, string phone, string email, int itemId)
        {

            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Inquiry_Save");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + NAME, DbType.String, name);
                _db.AddInParameter(cmd, "@" + COMPANY, DbType.String, company);
                _db.AddInParameter(cmd, "@" + COMMENT, DbType.String, comment);
                _db.AddInParameter(cmd, "@" + ADDRESS, DbType.String, address);
                _db.AddInParameter(cmd, "@" + CITY, DbType.String, city);
                _db.AddInParameter(cmd, "@" + COUNTRY, DbType.String, country);
                _db.AddInParameter(cmd, "@" + PHONE, DbType.String, phone);
                _db.AddInParameter(cmd, "@" + EMAIL, DbType.String, email);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result);
        }

        public bool Destroy(InquiryEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public InquiryEntity GetItem(int itemId)
        {
            InquiryEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
        
        
        private List<InquiryEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<InquiryEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new InquiryEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private InquiryEntity ConvertToEntity(DataRow row)
        {
            InquiryEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new InquiryEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(NAME) && !row.IsNull(NAME))
                    {
                        entity.Name = row[NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(COMPANY) && !row.IsNull(COMPANY))
                    {
                        entity.Company = row[COMPANY].ToString();
                    }

                    if (row.Table.Columns.Contains(COMMENT) && !row.IsNull(COMMENT))
                    {
                        entity.Comment = row[COMMENT].ToString();
                    }

                    if (row.Table.Columns.Contains(ADDRESS) && !row.IsNull(ADDRESS))
                    {
                        entity.Address = row[ADDRESS].ToString();
                    }

                    if (row.Table.Columns.Contains(CITY) && !row.IsNull(CITY))
                    {
                        entity.City = row[CITY].ToString();
                    }

                    if (row.Table.Columns.Contains(COUNTRY) && !row.IsNull(COUNTRY))
                    {
                        entity.Country = row[COUNTRY].ToString();
                    }

                    if (row.Table.Columns.Contains(PHONE) && !row.IsNull(PHONE))
                    {
                        entity.Phone = row[PHONE].ToString();
                    }

                    if (row.Table.Columns.Contains(EMAIL) && !row.IsNull(EMAIL))
                    {
                        entity.Email = row[EMAIL].ToString();
                    }
                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}