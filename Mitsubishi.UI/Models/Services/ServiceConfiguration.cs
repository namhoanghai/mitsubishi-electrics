﻿using System;
using System.Configuration;

namespace Mitsubishi.UI.Models.Services
{
    public class ServiceConfiguration
    {
        public ServiceConfiguration()
        {
            PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            PageSizes = new[] {10, 30, 50, 100, 300};
            GridHeight = ConfigurationManager.AppSettings["GridHeight"];
            DeleteConfirmationMessage = "Are you sure to want to delete this item?";
            EnableCached = false;

            EmailTo = ConfigurationManager.AppSettings["RECEIVED_MAIL"];

            DownloadPath = ConfigurationManager.AppSettings["FOLDER_DOWNLOAD"];
            ImagePath = ConfigurationManager.AppSettings["FOLDER_IMAGES"];
            RelativeImagePath = "~" + ImagePath;

            if (ConfigurationManager.AppSettings["EnableCached"] != null)
            {
                var str = ConfigurationManager.AppSettings["EnableCached"].Trim().ToUpper();

                if (str == "TRUE" || str == "YES" || str == "1")
                {
                    EnableCached = true;
                }
            }
        }

        public string RelativePath(string path)
        {
            return "~" + path;
        }

        public string DownloadPath { set; get; }
        public string ImagePath { set; get; }
        public string RelativeImagePath { set; get; }

        public string EmailTo { set; get; }

        public int PageSize { get; set; }
        public int[] PageSizes { get; private set; }
        public string GridHeight { get; private set; }
        public string DeleteConfirmationMessage { set; get; }
        public bool EnableCached { set; get; }
    }
}