﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class LocationService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Province";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Name";
        private const string SORT_ORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";

        public LocationService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public LocationEntity GetItem(int itemId)
        {
            var entity = new LocationEntity();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<LocationEntity> GetList()
        {
            var lst = new List<LocationEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE IsActive = 1 ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private LocationEntity ConvertToEntity(DataRow row)
        {
            LocationEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new LocationEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER].ToString());
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE].ToString());
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}