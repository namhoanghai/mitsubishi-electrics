﻿using System;
using System.Collections.Generic;
using System.Data;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class LocalizationService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        public LocalizationService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public List<LocalizationEntity> GetList()
        {
            var lst = new List<LocalizationEntity>();

            try
            {
                var sql = "SELECT * FROM Localization";

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private LocalizationEntity ConvertToEntity(DataRow row)
        {
            LocalizationEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new LocalizationEntity();

                    if (row.Table.Columns.Contains("ItemID") && !row.IsNull("ItemID"))
                    {
                        entity.ItemID = Convert.ToInt32(row["ItemID"]);
                    }

                    if (row.Table.Columns.Contains("LanguageID") && !row.IsNull("LanguageID"))
                    {
                        entity.LanguageID = row["LanguageID"].ToString();
                    }

                    if (row.Table.Columns.Contains("ItemKey") && !row.IsNull("ItemKey"))
                    {
                        entity.ItemKey = row["ItemKey"].ToString();
                    }

                    if (row.Table.Columns.Contains("Title") && !row.IsNull("Title"))
                    {
                        entity.Title = row["Title"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;

        }
    }
}