﻿using System;
using System.Collections.Generic;
using System.Data;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ParamConfigService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "SystemConfig";
        private const string PARAM_NAME = "ParamName";
        private const string PARAM_VALUE = "ParamValue";

        public ParamConfigService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public ParamConfig GetItem(string name)
        {
            ParamConfig entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ParamName = @Name";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@Name", DbType.String, name);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public bool Update(ParamConfig e)
        {
            var result = 0;

            try
            {
                var sql = "UPDATE SystemConfig SET ParamValue = @Value WHERE ParamName = @Name";

                var cmd = _db.GetSqlStringCommand(sql);
                
                _db.AddInParameter(cmd, "@Name", DbType.String, e.ParamName);
                _db.AddInParameter(cmd, "@Value", DbType.String, e.ParamValue);
                
                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result > 0;
        }

        public List<ParamConfig> Read()
        {
            var lst = new List<ParamConfig>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ParamConfig ConvertToEntity(DataRow row)
        {
            ParamConfig entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ParamConfig();

                    if (row.Table.Columns.Contains(PARAM_NAME) && !row.IsNull(PARAM_NAME))
                    {
                        entity.ParamName = row[PARAM_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(PARAM_VALUE) && !row.IsNull(PARAM_VALUE))
                    {
                        entity.ParamValue = row[PARAM_VALUE].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}