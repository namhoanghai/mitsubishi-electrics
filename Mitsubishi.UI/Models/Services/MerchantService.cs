﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using log4net;
using Mitsubishi.UI.Models.Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Mitsubishi.UI.Models.Services
{
    public class MerchantService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME_MERCHANT = "Merchant";
        private const string TABLE_NAME_PRODUCTMERCHANT = "ProductMerchant";
        private const string ID = "Id";
        private const string NAME = "Name";
        private const string LINK = "Link";
        private const string IMAGE = "Image";
        private const string MERCHANTID = "MerchantId";
        private const string PRODUCTID = "ProductId";
        private const string ORDER = "Order";

        public MerchantService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        #region Services

        public bool IsMerchantLinked(int merchantId, int productId)
        {
            try
            {
                var sql = "SELECT Link FROM {0} where ProductId = {1} and MerchantId = {2}";

                sql = string.Format(sql, TABLE_NAME_PRODUCTMERCHANT, productId, merchantId);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data.Rows.Count != 0) return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public string GetMerchantLink(int merchantId, int productId)
        {
            try
            {
                var sql = "SELECT Link FROM {0} where ProductId = {1} and MerchantId = {2}";

                sql = string.Format(sql, TABLE_NAME_PRODUCTMERCHANT, productId, merchantId);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data == null) return "";
              
                return ConvertToProductMerchantEntity(data.Rows[0]).Link;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return "";
        }

        public MerchantEntity GetMerchantById(int merchantId)
        {
            var result = new MerchantEntity();

            try
            {
                var sql = "SELECT * FROM {0} where Id = {1}";

                sql = string.Format(sql, TABLE_NAME_MERCHANT, merchantId);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    result = ConvertToMerchantEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public List<MerchantEntity> GetAllMechants()
        {
            var lst = new List<MerchantEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY [Order]";

                sql = string.Format(sql, TABLE_NAME_MERCHANT);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToMerchantEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductMerchantEntity> GetAllMechantsOfProduct(int productId, bool getFull = true)
        {
            var lst = new List<ProductMerchantEntity>();

            try
            {
                var sql = "SELECT * FROM {0} where ProductId = {1}";

                sql = string.Format(sql, TABLE_NAME_PRODUCTMERCHANT, productId);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToProductMerchantEntity(row));

                    if (getFull)
                    {
                        lst.ForEach(l =>
                        {
                            l.Merchant = GetMerchantById(l.MerchantId);
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        #endregion

        #region ProductMerchant

        public bool CreateProductMerchant(int productId, int merchantId, string link)
        {
            try
            {
                var sql = "INSERT INTO {0} VALUES ({1}, {2}, N'{3}')";

                sql = string.Format(sql, TABLE_NAME_PRODUCTMERCHANT, productId, merchantId, link);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        public bool UpdateProductMerchant(int productId, int merchantId, string link)
        {
            try
            {
                var sql = "update {0} set Link = N'{1}' where ProductId = {2} and MerchantId = {3}";

                sql = string.Format(sql, TABLE_NAME_PRODUCTMERCHANT, link, productId, merchantId);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        public bool DeleteProductMerchant(int productId, int merchantId)
        {
            try
            {
                var sql = "delete from {0} where ProductId = {1} and MerchantId = {2}";

                sql = string.Format(sql, TABLE_NAME_PRODUCTMERCHANT, productId, merchantId);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        private ProductMerchantEntity ConvertToProductMerchantEntity(DataRow row)
        {
            var entity = new ProductMerchantEntity();

            try
            {
                if (row != null)
                {
                    entity = new ProductMerchantEntity();

                    if (row.Table.Columns.Contains(PRODUCTID) && !row.IsNull(PRODUCTID))
                    {
                        entity.ProductId = Convert.ToInt32(row[PRODUCTID].ToString());
                    }

                    if (row.Table.Columns.Contains(MERCHANTID) && !row.IsNull(MERCHANTID))
                    {
                        entity.MerchantId = Convert.ToInt32(row[MERCHANTID].ToString()); ;
                    }

                    if (row.Table.Columns.Contains(LINK) && !row.IsNull(LINK))
                    {
                        entity.Link = row[LINK].ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        #endregion
        
        #region Merchant

        public bool CreateMerchant(MerchantEntity merchant)
        {
            try
            {
                var sql = "INSERT INTO {0} VALUES (N'{1}', N'{2}', {3})";

                sql = string.Format(sql, TABLE_NAME_MERCHANT, merchant.Name, merchant.Image, merchant.Order);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        public bool UpdateMerchant(MerchantEntity merchant)
        {
            try
            {
                var sql = "update {0} set Name = N'{1}', Image = N'{2}', [Order] = {3} where Id = {4}";

                sql = string.Format(sql, TABLE_NAME_MERCHANT, merchant.Name, merchant.Image, merchant.Order, merchant.Id);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        public bool DeleteMerchant(int merchantId)
        {
            try
            {
                var sql = "delete from {0} where Id = {1}";

                sql = string.Format(sql, TABLE_NAME_MERCHANT, merchantId);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }

        private MerchantEntity ConvertToMerchantEntity(DataRow row)
        {
            var entity = new MerchantEntity();

            try
            {
                if (row != null)
                {
                    entity = new MerchantEntity();

                    if (row.Table.Columns.Contains(ID) && !row.IsNull(ID))
                    {
                        entity.Id = Convert.ToInt32(row[ID].ToString());
                    }

                    if (row.Table.Columns.Contains(NAME) && !row.IsNull(NAME))
                    {
                        entity.Name = row[NAME].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(IMAGE) && !row.IsNull(IMAGE))
                    {
                        entity.Image = row[IMAGE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(ORDER) && !row.IsNull(ORDER))
                    {
                        entity.Order = Convert.ToInt32(row[ORDER].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        #endregion        
    }
}