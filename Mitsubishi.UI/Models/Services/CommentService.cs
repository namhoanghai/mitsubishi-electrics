﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class CommentService
    {

        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "ProductComment";
        private const string ITEM_ID = "ItemID";
        private const string PRODUCT_ID = "ProductID";
        private const string NAME = "Name";
        private const string EMAIL = "Email";
        private const string DESCRIPTION = "Description";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";

        public CommentService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }
        public IEnumerable<CommentEntity> Read()
        {
            var lst = new List<CommentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY CreatedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        public List<CommentEntity> GetList()
        {
            var lst = new List<CommentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY CreatedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }


        public List<CommentEntity> GetListByProduct(int itemID)
        {
            var lst = new List<CommentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} where ProductID = {1} ORDER BY CreatedDate DESC";

                sql = string.Format(sql, TABLE_NAME, itemID);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public CommentEntity GetItem(int itemId)
        {
            CommentEntity entity = null;

            try
            {
                var sql = "SELECT * FROM ProductComment WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return entity;
        }

        public List<CommentEntity> GetListByProduct(string ProductID)
        {
            var lst = new List<CommentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ProductID = {1} AND IsActive = 1 ORDER BY CreatedDate DESC";

                sql = string.Format(sql, TABLE_NAME, ProductID);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }


     

        public bool Destroy(CommentEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE ProductComment SET IsActive = @IsActive  WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public int Create(CommentEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Comment_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + PRODUCT_ID, DbType.Int32, e.ProductID);
                _db.AddInParameter(cmd, "@" + DESCRIPTION, DbType.String, e.Description);
                _db.AddInParameter(cmd, "@" + NAME, DbType.String, e.Name);
                _db.AddInParameter(cmd, "@" + EMAIL, DbType.String, e.Email);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public bool Update(CommentEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Comment_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + PRODUCT_ID, DbType.Int32, e.ProductID);
                _db.AddInParameter(cmd, "@" + DESCRIPTION, DbType.String, e.Description);
                _db.AddInParameter(cmd, "@" + NAME, DbType.String, e.Name);
                _db.AddInParameter(cmd, "@" + EMAIL, DbType.String, e.Email);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        private List<CommentEntity> ConvertToEnumerable(DataTable dt)
        {
            var lst = new List<CommentEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    lst.AddRange(from DataRow row in dt.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private CommentEntity ConvertToEntity(DataRow row)
        {
            CommentEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new CommentEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(PRODUCT_ID) && !row.IsNull(PRODUCT_ID))
                    {
                        entity.ProductID = Convert.ToInt32(row[PRODUCT_ID].ToString());
                    }



                    if (row.Table.Columns.Contains(DESCRIPTION) && !row.IsNull(DESCRIPTION))
                    {
                        entity.Description = row[DESCRIPTION].ToString().Trim();
                    }
                    if (row.Table.Columns.Contains(NAME) && !row.IsNull(NAME))
                    {
                        entity.Name = row[NAME].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(EMAIL) && !row.IsNull(EMAIL))
                    {
                        entity.Email = row[EMAIL].ToString().Trim();
                    }
                 

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }


                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

               
                  
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}