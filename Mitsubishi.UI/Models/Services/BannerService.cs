﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Common.Instrumentation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class BannerService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Banners";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Title";
        private const string IMAGE_NAME = "ImageName";
        private const string SORT_ORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";
        private const string IS_ENABLE_LINK = "IsEnableLink";
        private const string REDIRECT_URL = "RedirectUrl";

        public BannerService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public List<BannerEntity> Read()
        {
            var lst = new List<BannerEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<BannerEntity> GetList()
        {
            var lst = new List<BannerEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE IsActive = 1 ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE Banners SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public int Create(BannerEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Banners_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + IMAGE_NAME, DbType.String, e.ImageName);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.String, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + IS_ENABLE_LINK, DbType.Boolean, e.IsEnableLink);
                _db.AddInParameter(cmd, "@" + REDIRECT_URL, DbType.String, e.RedirectUrl);

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result);
        }

        public bool Update(BannerEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Banners_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + IMAGE_NAME, DbType.String, e.ImageName);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.String, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.String, e.IsActive);
                _db.AddInParameter(cmd, "@" + IS_ENABLE_LINK, DbType.Boolean, e.IsEnableLink);
                _db.AddInParameter(cmd, "@" + REDIRECT_URL, DbType.String, e.RedirectUrl);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result > 0;
        }

        public bool Destroy(BannerEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public BannerEntity GetItem(int itemId)
        {
            BannerEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        private List<BannerEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<BannerEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new BannerEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private BannerEntity ConvertToEntity(DataRow row)
        {
            BannerEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new BannerEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    if (row.Table.Columns.Contains(IMAGE_NAME) && !row.IsNull(IMAGE_NAME))
                    {
                        entity.ImageName = row[IMAGE_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER].ToString());
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE].ToString());
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }

                    if (row.Table.Columns.Contains(IS_ENABLE_LINK) && !row.IsNull(IS_ENABLE_LINK))
                    {
                        entity.IsEnableLink = Convert.ToBoolean(row[IS_ENABLE_LINK]);
                    }

                    if (row.Table.Columns.Contains(REDIRECT_URL) && !row.IsNull(REDIRECT_URL))
                    {
                        entity.RedirectUrl = row[REDIRECT_URL].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}