﻿using System;
using System.Collections.Generic;
using System.Data;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class WebUrlService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "WebUrl";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Title";
        private const string URL = "Url";

        public WebUrlService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public List<WebUrlEntity> Read()
        {
            var lst = new List<WebUrlEntity>();

            try
            {
                var sql = "SELECT * FROM {0}";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private WebUrlEntity ConvertToEntity(DataRow row)
        {
            WebUrlEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new WebUrlEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    if (row.Table.Columns.Contains(URL) && !row.IsNull(URL))
                    {
                        entity.Url = row[URL].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}