﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ContentCategoryService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "ContentCategory";
        private const string ITEM_ID = "ItemID";
        private const string TITLE_ENG = "TitleEN";
        private const string TITLE = "Title";
        private const string SORTORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";
        private const string PARENT_ID = "ParentID";

        public ContentCategoryService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public IEnumerable<ContentCategoryEntity> Read()
        {
            var lst = new List<ContentCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0}";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ContentCategoryEntity> GetList()
        {
            var lst = new List<ContentCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public int Create(ContentCategoryEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ContentCategory_Save");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + TITLE_ENG, DbType.String, e.TitleEN.Trim());
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title.Trim());
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + SORTORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + PARENT_ID, DbType.Int32, e.ParentID);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public bool Destroy(ContentCategoryEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public ContentCategoryEntity GetItem(int itemId)
        {
            ContentCategoryEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemId = @ItemId";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemId", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE ContentCategory SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public List<ContentCategoryEntity> GetListRoot(bool isActive = false)
        {
            var lst = new List<ContentCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE (ParentID = 0 OR ParentID IS NULL) ORDER BY SortOrder ASC, Title ASC";

                if (isActive)
                {
                    sql = "SELECT * FROM {0} WHERE (ParentID = 0 OR ParentID IS NULL) AND IsActive = 1 " +
                          "ORDER BY SortOrder ASC, Title ASC";
                }

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ContentCategoryEntity> GetListActive(int parentId)
        {
            var lst = new List<ContentCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ParentID = @ParentID AND IsActive = 1 ORDER BY SortOrder ASC, Title ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private List<ContentCategoryEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<ContentCategoryEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new ContentCategoryEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ContentCategoryEntity ConvertToEntity(DataRow row)
        {
            ContentCategoryEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ContentCategoryEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_ENG) && !row.IsNull(TITLE_ENG))
                    {
                        entity.TitleEN = row[TITLE_ENG].ToString().Trim();
                    }
                    entity.DataTextField = entity.TitleEN;

                    if (row.Table.Columns.Contains(PARENT_ID) && !row.IsNull(PARENT_ID))
                    {
                        entity.ParentID = Convert.ToInt32(row[PARENT_ID]);
                    }

                    if (row.Table.Columns.Contains(SORTORDER) && !row.IsNull(SORTORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORTORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ContentCategoryEntity> GetListCombobox()
        {
            var lst = new List<ContentCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var dt = DbHelper.ExecuteDataTable(_db, sql);

                lst = ConvertToEnumerable(dt, true);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
    }
}