﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class FaqService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Faq";
        private const string ITEMID = "ItemID";
        private const string QUESTION = "Question";
        private const string ANSWER = "ANSWER";
        private const string EMAIL = "Email";
        private const string ISACTIVE = "IsActive";
        private const string NAME = "Name";
        private const string CREATEDATE = "CreatedDate";
        private const string ORDER = "Order";

        public FaqService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }
        public List<FaqEntity> GetList(int count, bool IsActive = true)
        {
            var lst = new List<FaqEntity>();

            try
            {
                var sql = string.Empty;

                if (count == -1)
                {
                    sql = "SELECT * FROM {0} ORDER BY  [Order] ASC, ItemID ASC";

                    sql = string.Format(sql, TABLE_NAME);
                }
                else
                {
                    sql = "SELECT top {0} * FROM {1} ORDER BY  [Order] ASC, ItemID ASC";

                    sql = string.Format(sql, count, TABLE_NAME);
                }


                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        public FaqEntity GetItem(int itemId)
        {
            FaqEntity entity = null;

            try
            {
                var sql = "SELECT * FROM Faq WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertEntity(data.Rows[0]);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return entity;
        }
        public bool Destroy(FaqEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEMID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEMID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }
        public int Create(FaqEntity e)
        {
            try
            {


                var sql = "INSERT INTO {0}(ItemID,Question,Order,IsActive,Email,Name,CreatedDate) VALUES ({1} , N'{2}', {3},  {4}, N'{5}', N'{6}', GETDATE())";


                sql = string.Format(sql, TABLE_NAME, LastID() + 1, e.Question, e.Order, e.IsActive  ,e.Email , e.Name  );

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);


                return LastID();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return 0;
        }
        public bool Update(FaqEntity e)
        {
            try
            {

                var sql = "update {0} set Question = N'{1}', Answer = N'{2}', [Order] = '{3}',  IsActive = '{4}' ,Email = {5}, Name = '{6}' where ItemID = {7} ";

                sql = string.Format(sql, TABLE_NAME, e.Question, e.Answer, e.Order, e.IsActive, e.Email, e.Name ,e.ItemID);

                var cmd = _db.GetSqlStringCommand(sql);
                _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }

            return true;
        }
        public IEnumerable<FaqEntity> Read()
        {
            var lst = new List<FaqEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY [Order] ASC, ItemID DESC ";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        private List<FaqEntity> ConvertToEnumerable(DataTable dt)
        {
            var lst = new List<FaqEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    lst.AddRange(from DataRow row in dt.Rows select ConvertEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        public int LastID()
        {
            try
            {
                var sql = "SELECT TOP(1) ItemID FROM  {0} ORDER BY ItemID DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);
                string LastID = _db.ExecuteScalar(cmd).ToString();
                return int.Parse(LastID);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return 0;
        }
        private FaqEntity ConvertEntity(DataRow row)
        {
            FaqEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new FaqEntity();

                    if (row.Table.Columns.Contains(ITEMID) && !row.IsNull(ITEMID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEMID].ToString());
                    }


                    if (row.Table.Columns.Contains(QUESTION) && !row.IsNull(QUESTION))
                    {
                        entity.Question = row[QUESTION].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(ANSWER) && !row.IsNull(ANSWER))
                    {
                        entity.Answer = row[ANSWER].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(CREATEDATE) && !row.IsNull(CREATEDATE))
                    {
                        entity.CreateDate = Convert.ToDateTime(row[CREATEDATE]);
                    }
                    if (row.Table.Columns.Contains(ORDER) && !row.IsNull(ORDER))
                    {
                        entity.Order = Convert.ToInt32(row[ORDER]);
                    }
                    if (row.Table.Columns.Contains(NAME) && !row.IsNull(NAME))
                    {
                        entity.Name = row[NAME].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(EMAIL) && !row.IsNull(EMAIL))
                    {
                        entity.Email = row[EMAIL].ToString().Trim();
                    }
                    if (row.Table.Columns.Contains(ISACTIVE) && !row.IsNull(ISACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[ISACTIVE]);
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}