﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ProductCategoryService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "ProductCategory";
        private const string ITEM_ID = "ItemID";
        private const string TITLE_ENG = "TitleEN";
        private const string TITLE = "Title";
        private const string PRODUCT_CODE = "ProductCode";
        private const string PRODUCT_TYPE_ID = "ProductTypeId";
        private const string GROUP_ID = "GroupID";
        private const string SHORT_DESCRIPTION_ENG = "ShortDescriptionEN";
        private const string LONG_DESCRIPTION_ENG = "LongDescriptionEN";
        private const string SHORT_DESCRIPTION = "ShortDescription";
        private const string LONG_DESCRIPTION = "LongDescription";
        private const string SPEC_SUMMARY = "SpecSummary";
        private const string SPEC_SUMMARY_ENG = "SpecSummaryEN";
        private const string ICON_NAME = "IconName";
        private const string ICON_FOOTER = "IconFooter";
        private const string IMAGE_NAME = "ImageName";
        private const string BROCHURE_FILE_NAME = "BrochureFileName";
        private const string PARENT_ID = "ParentID";
        private const string BANNERS = "Banners";
        private const string SORT_ORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";
        private const string ALLOW_SHOW_BANNERS = "IsAllowShowBanner";
        private const string SPECIFICATIONS = "Specifications";
        private const string TECHNICAL_DESCRIPTION = "TechnicalDescription";
        private const string COLORS = "Colors";
        private const string SLIDE_IMAGES = "SlideImages";
        private const string SLIDE_PRODUCT_IMAGES = "SlideProductImages";
        private const string ALLOW_SHOW_INDEX = "IsAllowShowIndex";
        private const string RELATED_TO = "RelatedTo";
        private const string SUB_MODEL = "SubModel";
        private const string IS_SUB_MODEL = "IsSubModel";
        private const string REDIRECT_URL = "RedirectUrl";
        private const string EMAIL_SUPPORT = "EmailSupport";
        private const string META_KEYWORD = "MetaKeyword";
        private const string META_DESCRPTION = "MetaDescription";
        private const string LINK_BANNERS = "LinkBanners";

        private const string LINKDIENMAYXANH = "LinkDienMayXanh";
        private const string LINKNGUYENKIM = "LinkNguyenKim";
        private const string LINKTHIENHOA = "LinkThienHoa";
        private const string LINKCHOLON = "LinkChoLon";
        private const string LINKVIDEO = "LinkVideo";
        private const string THUMBVIDEO = "ThumbVideo";
        private const string PRICE = "Price";
        private const string PROMOTION = "Promotion";

        private const string SHORT_FIELDS = "ItemID, ProductCode, TitleEN, Title, ProductTypeId, IconName, ImageName, BrochureFileName, " +
                                            "ParentID, Banners, SortOrder, IsActive, Specifications, Colors, SlideImages, RedirectUrl, LinkBanners , Price, Promotion ";

        public ProductCategoryService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public bool UpdateDescription(int itemId, string language, string content)
        {
            var result = 0;

            try
            {
                var sql = "UPDATE ProductCategory SET LongDescription = @LongDescription WHERE ItemID = @ItemID ";

                if (language == GlobalConstants.LANGUAGE_EN)
                {
                    sql = "UPDATE ProductCategory SET LongDescriptionEng = @LongDescription WHERE ItemID = @ItemID ";
                }

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@LongDescription", DbType.String, content);
                
                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        public bool IsAllowShowBanner(int itemId)
        {
            var result = false;

            try
            {
                var sql = "SELECT IsAllowShowBanner FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    result = Convert.ToBoolean(dt.Rows[0]["IsAllowShowBanner"]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public List<ProductCategoryEntity> Read()
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public ProductCategoryEntity GetFirstItem(int categoryId)
        {
            ProductCategoryEntity entity = null;

            try
            {
                var sql = "SELECT TOP 1 * FROM {0} WHERE (ParentID = @ParentID) AND IsActive = 1 AND IsSubModel = 0 ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, categoryId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToFullEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

      
        public ProductModelEntity GetModelItem(int itemId)
        {
            ProductModelEntity entity = null;

            try
            {
                var sql = "SELECT {0}, ShortDescription, ShortDescriptionEN, Specifications FROM {1} WHERE ItemId = @ItemId";

                sql = string.Format(sql, SHORT_FIELDS, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemId", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToProductModel(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public ProductCategoryEntity GetItem(int itemId)
        {

            ProductCategoryEntity entity = null;

            try
            {
                
                var sql = "SELECT * FROM {0} WHERE ItemId = {1}";

                sql = string.Format(sql, TABLE_NAME , itemId);

                var cmd = _db.GetSqlStringCommand(sql);


                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public ProductCategoryEntity GetItemFullDetail(int itemId)
        {
            ProductCategoryEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemId = @ItemId";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemId", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToFullEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ListItemEntity> GetListByParentId(int parentId, bool isEmpty = true)
        {
            var lst = new List<ListItemEntity>();

            try
            {
                var sql = "SELECT ItemID, Title FROM {0} WHERE ParentID = @ParentID ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        var text = row["Title"].ToString();
                        var value = row["ItemID"].ToString();

                        lst.Add(new ListItemEntity(value, text));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListSubCategory(int productTypeId)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT {0} FROM {1} WHERE ParentID IN " +
                          "(" +
                          "     SELECT ItemID FROM ProductCategory WHERE ProductTypeId = @ProductTypeId  AND ParentID = 0" +
                          ") AND IsActive = 1 ORDER BY SortOrder ASC";

                sql = string.Format(sql, SHORT_FIELDS, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ProductTypeId", DbType.Int32, productTypeId);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListInCategory(int itemId)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT {0} FROM {1} WHERE ParentID = " +
                          "(SELECT ParentID FROM ProductCategory WHERE ItemID = @ItemID) AND IsActive = 1 " +
                          "ORDER BY SortOrder ASC";

                sql = string.Format(sql, SHORT_FIELDS, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetList()
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY TitleEN";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, true);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListRoot(int productTypeId, bool isActive = false)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE (ProductTypeId = @ProductTypeId) AND (ParentID = 0 OR ParentID IS NULL) " +
                          "ORDER BY SortOrder ASC, Title ASC";

                if (isActive)
                {
                    sql = "SELECT * FROM {0} WHERE (ProductTypeId = @ProductTypeId) AND (ParentID = 0 OR ParentID IS NULL) " +
                          "AND IsActive = 1 ORDER BY SortOrder ASC, Title ASC";
                }

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ProductTypeId", DbType.Int32, productTypeId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListRoot(bool isActive = false)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE (ParentID = 0 OR ParentID IS NULL) ORDER BY SortOrder ASC, Title ASC";

                if (isActive)
                {
                    sql = "SELECT * FROM {0} WHERE (ParentID = 0 OR ParentID IS NULL) AND IsActive = 1 " +
                          "ORDER BY SortOrder ASC, Title ASC";
                }

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }


        public List<ProductCategoryEntity> GetListActive(int parentId , int count =4)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                //var sql = "SELECT * FROM {0} WHERE ParentID = @ParentID AND IsActive = 1 And IsAllowShowIndex = 1 " +
                //          "ORDER BY SortOrder ASC, Title ASC";
                var sql = "SELECT  * FROM {0} WHERE ParentID = @ParentID AND IsActive = 1 " +
                              "ORDER BY SortOrder ASC, Title ASC";

                sql = string.Format(sql, TABLE_NAME);

                if ( count != 0 )
                {
                    sql = "SELECT top {0} * FROM {1} WHERE ParentID = @ParentID AND IsActive = 1 " +
                              "ORDER BY SortOrder ASC, Title ASC";
                    sql = string.Format(sql, count, TABLE_NAME);
                }

                

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];
                
                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListActiveRelated(int parentId, int count = 4, int cat_current = 0)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                //var sql = "SELECT * FROM {0} WHERE ParentID = @ParentID AND IsActive = 1 And IsAllowShowIndex = 1 " +
                //          "ORDER BY SortOrder ASC, Title ASC";
                var sql = "SELECT  * FROM {0} WHERE ParentID = @ParentID AND IsActive = 1 AND ItemID != {1} " +
                              "ORDER BY SortOrder ASC, Title ASC";

                sql = string.Format(sql, TABLE_NAME, parentId , cat_current);

                if (count != 0)
                {
                    sql = "SELECT top {0} * FROM {1} WHERE ParentID = @ParentID AND IsActive = 1 AND ItemID != {2} " +
                              "ORDER BY SortOrder ASC, Title ASC";
                    sql = string.Format(sql, count, TABLE_NAME, parentId, cat_current);
                }



                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetChilds(int parentId)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ParentID = @ParentID " +
                          "ORDER BY SortOrder ASC, Title ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListRelated(int parentId)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ParentID = @ParentID AND IsActive = 1 " +
                          "ORDER BY SortOrder ASC, Title ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListSubModels(int parentId)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ParentID = @ParentID AND IsActive = 1 " +
                          "ORDER BY SortOrder ASC, Title ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductCategoryEntity> GetListByType(int productTypeId)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ProductTypeID = @ProductTypeID " +
                          "AND (ParentID = 0 OR ParentID IS NULL)  " +
                          "AND IsActive = 1 ORDER BY SortOrder ASC, Title ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ProductTypeID", DbType.Int32, productTypeId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<string> GetListBanner(string banners)
        {
            var lst = new List<string>();

            try
            {
                if (!String.IsNullOrEmpty(banners))
                {
                    banners = banners.Substring(0, banners.Length - 1);
                    var temp = banners.Split(';');

                    foreach (var item in temp)
                    {
                         lst.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<BannerEntity> GetListBanner(int itemId)
        {
            var lst = new List<BannerEntity>();

            try
            {
                var sql = "SELECT ItemID, Banners FROM ProductCategory WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    var banners = data.Rows[0]["Banners"].ToString().Trim();

                    if (!string.IsNullOrEmpty(banners))
                    {
                        var tmp = banners.Split(';').ToList();

                        foreach (var item in tmp)
                        {
                            var banner = new BannerEntity
                            {
                                ImageName = item
                            };

                            lst.Add(banner);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<BannerEntity> GetListLinkBanners(int itemId)
        {
            var result = new List<BannerEntity>();

            try
            {
                var sql = "SELECT ItemID, LinkBanners FROM ProductCategory WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    var str = data.Rows[0]["LinkBanners"].ToString().Trim();

                    if (!string.IsNullOrEmpty(str))
                    {
                        var lst = str.Split(',').ToList();

                        if (lst.Any())
                        {
                            var bannerService = new BannerService();

                            foreach (var item in lst)
                            {
                                var banner = bannerService.GetItem(Convert.ToInt32(item));

                                if (banner != null)
                                {
                                    result.Add(banner);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public ProductCategoryEntity GetItemById(int id)
        {
            ProductCategoryEntity obj = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE {1} = @{1} ";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, id);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                if (dt != null && dt.Rows.Count > 0)
                {
                    obj = ConvertToEntity(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return obj;
        }

        public int Create(ProductCategoryEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ProductCategory_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + PARENT_ID, DbType.Int32, e.ParentID);
                _db.AddInParameter(cmd, "@" + PRODUCT_CODE, DbType.String, e.ProductCode);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION, DbType.String, e.ShortDescription);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION, DbType.String, e.LongDescription);

                _db.AddInParameter(cmd, "@" + TITLE_ENG, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION_ENG, DbType.String, e.ShortDescriptionEN);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION_ENG, DbType.String, e.LongDescriptionEN);
                
                _db.AddInParameter(cmd, "@" + ICON_NAME, DbType.String, e.IconName);
                _db.AddInParameter(cmd, "@" + ICON_FOOTER, DbType.String, e.IconFooter);
                _db.AddInParameter(cmd, "@" + IMAGE_NAME, DbType.String, e.ImageName);
                _db.AddInParameter(cmd, "@" + BROCHURE_FILE_NAME, DbType.String, e.BrochureFileName);

                _db.AddInParameter(cmd, "@" + BANNERS, DbType.String, e.Banners);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + ALLOW_SHOW_BANNERS, DbType.Boolean, e.IsAllowShowBanner);

                _db.AddInParameter(cmd, "@" + SPEC_SUMMARY, DbType.String, e.SpecSummary);
                _db.AddInParameter(cmd, "@" + SPEC_SUMMARY_ENG, DbType.String, e.SpecSummaryEN);
                _db.AddInParameter(cmd, "@" + SPECIFICATIONS, DbType.String, e.Specifications);
                _db.AddInParameter(cmd, "@" + TECHNICAL_DESCRIPTION, DbType.String, e.TechnicalDescription);
                _db.AddInParameter(cmd, "@" + COLORS, DbType.String, string.Empty);
                _db.AddInParameter(cmd, "@" + SLIDE_IMAGES, DbType.String, e.SlideImages);

                _db.AddInParameter(cmd, "@" + ALLOW_SHOW_INDEX, DbType.Boolean, e.IsAllowShowIndex);
                _db.AddInParameter(cmd, "@" + RELATED_TO, DbType.String, e.RelatedTo);
                _db.AddInParameter(cmd, "@" + GROUP_ID, DbType.Int32, e.GroupID);
                _db.AddInParameter(cmd, "@" + SUB_MODEL, DbType.String, e.SubModel);
                _db.AddInParameter(cmd, "@" + IS_SUB_MODEL, DbType.String, e.IsSubModel);
                _db.AddInParameter(cmd, "@" + REDIRECT_URL, DbType.String, e.RedirectUrl);
                _db.AddInParameter(cmd, "@" + EMAIL_SUPPORT, DbType.String, e.EmailSupport);

                _db.AddInParameter(cmd, "@" + META_KEYWORD, DbType.String, e.MetaKeyword);
                _db.AddInParameter(cmd, "@" + META_DESCRPTION, DbType.String, e.MetaDescription);

                _db.AddInParameter(cmd, "@" + LINK_BANNERS, DbType.String, e.LinkBanners);

                _db.AddInParameter(cmd, "@" + LINKDIENMAYXANH, DbType.String, CheckUri(e.LinkDienMayXanh));
                _db.AddInParameter(cmd, "@" + LINKNGUYENKIM, DbType.String, CheckUri(e.LinkNguyenKim));
                _db.AddInParameter(cmd, "@" + LINKTHIENHOA, DbType.String, CheckUri(e.LinkThienHoa));
                _db.AddInParameter(cmd, "@" + LINKCHOLON, DbType.String, CheckUri(e.LinkChoLon));
                _db.AddInParameter(cmd, "@" + LINKVIDEO, DbType.String, e.LinkVideo);
                _db.AddInParameter(cmd, "@" + THUMBVIDEO, DbType.String, e.ThumbVideo);


                _db.AddInParameter(cmd, "@" + PRICE, DbType.String, e.Price);
                _db.AddInParameter(cmd, "@" + PROMOTION, DbType.String, e.Promotion);

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }
        public string CheckUri(string uri)
        {
            string url = uri;
            if (url != "")
            {
                if (!url.StartsWith("http://", StringComparison.OrdinalIgnoreCase)) url = "http://" + url;
            }
            
            return url;
        }
        public bool Update(ProductCategoryEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ProductCategory_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + PARENT_ID, DbType.Int32, e.ParentID);
                _db.AddInParameter(cmd, "@" + PRODUCT_CODE, DbType.String, e.ProductCode);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title.Trim());
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION, DbType.String, e.ShortDescription);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION, DbType.String, e.LongDescription);

                _db.AddInParameter(cmd, "@" + TITLE_ENG, DbType.String, e.TitleEN.Trim());
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION_ENG, DbType.String, e.ShortDescriptionEN);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION_ENG, DbType.String, e.LongDescriptionEN);

                _db.AddInParameter(cmd, "@" + ICON_NAME, DbType.String, e.IconName);
                _db.AddInParameter(cmd, "@" + ICON_FOOTER, DbType.String, e.IconFooter);
                _db.AddInParameter(cmd, "@" + IMAGE_NAME, DbType.String, e.ImageName);
                _db.AddInParameter(cmd, "@" + BROCHURE_FILE_NAME, DbType.String, e.BrochureFileName);
                _db.AddInParameter(cmd, "@" + SLIDE_IMAGES, DbType.String, e.SlideImages);

                _db.AddInParameter(cmd, "@" + BANNERS, DbType.String, e.Banners);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + ALLOW_SHOW_BANNERS, DbType.Boolean, e.IsAllowShowBanner);

                _db.AddInParameter(cmd, "@" + SPEC_SUMMARY, DbType.String, e.SpecSummary);
                _db.AddInParameter(cmd, "@" + SPEC_SUMMARY_ENG, DbType.String, e.SpecSummaryEN);
                _db.AddInParameter(cmd, "@" + SPECIFICATIONS, DbType.String, e.Specifications);
                _db.AddInParameter(cmd, "@" + TECHNICAL_DESCRIPTION, DbType.String, e.TechnicalDescription);
                _db.AddInParameter(cmd, "@" + COLORS, DbType.String, string.Empty);

                _db.AddInParameter(cmd, "@" + ALLOW_SHOW_INDEX, DbType.Boolean, e.IsAllowShowIndex);
                _db.AddInParameter(cmd, "@" + RELATED_TO, DbType.String, e.RelatedTo);
                _db.AddInParameter(cmd, "@" + GROUP_ID, DbType.Int32, e.GroupID);
                _db.AddInParameter(cmd, "@" + SUB_MODEL, DbType.String, e.SubModel);
                _db.AddInParameter(cmd, "@" + IS_SUB_MODEL, DbType.String, e.IsSubModel);
                _db.AddInParameter(cmd, "@" + REDIRECT_URL, DbType.String, e.RedirectUrl);
                _db.AddInParameter(cmd, "@" + EMAIL_SUPPORT, DbType.String, e.EmailSupport);

                _db.AddInParameter(cmd, "@" + META_KEYWORD, DbType.String, e.MetaKeyword);
                _db.AddInParameter(cmd, "@" + META_DESCRPTION, DbType.String, e.MetaDescription);

                _db.AddInParameter(cmd, "@" + LINK_BANNERS, DbType.String, e.LinkBanners);

                _db.AddInParameter(cmd, "@" + LINKDIENMAYXANH, DbType.String, CheckUri(e.LinkDienMayXanh));
                _db.AddInParameter(cmd, "@" + LINKNGUYENKIM, DbType.String, CheckUri(e.LinkNguyenKim));
                _db.AddInParameter(cmd, "@" + LINKTHIENHOA, DbType.String, CheckUri(e.LinkThienHoa));
                _db.AddInParameter(cmd, "@" + LINKCHOLON, DbType.String, CheckUri(e.LinkChoLon));
                _db.AddInParameter(cmd, "@" + LINKVIDEO, DbType.String, e.LinkVideo);
                _db.AddInParameter(cmd, "@" + THUMBVIDEO, DbType.String, e.ThumbVideo);

                _db.AddInParameter(cmd, "@" + PRICE, DbType.String, e.Price);
                _db.AddInParameter(cmd, "@" + PROMOTION, DbType.String, e.Promotion);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        public bool Destroy(int id)
        {
            try
            {
                var sql = "DELETE FROM ProductCategory WHERE ParentID = @ItemID; " +
                          "DELETE FROM ProductCategory WHERE ItemID = @ItemID;";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, id);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE ProductCategory SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public List<ProductCategoryEntity> GetListRelated(List<int> lstIds)
        {
            var filter = Utils.ConvertFromListInt(lstIds);

            var lst = new List<ProductCategoryEntity>();

            try
            {
                var sql = "SELECT {0} FROM {1} WHERE AND IsActive = 1 AND ItemID IN ({2}) ORDER BY SortOrder ASC";

                sql = string.Format(sql, SHORT_FIELDS, TABLE_NAME, filter);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private List<ProductCategoryEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<ProductCategoryEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new ProductCategoryEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ProductCategoryEntity ConvertToEntity(DataRow row)
        {
            ProductCategoryEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ProductCategoryEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(PRODUCT_CODE) && !row.IsNull(PRODUCT_CODE))
                    {
                        entity.ProductCode = row[PRODUCT_CODE].ToString();
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    entity.DataTextField = entity.TitleEN;

                    if (row.Table.Columns.Contains(TITLE_ENG) && !row.IsNull(TITLE_ENG))
                    {
                        entity.TitleEN = row[TITLE_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(ICON_NAME) && !row.IsNull(ICON_NAME))
                    {
                        entity.IconName = row[ICON_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(ICON_FOOTER) && !row.IsNull(ICON_FOOTER))
                    {
                        entity.IconFooter = row[ICON_FOOTER].ToString();
                    }

                    if (row.Table.Columns.Contains(IMAGE_NAME) && !row.IsNull(IMAGE_NAME))
                    {
                        entity.ImageName = row[IMAGE_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKDIENMAYXANH) && !row.IsNull(LINKDIENMAYXANH))
                    {
                        entity.LinkDienMayXanh = row[LINKDIENMAYXANH].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKNGUYENKIM) && !row.IsNull(LINKNGUYENKIM))
                    {
                        entity.LinkNguyenKim = row[LINKNGUYENKIM].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKTHIENHOA) && !row.IsNull(LINKTHIENHOA))
                    {
                        entity.LinkThienHoa = row[LINKTHIENHOA].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKCHOLON) && !row.IsNull(LINKCHOLON))
                    {
                        entity.LinkChoLon = row[LINKCHOLON].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKVIDEO) && !row.IsNull(LINKVIDEO))
                    {
                        entity.LinkVideo = row[LINKVIDEO].ToString();
                    }
                    if (row.Table.Columns.Contains(THUMBVIDEO) && !row.IsNull(THUMBVIDEO))
                    {
                        entity.ThumbVideo = row[THUMBVIDEO].ToString();
                    }

                    if (row.Table.Columns.Contains(BROCHURE_FILE_NAME) && !row.IsNull(BROCHURE_FILE_NAME))
                    {
                        entity.BrochureFileName = row[BROCHURE_FILE_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(PRODUCT_TYPE_ID) && !row.IsNull(PRODUCT_TYPE_ID))
                    {
                        entity.ProductTypeId = Convert.ToInt32(row[PRODUCT_TYPE_ID]);
                    }

                    if (row.Table.Columns.Contains(GROUP_ID) && !row.IsNull(GROUP_ID))
                    {
                        entity.GroupID = Convert.ToInt32(row[GROUP_ID]);
                    }

                    if (row.Table.Columns.Contains(PARENT_ID) && !row.IsNull(PARENT_ID))
                    {
                        entity.ParentID = Convert.ToInt32(row[PARENT_ID]);
                    }

                    if (entity.ParentID != 0)
                    {
                        entity.ParentCategory = GetItem(entity.ParentID);
                    }

                    if (row.Table.Columns.Contains(REDIRECT_URL) && !row.IsNull(REDIRECT_URL))
                    {
                        entity.RedirectUrl = row[REDIRECT_URL].ToString();
                    }

                    if (row.Table.Columns.Contains(EMAIL_SUPPORT) && !row.IsNull(EMAIL_SUPPORT))
                    {
                        entity.EmailSupport = row[EMAIL_SUPPORT].ToString();
                    }

                    entity.ProductType = new ProductTypeService().GetItem(entity.ProductTypeId);

                    //if (row.Table.Columns.Contains(SHORT_DESCRIPTION) && !row.IsNull(SHORT_DESCRIPTION))
                    //{
                    //    entity.ShortDescription = row[SHORT_DESCRIPTION].ToString();
                    //}

                    //if (row.Table.Columns.Contains(LONG_DESCRIPTION) && !row.IsNull(LONG_DESCRIPTION))
                    //{
                    //    entity.LongDescription = row[LONG_DESCRIPTION].ToString();
                    //}

                    //if (row.Table.Columns.Contains(SHORT_DESCRIPTION_ENG) && !row.IsNull(SHORT_DESCRIPTION_ENG))
                    //{
                    //    entity.ShortDescriptionEN = row[SHORT_DESCRIPTION_ENG].ToString();
                    //}

                    //if (row.Table.Columns.Contains(LONG_DESCRIPTION_ENG) && !row.IsNull(LONG_DESCRIPTION_ENG))
                    //{
                    //    entity.LongDescriptionEN = row[LONG_DESCRIPTION_ENG].ToString();
                    //}

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(SPECIFICATIONS) && !row.IsNull(SPECIFICATIONS))
                    {
                        entity.Specifications = row[SPECIFICATIONS].ToString();
                    }

                    if (row.Table.Columns.Contains(TECHNICAL_DESCRIPTION) && !row.IsNull(TECHNICAL_DESCRIPTION))
                    {
                        entity.TechnicalDescription = row[TECHNICAL_DESCRIPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(SPEC_SUMMARY) && !row.IsNull(SPEC_SUMMARY))
                    {
                        entity.SpecSummary = row[SPEC_SUMMARY].ToString();
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }

                    if (row.Table.Columns.Contains(BANNERS) && !row.IsNull(BANNERS))
                    {
                        entity.Banners = row[BANNERS].ToString();
                    }

                    if (row.Table.Columns.Contains(PRICE) && !row.IsNull(PRICE))
                    {
                        entity.Price = row[PRICE].ToString();
                    }


                    if (row.Table.Columns.Contains(ALLOW_SHOW_BANNERS) && !row.IsNull(ALLOW_SHOW_BANNERS))
                    {
                        entity.IsAllowShowBanner = Convert.ToBoolean(row[ALLOW_SHOW_BANNERS]);
                    }
                    if (row.Table.Columns.Contains(SLIDE_PRODUCT_IMAGES) && !row.IsNull(SLIDE_PRODUCT_IMAGES))
                    {
                        entity.SlideProductImages = row[SLIDE_PRODUCT_IMAGES].ToString();

                        if (!string.IsNullOrEmpty(entity.SlideProductImages))
                        {
                            entity.ListProductImages = entity.SlideProductImages.Split(';').ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        private ProductCategoryEntity ConvertToFullEntity(DataRow row)
        {
            ProductCategoryEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ProductCategoryEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(PRODUCT_CODE) && !row.IsNull(PRODUCT_CODE))
                    {
                        entity.ProductCode = row[PRODUCT_CODE].ToString();
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    entity.DataTextField = entity.TitleEN;

                    if (row.Table.Columns.Contains(TITLE_ENG) && !row.IsNull(TITLE_ENG))
                    {
                        entity.TitleEN = row[TITLE_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(ICON_NAME) && !row.IsNull(ICON_NAME))
                    {
                        entity.IconName = row[ICON_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(ICON_FOOTER) && !row.IsNull(ICON_FOOTER))
                    {
                        entity.IconFooter = row[ICON_FOOTER].ToString();
                    }

                    if (row.Table.Columns.Contains(IMAGE_NAME) && !row.IsNull(IMAGE_NAME))
                    {
                        entity.ImageName = row[IMAGE_NAME].ToString();
                    }
                    if (row.Table.Columns.Contains(LINKDIENMAYXANH) && !row.IsNull(LINKDIENMAYXANH))
                    {
                        entity.LinkDienMayXanh = row[LINKDIENMAYXANH].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKNGUYENKIM) && !row.IsNull(LINKNGUYENKIM))
                    {
                        entity.LinkNguyenKim = row[LINKNGUYENKIM].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKTHIENHOA) && !row.IsNull(LINKTHIENHOA))
                    {
                        entity.LinkThienHoa = row[LINKTHIENHOA].ToString();
                    }

                    if (row.Table.Columns.Contains(LINKCHOLON) && !row.IsNull(LINKCHOLON))
                    {
                        entity.LinkChoLon = row[LINKCHOLON].ToString();
                    }
                    if (row.Table.Columns.Contains(LINKVIDEO) && !row.IsNull(LINKVIDEO))
                    {
                        entity.LinkVideo = row[LINKVIDEO].ToString();
                    }
                    if (row.Table.Columns.Contains(THUMBVIDEO) && !row.IsNull(THUMBVIDEO))
                    {
                        entity.ThumbVideo = row[THUMBVIDEO].ToString();
                    }

                    if (row.Table.Columns.Contains(BROCHURE_FILE_NAME) && !row.IsNull(BROCHURE_FILE_NAME))
                    {
                        entity.BrochureFileName = row[BROCHURE_FILE_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(PRODUCT_TYPE_ID) && !row.IsNull(PRODUCT_TYPE_ID))
                    {
                        entity.ProductTypeId = Convert.ToInt32(row[PRODUCT_TYPE_ID]);
                    }

                    if (row.Table.Columns.Contains(GROUP_ID) && !row.IsNull(GROUP_ID))
                    {
                        entity.GroupID = Convert.ToInt32(row[GROUP_ID]);
                    }

                    if (row.Table.Columns.Contains(PARENT_ID) && !row.IsNull(PARENT_ID))
                    {
                        entity.ParentID = Convert.ToInt32(row[PARENT_ID]);
                    }

                    if (entity.ParentID != 0)
                    {
                        entity.ParentCategory = GetItem(entity.ParentID);
                    }

                    entity.ProductType = new ProductTypeService().GetItem(entity.ProductTypeId);

                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION) && !row.IsNull(SHORT_DESCRIPTION))
                    {
                        entity.ShortDescription = row[SHORT_DESCRIPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCRIPTION) && !row.IsNull(LONG_DESCRIPTION))
                    {
                        entity.LongDescription = row[LONG_DESCRIPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION_ENG) && !row.IsNull(SHORT_DESCRIPTION_ENG))
                    {
                        entity.ShortDescriptionEN = row[SHORT_DESCRIPTION_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCRIPTION_ENG) && !row.IsNull(LONG_DESCRIPTION_ENG))
                    {
                        entity.LongDescriptionEN = row[LONG_DESCRIPTION_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }

                    if (row.Table.Columns.Contains(BANNERS) && !row.IsNull(BANNERS))
                    {
                        entity.Banners = row[BANNERS].ToString();

                        if (!string.IsNullOrEmpty(entity.Banners))
                        {
                            entity.ListBanners = entity.Banners.Split(';').ToList();
                        }
                    }

                    if (row.Table.Columns.Contains(ALLOW_SHOW_BANNERS) && !row.IsNull(ALLOW_SHOW_BANNERS))
                    {
                        entity.IsAllowShowBanner = Convert.ToBoolean(row[ALLOW_SHOW_BANNERS]);
                    }

                    if (row.Table.Columns.Contains(ALLOW_SHOW_INDEX) && !row.IsNull(ALLOW_SHOW_INDEX))
                    {
                        entity.IsAllowShowIndex = Convert.ToBoolean(row[ALLOW_SHOW_INDEX]);
                    }

                    if (row.Table.Columns.Contains(IS_SUB_MODEL) && !row.IsNull(IS_SUB_MODEL))
                    {
                        entity.IsSubModel = Convert.ToBoolean(row[IS_SUB_MODEL]);
                    }

                    if (row.Table.Columns.Contains(SPECIFICATIONS) && !row.IsNull(SPECIFICATIONS))
                    {
                        entity.Specifications = row[SPECIFICATIONS].ToString();
                    }

                    if (row.Table.Columns.Contains(TECHNICAL_DESCRIPTION) && !row.IsNull(TECHNICAL_DESCRIPTION))
                    {
                        entity.TechnicalDescription = row[TECHNICAL_DESCRIPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(REDIRECT_URL) && !row.IsNull(REDIRECT_URL))
                    {
                        entity.RedirectUrl = row[REDIRECT_URL].ToString();
                    }

                    if (row.Table.Columns.Contains(EMAIL_SUPPORT) && !row.IsNull(EMAIL_SUPPORT))
                    {
                        entity.EmailSupport = row[EMAIL_SUPPORT].ToString();
                    }
                    
                    entity.Colors = new ProductColorService().GetList(entity.ItemID);
                    
                    if (row.Table.Columns.Contains(SLIDE_IMAGES) && !row.IsNull(SLIDE_IMAGES))
                    {
                        entity.SlideImages = row[SLIDE_IMAGES].ToString();

                        if (!string.IsNullOrEmpty(entity.SlideImages))
                        {
                            entity.ListImages = entity.SlideImages.Split(';').ToList();
                        }
                    }

                    if (row.Table.Columns.Contains(SLIDE_PRODUCT_IMAGES) && !row.IsNull(SLIDE_PRODUCT_IMAGES))
                    {
                        entity.SlideProductImages = row[SLIDE_PRODUCT_IMAGES].ToString();

                        if (!string.IsNullOrEmpty(entity.SlideProductImages))
                        {
                            entity.ListProductImages = entity.SlideProductImages.Split(';').ToList();
                        }
                    }

                    entity.ListRelated = new List<ProductCategoryEntity>();

                    if (row.Table.Columns.Contains(RELATED_TO) && !row.IsNull(RELATED_TO))
                    {
                        var str = row[RELATED_TO].ToString().Trim();

                        if (!string.IsNullOrEmpty(str))
                        {
                            var lst = str.Split(',').ToList();

                            if (lst.Any())
                            {
                                foreach (var item in lst)
                                {
                                    if (!string.IsNullOrEmpty(item.Trim()))
                                    {
                                        var e = GetItem(Convert.ToInt32(item));

                                        if (e != null)
                                        {
                                            entity.ListRelated.Add(e);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (row.Table.Columns.Contains(SUB_MODEL) && !row.IsNull(SUB_MODEL))
                    {
                        var str = row[SUB_MODEL].ToString().Trim();

                        if (!string.IsNullOrEmpty(str))
                        {
                            var lst = str.Split(',').ToList();

                            if (lst.Any())
                            {
                                foreach (var item in lst)
                                {
                                    entity.ListSubModel.Add(GetModelItem(Convert.ToInt32(item)));
                                }
                            }
                        }
                    }

                    if (row.Table.Columns.Contains(LINK_BANNERS) && !row.IsNull(LINK_BANNERS))
                    {
                        var str = row[LINK_BANNERS].ToString().Trim();

                        if (!string.IsNullOrEmpty(str))
                        {
                            var lst = str.Split(',').ToList();

                            if (lst.Any())
                            {
                                var bannerService = new BannerService();


                                foreach (var item in lst)
                                {
                                    entity.ListLinkBanners.Add(bannerService.GetItem(Convert.ToInt32(item)));
                                }
                            }
                        }
                    }

                    if (row.Table.Columns.Contains(META_KEYWORD) && !row.IsNull(META_KEYWORD))
                    {
                        entity.MetaKeyword = row[META_KEYWORD].ToString();
                    }

                    if (row.Table.Columns.Contains(META_DESCRPTION) && !row.IsNull(META_DESCRPTION))
                    {
                        entity.MetaDescription = row[META_DESCRPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(PRICE) && !row.IsNull(PRICE))
                    {
                        entity.Price = row[PRICE].ToString();
                    }

                    if (row.Table.Columns.Contains(PROMOTION) && !row.IsNull(PROMOTION))
                    {
                        entity.Promotion = row[PROMOTION].ToString();
                    }


                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        private ProductModelEntity ConvertToProductModel(DataRow row)
        {
            ProductModelEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ProductModelEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(PRODUCT_CODE) && !row.IsNull(PRODUCT_CODE))
                    {
                        entity.ProductCode = row[PRODUCT_CODE].ToString();
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    entity.DataTextField = entity.TitleEN;

                    if (row.Table.Columns.Contains(TITLE_ENG) && !row.IsNull(TITLE_ENG))
                    {
                        entity.TitleEN = row[TITLE_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(ICON_NAME) && !row.IsNull(ICON_NAME))
                    {
                        entity.IconName = row[ICON_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(ICON_FOOTER) && !row.IsNull(ICON_FOOTER))
                    {
                        entity.IconFooter = row[ICON_FOOTER].ToString();
                    }

                    if (row.Table.Columns.Contains(IMAGE_NAME) && !row.IsNull(IMAGE_NAME))
                    {
                        entity.ImageName = row[IMAGE_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(BROCHURE_FILE_NAME) && !row.IsNull(BROCHURE_FILE_NAME))
                    {
                        entity.BrochureFileName = row[BROCHURE_FILE_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(PRODUCT_TYPE_ID) && !row.IsNull(PRODUCT_TYPE_ID))
                    {
                        entity.ProductTypeId = Convert.ToInt32(row[PRODUCT_TYPE_ID]);
                    }

                    if (row.Table.Columns.Contains(GROUP_ID) && !row.IsNull(GROUP_ID))
                    {
                        entity.GroupID = Convert.ToInt32(row[GROUP_ID]);
                    }

                    if (row.Table.Columns.Contains(PARENT_ID) && !row.IsNull(PARENT_ID))
                    {
                        entity.ParentID = Convert.ToInt32(row[PARENT_ID]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }

                    if (row.Table.Columns.Contains(BANNERS) && !row.IsNull(BANNERS))
                    {
                        entity.Banners = row[BANNERS].ToString();

                        if (!string.IsNullOrEmpty(entity.Banners))
                        {
                            entity.ListBanners = entity.Banners.Split(';').ToList();
                        }
                    }

                    if (row.Table.Columns.Contains(ALLOW_SHOW_BANNERS) && !row.IsNull(ALLOW_SHOW_BANNERS))
                    {
                        entity.IsAllowShowBanner = Convert.ToBoolean(row[ALLOW_SHOW_BANNERS]);
                    }

                    if (row.Table.Columns.Contains(SPECIFICATIONS) && !row.IsNull(SPECIFICATIONS))
                    {
                        entity.Specifications = row[SPECIFICATIONS].ToString();
                    }

                   

                    if (row.Table.Columns.Contains(SLIDE_IMAGES) && !row.IsNull(SLIDE_IMAGES))
                    {
                        entity.SlideImages = row[SLIDE_IMAGES].ToString();

                        if (!string.IsNullOrEmpty(entity.SlideImages))
                        {
                            entity.ListImages = entity.SlideImages.Split(';').ToList();
                        }
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION) && !row.IsNull(SHORT_DESCRIPTION))
                    {
                        entity.ShortDescription = row[SHORT_DESCRIPTION].ToString();
                    }


                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION_ENG) && !row.IsNull(SHORT_DESCRIPTION_ENG))
                    {
                        entity.ShortDescriptionEN = row[SHORT_DESCRIPTION_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(SPECIFICATIONS) && !row.IsNull(SPECIFICATIONS))
                    {
                        entity.Specifications = row[SPECIFICATIONS].ToString();
                    }

                    entity.Colors = new ProductColorService().GetList(entity.ItemID);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ListItemEntity> GetListComboBox(int productTypeId)
        {
            var lst = new List<ListItemEntity>();

            try
            {
                var sql = "SELECT ItemID, Title FROM ProductCategory WHERE ParentID = @ParentID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, productTypeId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        var text = row["Title"].ToString();
                        var value = row["ItemID"].ToString();

                        lst.Add(new ListItemEntity(value, text));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public ListItemEntity GetListItemEntity(int itemId)
        {
            try
            {
                var sql = "SELECT ItemID, Title FROM {0} WHERE ItemId = @ItemId";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemId", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    var text = data.Rows[0]["Title"].ToString();
                    var value = data.Rows[0]["ItemID"].ToString();

                    return new ListItemEntity(value, text);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return null;
        }
    }
}