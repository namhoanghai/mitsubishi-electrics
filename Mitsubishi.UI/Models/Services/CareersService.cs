﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class CareersService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Careers";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Title";
        private const string TITLE_EN = "TitleEN";
        private const string DESCRIPTION = "Description";
        private const string DESCRIPTION_EN = "DescriptionEN";
        private const string SORT_ORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";

        public CareersService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public IEnumerable<CareersEntity> Read()
        {
            var lst = new List<CareersEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<CareersEntity> GetList()
        {
            var lst = new List<CareersEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE IsActive = 1 ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE Careers SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public int Create(CareersEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Careers_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + DESCRIPTION, DbType.String, e.LongDescription);
                _db.AddInParameter(cmd, "@" + DESCRIPTION_EN, DbType.String, e.LongDescriptionEN);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.String, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.String, e.IsActive);

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result);
        }

        public bool Update(CareersEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Careers_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + DESCRIPTION, DbType.String, e.LongDescription);
                _db.AddInParameter(cmd, "@" + DESCRIPTION_EN, DbType.String, e.LongDescriptionEN);
                _db.AddInParameter(cmd, "@" + SORT_ORDER, DbType.String, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.String, e.IsActive);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result > 0;
        }

        public bool Destroy(CareersEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public CareersEntity GetItem(int itemId)
        {
            CareersEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
        
        private List<CareersEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<CareersEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new CareersEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private CareersEntity ConvertToEntity(DataRow row)
        {
            CareersEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new CareersEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString();
                    }

                    if (row.Table.Columns.Contains(DESCRIPTION) && !row.IsNull(DESCRIPTION))
                    {
                        entity.LongDescription = row[DESCRIPTION].ToString();
                    }

                    if (row.Table.Columns.Contains(DESCRIPTION_EN) && !row.IsNull(DESCRIPTION_EN))
                    {
                        entity.LongDescriptionEN = row[DESCRIPTION_EN].ToString();
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER].ToString());
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE].ToString());
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}