﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;


namespace Mitsubishi.UI.Models.Services
{
    public class ManualCategoryService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "ManualCategory";
        private const string ITEM_ID = "ItemID";
        private const string TITLE_EN = "TitleEN";
        private const string TITLE = "Title";
        private const string SORTORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";
        private const string PARENT_ID = "ParentID";
        private const string LEVEL = "Level";
        private const string THUMBNAIL = "Thumbnail";
        private const string META_KEYWORD = "MetaKeyword";
        private const string META_DESCRPTION = "MetaDescription";

        public ManualCategoryService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }
        public IEnumerable<ManualCategoryEntity> Read()
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} where TaxonomyId = 1";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public ManualCategoryEntity GetItemFullDetail(int itemId)
        {
            ManualCategoryEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemId = @ItemId";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemId", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ManualCategoryEntity> GetList()
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC, ModifiedDate DESC ";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
      
        public List<ManualCategoryEntity> GetListByLevel(int level)
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE level = {1} ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, TABLE_NAME, level );

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        //lấy dánh sách các ID TOPIC từ ROOT ID
        public string GetTopicByTypeProduct(int parentId)
        {
            var lstCat = new List<ManualCategoryEntity>();
            var lstTopic = new List<ManualCategoryEntity>();
            string output = "";
            lstCat = GetChilds(parentId);
           
            List<int> IntTopic = new List<int>();

           
            foreach (var item in lstCat)
            {
                lstTopic = GetChilds(item.ItemID);
                
                foreach(var item2 in lstTopic){
                  //IntTopic.Add(item2.ItemID);
                    output += item2.ItemID + ",";
                }
                
            }
            if (IntTopic.Count > 0)
            {
                output = output.Remove(output.Length - 1, 1);
            }
            


            return output;
        }


        public List<ManualCategoryEntity> GetChilds(int parentId)
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ParentID = @ParentID " +
                          "ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ParentID", DbType.Int32, parentId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public int Create(ManualCategoryEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ManualCategory_Create");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + LEVEL, DbType.Int32,e.Level);
                _db.AddInParameter(cmd, "@" + THUMBNAIL, DbType.String, e.Thumbnail);                
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + SORTORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + PARENT_ID, DbType.Int32, e.ParentID);
                _db.AddInParameter(cmd, "@" + META_KEYWORD, DbType.Int32, e.MetaKeyword);
                _db.AddInParameter(cmd, "@" + META_DESCRPTION, DbType.Int32, e.MetaDescription);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public bool Update(ManualCategoryEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ManualCategory_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + LEVEL, DbType.Int32, e.Level);
                _db.AddInParameter(cmd, "@" + THUMBNAIL, DbType.String, e.Thumbnail);
                _db.AddInParameter(cmd, "@" + PARENT_ID, DbType.Int32, e.ParentID);

                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title.Trim());
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN.Trim());
                _db.AddInParameter(cmd, "@" + SORTORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + META_KEYWORD, DbType.String, e.MetaKeyword);
                _db.AddInParameter(cmd, "@" + META_DESCRPTION, DbType.String, e.MetaDescription);


                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        public bool Destroy(int id)
        {
            try
            {
                var sql = "DELETE FROM ManualCategory WHERE ParentID = @ItemID; " +
                          "DELETE FROM ManualCategory WHERE ItemID = @ItemID;";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, id);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public bool SetIs_Active(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE ManualCategory SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }


        public ManualCategoryEntity GetItem(int itemId)
        {
            ManualCategoryEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemId = @ItemId";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemId", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE ManualCategoryEntity SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public List<ManualCategoryEntity> GetListRoot(bool isActive = false, int TaxonomyId = 0)
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE ParentID = 0 ";

                if (isActive)
                {
                    sql = "SELECT * FROM {0} WHERE ParentID = 0 AND IsActive = 1 ";
                }

                if (TaxonomyId != 0)
                {
                    sql = sql + " AND TaxonomyId = " + TaxonomyId + " "; 
                }
                sql = string.Format(sql, TABLE_NAME);


               

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ManualCategoryEntity> GetListCategory(string taxonomyId)
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} where TaxonomyId IN({1})  ORDER BY SortOrder ASC";

                sql = string.Format(sql, TABLE_NAME,taxonomyId);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }

                return lst;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private List<ManualCategoryEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new ManualCategoryEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ManualCategoryEntity ConvertToEntity(DataRow row)
        {
            ManualCategoryEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ManualCategoryEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString().Trim();
                    }
                    entity.DataTextField = entity.TitleEN;

                    if (row.Table.Columns.Contains(PARENT_ID) && !row.IsNull(PARENT_ID))
                    {
                        entity.ParentID = Convert.ToInt32(row[PARENT_ID]);
                    }

                    if (entity.ParentID != 0)
                    {
                        entity.ParentCategory = GetItem(entity.ParentID);
                    }

                    if (row.Table.Columns.Contains(SORTORDER) && !row.IsNull(SORTORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORTORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }

                    if (row.Table.Columns.Contains(THUMBNAIL) && !row.IsNull(THUMBNAIL))
                    {
                        entity.Thumbnail = row[THUMBNAIL].ToString().Trim(); 
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ManualCategoryEntity> GetListCombobox()
        {
            var lst = new List<ManualCategoryEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC, ModifiedDate DESC";

                sql = string.Format(sql, TABLE_NAME);

                var dt = DbHelper.ExecuteDataTable(_db, sql);

                lst = ConvertToEnumerable(dt, true);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
    }

}