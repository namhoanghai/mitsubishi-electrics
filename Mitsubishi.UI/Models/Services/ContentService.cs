﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ContentService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Content";
        private const string ITEM_ID = "ItemID";
        private const string TITLE = "Title";
        private const string TITLE_EN = "TitleEN";
        private const string SHORT_DESCRIPTION = "ShortDescription";
        private const string SHORT_DESCRIPTION_EN = "ShortDescriptionEN";
        private const string LONG_DESCRIPTION = "LongDescription";
        private const string LONG_DESCRIPTION_EN = "LongDescriptionEN";
        private const string THUMBNAIL = "Thumbnail";
        private const string SORTORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";
        private const string CATEGORY_ID = "CategoryID";
        private const string ATTACH_FILENAME = "AttachFileName";
        private const string IMAGE_CONTENT = "ListImages";
        private const string ALLOW_SHOW_GALLERY = "IsAllowShowGallery";

        public ContentService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public ContentEntity GetItem(int itemId)
        {
            ContentEntity entity = null;

            try
            {
                var sql = "SELECT * FROM Content WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToFullEntity(data.Rows[0]);
                }
            }
            catch (Exception)
            {
                
                throw;
            }

            return entity;
        }

        public IEnumerable<ContentEntity> Read()
        {
            var lst = new List<ContentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC, ItemID DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ContentEntity> GetList()
        {
            var lst = new List<ContentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC, ItemID DESC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }
        //lấy danh sách bài viết liên quan, loại bỏ bài hiện tại
        public List<ContentEntity> GetListRelated(int nid, string cats, int count, string orderby = "ItemID", string order = "ASC", bool random = false)
        {
            var lst = new List<ContentEntity>();

            try
            {
                var sql = "";
                //random 
                if (random == false)
                {
                    
                    sql = "SELECT TOP {0}  * FROM {1} where CategoryID IN ({2}) AND IsActive = 1  AND ItemID != {3}  ORDER BY {4} {5}";
                    sql = string.Format(sql, count, TABLE_NAME, cats, nid, orderby, order);
                }
                else
                {
                    sql = "SELECT TOP {0}  * FROM {1} where CategoryID IN ({2}) AND IsActive = 1  AND ItemID != {3}  ORDER BY newid()";
                    sql = string.Format(sql, count, TABLE_NAME, cats, nid);
                }

               
                

                var cmd = _db.GetSqlStringCommand(sql);
                
                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public ContentEntity GetFirstItem(string filter)
        {

            ContentEntity entity = null;

            try
            {
                var sql = "SELECT TOP 1 * FROM {0} WHERE CategoryID IN ({1}) AND IsActive = 1 ORDER BY SortOrder ASC, ItemID DESC";

                sql = string.Format(sql, TABLE_NAME, filter);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];


                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToFullEntity(data.Rows[0]);
                }
                
              
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<ContentEntity> GetList(string filter)
        {
            var lst = new List<ContentEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE CategoryID IN ({1}) AND IsActive = 1 ORDER BY SortOrder ASC, ItemID DESC";

                sql = string.Format(sql, TABLE_NAME, filter);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public bool Destroy(ContentEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public bool SetIsActive(int itemId, bool isActive)
        {
            try
            {
                var sql = "UPDATE Content SET IsActive = @IsActive, ModifiedDate = GETDATE() WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, itemId);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, isActive);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public int Create(ContentEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Content_Create");

                _db.AddOutParameter(cmd, "@" + ITEM_ID, DbType.Int32, 16);
                _db.AddInParameter(cmd, "@" + CATEGORY_ID, DbType.Int32, e.CategoryID);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION, DbType.String, e.ShortDescription);
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION_EN, DbType.String, e.ShortDescriptionEN);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION, DbType.String, e.LongDescription);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION_EN, DbType.String, e.LongDescriptionEN);
                _db.AddInParameter(cmd, "@" + THUMBNAIL, DbType.String, e.Thumbnail);
                _db.AddInParameter(cmd, "@" + ATTACH_FILENAME, DbType.String, e.AttachFileName);
                _db.AddInParameter(cmd, "@" + IMAGE_CONTENT, DbType.String, e.SlideImages);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + SORTORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + ALLOW_SHOW_GALLERY, DbType.Boolean, e.IsAllowShowGallery);

                if (_db.ExecuteNonQuery(cmd) > 0)
                {
                    result = Convert.ToInt32(_db.GetParameterValue(cmd, "@" + ITEM_ID));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return result;
        }

        public bool Update(ContentEntity e)
        {
            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Content_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + CATEGORY_ID, DbType.Int32, e.CategoryID);
                _db.AddInParameter(cmd, "@" + TITLE_EN, DbType.String, e.TitleEN);
                _db.AddInParameter(cmd, "@" + TITLE, DbType.String, e.Title);
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION, DbType.String, e.ShortDescription);
                _db.AddInParameter(cmd, "@" + SHORT_DESCRIPTION_EN, DbType.String, e.ShortDescriptionEN);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION, DbType.String, e.LongDescription);
                _db.AddInParameter(cmd, "@" + LONG_DESCRIPTION_EN, DbType.String, e.LongDescriptionEN);
                _db.AddInParameter(cmd, "@" + THUMBNAIL, DbType.String, e.Thumbnail);
                _db.AddInParameter(cmd, "@" + ATTACH_FILENAME, DbType.String, e.AttachFileName);
                _db.AddInParameter(cmd, "@" + IMAGE_CONTENT, DbType.String, e.SlideImages);
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + SORTORDER, DbType.Int32, e.SortOrder);
                _db.AddInParameter(cmd, "@" + ALLOW_SHOW_GALLERY, DbType.Boolean, e.IsAllowShowGallery);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result > 0);
        }

        private List<ContentEntity> ConvertToEnumerable(DataTable dt)
        {
            var lst = new List<ContentEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    lst.AddRange(from DataRow row in dt.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ContentEntity ConvertToEntity(DataRow row)
        {
            ContentEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ContentEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(CATEGORY_ID) && !row.IsNull(CATEGORY_ID))
                    {
                        entity.CategoryID = Convert.ToInt32(row[CATEGORY_ID].ToString());

                        entity.Category = new ContentCategoryService().GetItem(entity.CategoryID);
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION) && !row.IsNull(SHORT_DESCRIPTION))
                    {
                        entity.ShortDescription = row[SHORT_DESCRIPTION].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION_EN) && !row.IsNull(SHORT_DESCRIPTION_EN))
                    {
                        entity.ShortDescriptionEN = row[SHORT_DESCRIPTION_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(THUMBNAIL) && !row.IsNull(THUMBNAIL))
                    {
                        entity.Thumbnail = row[THUMBNAIL].ToString().Trim();
                    }
                    //if (row.Table.Columns.Contains(LONG_DESCRIPTION) && !row.IsNull(LONG_DESCRIPTION))
                    //{
                    //    entity.LongDescription = row[LONG_DESCRIPTION].ToString().Trim();
                    //}

                    //if (row.Table.Columns.Contains(LONG_DESCRIPTION_EN) && !row.IsNull(LONG_DESCRIPTION_EN))
                    //{
                    //    entity.LongDescriptionEN = row[LONG_DESCRIPTION_EN].ToString().Trim();
                    //}

                    if (row.Table.Columns.Contains(SORTORDER) && !row.IsNull(SORTORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORTORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(ALLOW_SHOW_GALLERY) && !row.IsNull(ALLOW_SHOW_GALLERY))
                    {
                        entity.IsAllowShowGallery = Convert.ToBoolean(row[ALLOW_SHOW_GALLERY]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        private ContentEntity ConvertToFullEntity(DataRow row)
        {
            ContentEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ContentEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(CATEGORY_ID) && !row.IsNull(CATEGORY_ID))
                    {
                        entity.CategoryID = Convert.ToInt32(row[CATEGORY_ID].ToString());

                        entity.Category = new ContentCategoryService().GetItem(entity.CategoryID);
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(TITLE_EN) && !row.IsNull(TITLE_EN))
                    {
                        entity.TitleEN = row[TITLE_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION) && !row.IsNull(SHORT_DESCRIPTION))
                    {
                        entity.ShortDescription = row[SHORT_DESCRIPTION].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(SHORT_DESCRIPTION_EN) && !row.IsNull(SHORT_DESCRIPTION_EN))
                    {
                        entity.ShortDescriptionEN = row[SHORT_DESCRIPTION_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCRIPTION) && !row.IsNull(LONG_DESCRIPTION))
                    {
                        entity.LongDescription = row[LONG_DESCRIPTION].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(LONG_DESCRIPTION_EN) && !row.IsNull(LONG_DESCRIPTION_EN))
                    {
                        entity.LongDescriptionEN = row[LONG_DESCRIPTION_EN].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(SORTORDER) && !row.IsNull(SORTORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORTORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(ALLOW_SHOW_GALLERY) && !row.IsNull(ALLOW_SHOW_GALLERY))
                    {
                        entity.IsAllowShowGallery = Convert.ToBoolean(row[ALLOW_SHOW_GALLERY]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                    if (row.Table.Columns.Contains(THUMBNAIL) && !row.IsNull(THUMBNAIL))
                    {
                        entity.Thumbnail = row[THUMBNAIL].ToString().Trim();
                    }

                    if (row.Table.Columns.Contains(IMAGE_CONTENT) && !row.IsNull(IMAGE_CONTENT))
                    {
                        entity.ImagesContent = row[IMAGE_CONTENT].ToString();

                        if (!string.IsNullOrEmpty(entity.ImagesContent))
                        {
                            entity.ListImages = entity.ImagesContent.Split(';').ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}