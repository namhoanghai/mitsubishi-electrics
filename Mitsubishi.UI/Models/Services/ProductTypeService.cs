﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class ProductTypeService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "ProductType";
        private const string ITEM_ID = "ItemID";
        private const string NAME_ENG = "NameEng";
        private const string NAME = "Name";
        private const string IMAGE_NAME = "ImageName";
        private const string IMAGE_CONTENT = "ListImages";
        private const string SORT_ORDER = "SortOrder";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";

        public ProductTypeService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public IEnumerable<ProductTypeEntity> Read()
        {
            var lst = new List<ProductTypeEntity>();

            try
            {
                var sql = "SELECT * FROM {0}";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<ProductTypeEntity> GetList()
        {
            var lst = new List<ProductTypeEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY SortOrder ASC, Name ASC";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    lst.AddRange(from DataRow row in data.Rows select ConvertToEntity(row));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public int Create(ProductTypeEntity e)
        {

            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_ProductType_Insert_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + NAME_ENG, DbType.String, e.TitleEN.Trim());
                _db.AddInParameter(cmd, "@" + NAME, DbType.String, e.Title.Trim());
                _db.AddInParameter(cmd, "@" + IS_ACTIVE, DbType.Boolean, e.IsActive);
                _db.AddInParameter(cmd, "@" + IMAGE_NAME, DbType.String, e.ImageName);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result);
        }

        public bool Destroy(ProductTypeEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public ProductTypeEntity GetItem(int itemId)
        {
            ProductTypeEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public ProductTypeEntity GetItemByCategory(int categoryId)
        {
            ProductTypeEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = (SELECT ProductTypeId FROM ProductCategory WHERE ItemID = @CategoryID)";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@CategoryID", DbType.Int32, categoryId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
        
        private List<ProductTypeEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<ProductTypeEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new ProductTypeEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private ProductTypeEntity ConvertToEntity(DataRow row)
        {
            ProductTypeEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new ProductTypeEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(NAME_ENG) && !row.IsNull(NAME_ENG))
                    {
                        entity.TitleEN = row[NAME_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(NAME) && !row.IsNull(NAME))
                    {
                        entity.Title = row[NAME].ToString();
                    }

                    entity.DataTextField = entity.Title;

                    if (row.Table.Columns.Contains(IMAGE_NAME) && !row.IsNull(IMAGE_NAME))
                    {
                        entity.ImageName = row[IMAGE_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(IMAGE_CONTENT) && !row.IsNull(IMAGE_CONTENT))
                    {
                        entity.ImagesContent = row[IMAGE_CONTENT].ToString();

                        if (!string.IsNullOrEmpty(entity.ImagesContent))
                        {
                            entity.ListImages = entity.ImagesContent.Split(';').ToList();
                        }
                    }

                    if (row.Table.Columns.Contains(SORT_ORDER) && !row.IsNull(SORT_ORDER))
                    {
                        entity.SortOrder = Convert.ToInt32(row[SORT_ORDER]);
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}