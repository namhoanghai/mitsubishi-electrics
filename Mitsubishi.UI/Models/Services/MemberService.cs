﻿using System;
using System.Collections.Generic;
using System.Data;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;
using System.Text;
using System.Security.Cryptography;

namespace Mitsubishi.UI.Models.Services
{
    public class MemberService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Member";
        private const string ITEM_ID = "ItemID";
        private const string USERNAME = "UserName";
        private const string PASSWORD = "Password";
        private const string FULL_NAME = "FullName";
        private const string IS_ADMIN = "IsAdmin";
        private const string IS_PUBLISHER = "IsPublisher";
        private const string CREATED_DATE = "CreatedDate";
        private const string MODIFIED_DATE = "ModifiedDate";

        public MemberService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public IEnumerable<MemberEntity> Read()
        {
            var lst = new List<MemberEntity>();

            try
            {
                var sql = "SELECT * FROM {0}";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                lst = ConvertToEnumerable(dt, false);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public List<MemberEntity> GetList()
        {
            var lst = new List<MemberEntity>();

            try
            {
                var sql = "SELECT * FROM {0} ORDER BY Name";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        public int Create(MemberEntity e)
        {

            var result = 0;

            try
            {
                var cmd = _db.GetStoredProcCommand("sp_Member_Insert_Update");

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);
                _db.AddInParameter(cmd, "@" + USERNAME, DbType.String, e.UserName.Trim());
                _db.AddInParameter(cmd, "@" + PASSWORD, DbType.String, e.Password.Trim());
                _db.AddInParameter(cmd, "@" + FULL_NAME, DbType.String, e.FullName.Trim());
                _db.AddInParameter(cmd, "@" + IS_ADMIN, DbType.Boolean, e.IsAdmin);
                _db.AddInParameter(cmd, "@" + IS_PUBLISHER, DbType.Boolean, e.IsPublisher);

                result = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return (result);
        }

        public bool Destroy(MemberEntity e)
        {
            try
            {
                var sql = "DELETE FROM {0} WHERE {1} = @{1}";

                sql = string.Format(sql, TABLE_NAME, ITEM_ID);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + ITEM_ID, DbType.Int32, e.ItemID);

                var result = _db.ExecuteNonQuery(cmd);

                return (result > 0);

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return false;
        }

        public MemberEntity GetItem(int itemId)
        {
            MemberEntity entity = null;

            try
            {
                var sql = "SELECT * FROM {0} WHERE ItemID = @ItemID";

                sql = string.Format(sql, TABLE_NAME);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public int GetCurrentUserId(string username, string password)
        {
            try
            {
                var sql = "SELECT {0} FROM {1} WHERE {2} = @{2} AND {3} = @{3}";

                sql = string.Format(sql, ITEM_ID, TABLE_NAME, USERNAME, PASSWORD);

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@" + USERNAME, DbType.String, username);
                _db.AddInParameter(cmd, "@" + PASSWORD, DbType.String, password);

                var dt = _db.ExecuteDataSet(cmd).Tables[0];

                if (dt.Rows.Count > 0)
                {
                    var row = dt.Rows[0];

                    return Convert.ToInt32(row[ITEM_ID]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return 0;

        }

        

        private List<MemberEntity> ConvertToEnumerable(DataTable dt, bool insertEmpty)
        {
            var lst = new List<MemberEntity>();

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }

                    if (insertEmpty)
                    {
                        lst.Insert(0, new MemberEntity());
                    }

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private MemberEntity ConvertToEntity(DataRow row)
        {
            MemberEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new MemberEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(USERNAME) && !row.IsNull(USERNAME))
                    {
                        entity.UserName = row[USERNAME].ToString();
                    }

                    if (row.Table.Columns.Contains(PASSWORD) && !row.IsNull(PASSWORD))
                    {
                        entity.Password = row[PASSWORD].ToString();
                    }

                    if (row.Table.Columns.Contains(FULL_NAME) && !row.IsNull(FULL_NAME))
                    {
                        entity.FullName = row[FULL_NAME].ToString();
                    }

                    if (row.Table.Columns.Contains(IS_ADMIN) && !row.IsNull(IS_ADMIN))
                    {
                        entity.IsAdmin = Convert.ToBoolean(row[IS_ADMIN]);
                    }

                    if (row.Table.Columns.Contains(IS_PUBLISHER) && !row.IsNull(IS_PUBLISHER))
                    {
                        entity.IsPublisher = Convert.ToBoolean(row[IS_PUBLISHER]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}