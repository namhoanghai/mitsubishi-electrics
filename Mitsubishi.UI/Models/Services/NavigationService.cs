﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using log4net;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Mitsubishi.UI.Models.Entities;

namespace Mitsubishi.UI.Models.Services
{
    public class NavigationService
    {
        private readonly ILog _logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);
        private readonly Database _db;

        private const string TABLE_NAME = "Navigation";
        private const string ITEM_ID = "ItemID";
        private const string PARENT_ID = "ParentID";
        private const string TITLE_ENG = "TitleEng";
        private const string TITLE = "Title";
        private const string NAVIGATE_URL = "NavigateUrl";
        private const string BANNER_IMAGE = "BannerImage";
        private const string IS_ACTIVE = "IsActive";
        private const string CREATED_DATE = "Created";
        private const string MODIFIED_DATE = "Modified";

        public NavigationService()
        {
            _db = DatabaseFactory.CreateDatabase();
        }

        public NavigationEntity GetItem(int itemId)
        {
            NavigationEntity entity = null;

            try
            {
                var sql = "SELECT * FROM Navigation WHERE ItemID = @ItemID";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@ItemID", DbType.Int32, itemId);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public NavigationEntity GetItem(string navigateUrl)
        {
            NavigationEntity entity = null;

            try
            {
                var sql = "SELECT * FROM Navigation WHERE NavigateUrl = @NavigateUrl";

                var cmd = _db.GetSqlStringCommand(sql);

                _db.AddInParameter(cmd, "@NavigateUrl", DbType.String, navigateUrl);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null && data.Rows.Count > 0)
                {
                    entity = ConvertToEntity(data.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }

        public List<NavigationEntity> GetList(int parentId = 0)
        {
            var lst = new List<NavigationEntity>();

            try
            {
                var sql = "SELECT * FROM {0} WHERE IsActive = 1 AND (ParentID IS NULL OR ParentID = 0) ORDER BY SortOrder ASC";
                
                sql = string.Format(sql, TABLE_NAME);

                if (parentId != 0)
                {
                    sql = "SELECT * FROM {0} WHERE IsActive = 1 AND (ParentID = {1}) ORDER BY SortOrder ASC";

                    sql = string.Format(sql, TABLE_NAME, parentId);
                }

                var cmd = _db.GetSqlStringCommand(sql);

                var data = _db.ExecuteDataSet(cmd).Tables[0];

                if (data != null)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        lst.Add(ConvertToEntity(row));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return lst;
        }

        private NavigationEntity ConvertToEntity(DataRow row)
        {
            NavigationEntity entity = null;

            try
            {
                if (row != null)
                {
                    entity = new NavigationEntity();

                    if (row.Table.Columns.Contains(ITEM_ID) && !row.IsNull(ITEM_ID))
                    {
                        entity.ItemID = Convert.ToInt32(row[ITEM_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(PARENT_ID) && !row.IsNull(PARENT_ID))
                    {
                        entity.ParentID = Convert.ToInt32(row[PARENT_ID].ToString());
                    }

                    if (row.Table.Columns.Contains(TITLE_ENG) && !row.IsNull(TITLE_ENG))
                    {
                        entity.TitleEng = row[TITLE_ENG].ToString();
                    }

                    if (row.Table.Columns.Contains(TITLE) && !row.IsNull(TITLE))
                    {
                        entity.Title = row[TITLE].ToString();
                    }

                    if (row.Table.Columns.Contains(NAVIGATE_URL) && !row.IsNull(NAVIGATE_URL))
                    {
                        entity.NavigateUrl = row[NAVIGATE_URL].ToString();
                    }

                    if (row.Table.Columns.Contains(BANNER_IMAGE) && !row.IsNull(BANNER_IMAGE))
                    {
                        entity.BannerImage = row[BANNER_IMAGE].ToString();
                    }

                    if (row.Table.Columns.Contains(IS_ACTIVE) && !row.IsNull(IS_ACTIVE))
                    {
                        entity.IsActive = Convert.ToBoolean(row[IS_ACTIVE]);
                    }

                    if (row.Table.Columns.Contains(CREATED_DATE) && !row.IsNull(CREATED_DATE))
                    {
                        entity.CreatedDate = Convert.ToDateTime(row[CREATED_DATE]);
                    }

                    if (row.Table.Columns.Contains(MODIFIED_DATE) && !row.IsNull(MODIFIED_DATE))
                    {
                        entity.ModifiedDate = Convert.ToDateTime(row[MODIFIED_DATE]);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return entity;
        }
    }
}