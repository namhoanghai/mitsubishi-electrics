﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

using System.Text.RegularExpressions;
using log4net;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Models
{
    public class Utils
    {
        private static readonly ILog Logger = LogManager.GetLogger(GlobalConstants.APPLICATION_LOG);

        public static string StripTag(string str)
        {
            str = str.Replace("\"", "&quot;");
            str = str.Replace("\'", "&apos;");
            str = str.Replace("“", "&quot;");
            str = str.Replace("”", "&quot;");
            return Regex.Replace(str, @"<.*?>", string.Empty);
        }

        public static bool SendTipQuestion(string From ,string subject, string body)
        {
            var result = false;

            var config = new ParamConfigService();

            var mailMessage = new MailMessage();

            var emailTo = config.GetItem("TIP_EMAIL").ParamValue;

            var smtpHost = config.GetItem("SMTP_HOST").ParamValue;
            var smtpPort = config.GetItem("SMTP_PORT").ParamValue;
            var authenName = config.GetItem("AUTHENTICATION_NAME").ParamValue;
            var authenPassword = config.GetItem("AUTHENTICATION_PASSWORD").ParamValue;

            var mailClient = new SmtpClient(smtpHost, Convert.ToInt16(smtpPort));

            mailClient.UseDefaultCredentials = false;
            mailClient.EnableSsl = true;
            mailClient.Credentials = new NetworkCredential(authenName, authenPassword);
            mailClient.Timeout = 100000;

            mailMessage.IsBodyHtml = true;
            mailMessage.From = new MailAddress(authenName);
            mailMessage.Subject = subject;
            mailMessage.Body = !string.IsNullOrEmpty(body) ? body : string.Empty;

           

            if (emailTo.Trim() != "")
            {
                var arr = emailTo.Split(';');


                foreach (var email in arr)
                {
                    mailMessage.To.Clear();

                    mailMessage.To.Add(email);

                    try
                    {
                        mailClient.Send(mailMessage);

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                        result = false;
                    }
                }
                //gửi cho khách hàng thông báo
                mailMessage.To.Clear();
                mailMessage.To.Add(From);
                mailMessage.Subject = "Mitsubishi Electric Việt Nam";
                mailMessage.Body = "Cảm ơn bạn đã đặt câu hỏi tới Mitsubishi Electric Việt Nam. <br />Chúng tôi sẽ phản hồi sớm nhất.<br />Cảm ơn!";
                mailClient.Send(mailMessage);
            }

            return result;
        }

        public static bool SendInquiry(string subject, string body, string attachmentFile = "")
        {
            var result = false;
            
            var config = new ParamConfigService();

            var mailMessage = new MailMessage();

            var emailTo = config.GetItem("INQUIRY_EMAIL").ParamValue;

            var smtpHost = config.GetItem("SMTP_HOST").ParamValue;
            var smtpPort = config.GetItem("SMTP_PORT").ParamValue;
            var authenName = config.GetItem("AUTHENTICATION_NAME").ParamValue;
            var authenPassword = config.GetItem("AUTHENTICATION_PASSWORD").ParamValue;

            var mailClient = new SmtpClient(smtpHost);

            mailClient.UseDefaultCredentials = false;//cai nay`
            //neu = true, thi dung trong web.config
            //mac dinh no true.


            mailClient.EnableSsl = true;//google yeu cau
            mailClient.Credentials = new NetworkCredential(authenName, authenPassword);
            mailClient.Timeout = 100000;
            mailClient.Host = smtpHost;
            mailClient.Port = Convert.ToInt32(smtpPort);

            mailMessage.IsBodyHtml = true;
            mailMessage.From = new MailAddress(authenName);
            mailMessage.Subject = subject;
            mailMessage.Body = !string.IsNullOrEmpty(body) ? body : string.Empty;

            if (!string.IsNullOrEmpty(attachmentFile))
            {
                mailMessage.Attachments.Add(new Attachment(attachmentFile));
            }

            if (emailTo.Trim() != "")
            {
                var arr = emailTo.Split(';');

                foreach (var email in arr)
                {
                    mailMessage.To.Clear();

                    mailMessage.To.Add(email);

                    try
                    {
                        mailClient.Send(mailMessage);

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex);
                        result = false;
                    }
                }
            }

            return result;
        }

        //public static bool SendEmail(string subject, string body, string attachmentFile = "")
        //{
        //    var result = false;

        //    var service = new ServiceConfiguration();

        //    var mailMessage = new MailMessage();
        //    var emailTo = service.EmailTo;

        //    var mailClient = new SmtpClient(service.Smtp, Convert.ToInt16(service.Port))
        //    {
        //        UseDefaultCredentials = false,
        //        EnableSsl = false
        //    };

        //    if (!string.IsNullOrEmpty(service.UserName) && !string.IsNullOrEmpty(service.Password))
        //    {
        //        mailClient.Credentials = new NetworkCredential(service.UserName, service.Password);
        //    }
        //    else
        //    {
        //        mailClient.Credentials = CredentialCache.DefaultNetworkCredentials;
        //    }

        //    mailMessage.IsBodyHtml = true;
        //    mailMessage.From = new MailAddress(service.UserName);
        //    mailMessage.Subject = subject;
        //    mailMessage.Body = !string.IsNullOrEmpty(body) ? body : string.Empty;

        //    if (!string.IsNullOrEmpty(attachmentFile))
        //    {
        //        mailMessage.Attachments.Add(new Attachment(attachmentFile));
        //    }

        //    if (emailTo.Trim() != "")
        //    {
        //        var arr = emailTo.Split(';');

        //        foreach (var email in arr)
        //        {
        //            mailMessage.To.Clear();

        //            mailMessage.To.Add(email);

        //            try
        //            {
        //                mailClient.Send(mailMessage);

        //                result = true;
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.Error(ex);
        //                result = false;
        //            }
        //        }
        //    }

        //    return result;
        //}

        public static string ConvertUrf8ToAscii(string sConvert)
        {
            var reg = new Regex(@"á|à|ả|ã|ạ|â|ấ|ầ|ẩ|ẫ|ậ|ă|ắ|ằ|ẳ|ẵ|ặ");
            sConvert = reg.Replace(sConvert, "a");

            reg = new Regex(@"é|è|ẻ|ẽ|ẹ|ê|ê|ế|ề|ể|ễ|ệ");
            sConvert = reg.Replace(sConvert, "e");

            reg = new Regex(@"í|ì|ỉ|ĩ|ị");
            sConvert = reg.Replace(sConvert, "i");

            reg = new Regex(@"ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ");
            sConvert = reg.Replace(sConvert, "o");

            reg = new Regex(@"ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự");
            sConvert = reg.Replace(sConvert, "u");

            reg = new Regex(@"ý|ỳ|ỷ|ỹ|ỵ");
            sConvert = reg.Replace(sConvert, "y");

            reg = new Regex(@"đ");
            sConvert = reg.Replace(sConvert, "d");

            reg = new Regex(@"Á|À|Ả|Ã|Ạ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ");
            sConvert = reg.Replace(sConvert, "A");

            reg = new Regex(@"É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ");
            sConvert = reg.Replace(sConvert, "E");

            reg = new Regex(@"Í|Ì|Ỉ|Ĩ|Ị");
            sConvert = reg.Replace(sConvert, "I");

            reg = new Regex(@"Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ");
            sConvert = reg.Replace(sConvert, "O");

            reg = new Regex(@"Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự");
            sConvert = reg.Replace(sConvert, "U");

            reg = new Regex(@"Ý|Ỳ|Ỷ|Ỹ|Ỵ");
            sConvert = reg.Replace(sConvert, "Y");

            reg = new Regex(@"Đ");
            sConvert = reg.Replace(sConvert, "D");

            reg = new Regex(@"[^a-zA-Z0-9]");
            sConvert = reg.Replace(sConvert, "-");

            reg = new Regex(@"(-)\1+");
            sConvert = reg.Replace(sConvert, "-");

            return sConvert.ToLower();
        }

        public static string ConvertFromListInt(List<int> lstInt)
        {
            var str = String.Join(", ", lstInt.ToArray());

            return str;
        }

    }
}