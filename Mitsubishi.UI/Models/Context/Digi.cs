﻿using System.Data.Entity;


namespace Mitsubishi.UI.Models
{
    public class Digi:DbContext
    {
        public Digi() : base("DefaultConnection") { }
        public DbSet<Models.SystemConfig> SystemConfigs { get; set; }
    }
}