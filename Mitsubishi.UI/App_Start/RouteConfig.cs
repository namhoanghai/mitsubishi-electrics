﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Mitsubishi.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection context)
        {
            context.IgnoreRoute("{resource}.axd/{*pathInfo}");

            
            
            context.MapRoute(
                name: "NewsDetail",
                url: "news-detail/{title}-{itemId}.htm",
             defaults: new
             {
                 controller = "Content",
                 action = "NewsDetail"
             },
               namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "ProductTypes",
                url: "product-types/{ProductTypeName}-{productTypeId}.htm",
                defaults: new
                {
                    controller = "ProductType",
                    action = "Index",
                    productTypeId = UrlParameter.Optional
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "BusinessOwner",
                url: "dieu-hoa-khong-khi-du-an/chu-dau-tu.htm",
                defaults: new
                {
                    controller = "ProductType",
                    action = "BusinessOwner"
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );


           

            context.MapRoute(
                name: "SubProductCategory",
                url: "{productTypeName}/{parentCategoryName}/{subCategoryName}-{productCategoryId}.htm",
                defaults: new
                {
                    controller = "ProductCategory",
                    action = "Detail",
                    productCategoryId = UrlParameter.Optional
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "SubProductCategory2",
                url: "{productTypeName}/{parentCategoryName}/{subParentCategoryName}/{subCategoryName}-{productCategoryId}.htm",
                defaults: new
                {
                    controller = "ProductCategory",
                    action = "Detail",
                    productCategoryId = UrlParameter.Optional
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );



            context.MapRoute(
                name: "ProductCategory",
                url: "{ProductTypeName}-{ProductTypeId}/{ProductCategoryName}-{productCategoryId}.htm",
                defaults: new
                {
                    controller = "ProductCategory",
                    action = "Index",
                    productCategoryId = UrlParameter.Optional
                }
                , namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );


            context.MapRoute(
               name: "404page",
               url: "help/page-404",
               defaults: new
               {
                   controller = "Home",
                   action = "NotFoundPage"
               },
                 namespaces: new string[] { "Mitsubishi.UI.Controllers" }
           );

            context.MapRoute(
                name: "500page",
                url: "help/page-500",
                defaults: new
                {
                    controller = "Home",
                    action = "ErrorServer"
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "defaulterror",
                url: "help/page-error",
                defaults: new
                {
                    controller = "Home",
                    action = "ErrorDefault"
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "tips",
                url: "so-tay-gia-dung.htm",
                defaults: new
                {
                    controller = "Manual",
                    action = "Index"
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
               name: "video-playlist",
               url: "so-tay-gia-dung/playlist.htm",
               defaults: new
               {
                   controller = "Manual",
                   action = "Playlist"
               },
                 namespaces: new string[] { "Mitsubishi.UI.Controllers" }
           );

            context.MapRoute(
             name: "sotay-faq",
             url: "hoi-dap.htm",
             defaults: new
             {
                 controller = "Faq",
                 action = "DatCauHoi"
             },
               namespaces: new string[] { "Mitsubishi.UI.Controllers" }
         );

            context.MapRoute(
               name: "sotay-faq-form",
               url: "dat-cau-hoi.htm",
               defaults: new
               {
                   controller = "Faq",
                   action = "DatCauHoi"
               },
                 namespaces: new string[] { "Mitsubishi.UI.Controllers" }
           );
            context.MapRoute(
                name: "activation",
                url: "dia-diem-activation.htm",
                defaults: new
                {
                    controller = "Activation",
                    action = "DiaDiemActivation"
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
              name: "productpage",
              url: "san-pham.htm",
              defaults: new
              {
                  controller = "Home",
                  action = "ProductPage"
              },
                namespaces: new string[] { "Mitsubishi.UI.Controllers" }
          );
            // category
            context.MapRoute(
                name: "CategoryManual",
                url: "sotaycat/{title}-{CategoryManualID}.htm",
                defaults: new
                {
                    controller = "Manual",
                    action = "Index",
                    CategoryManualID = UrlParameter.Optional
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );


            context.MapRoute(
                   name: "ManualCategoryExtra",
                   url: "sotaytuvan/{title}-{ManualCategoryExtraID}.htm",
                   defaults: new
                   {
                       controller = "Manual",
                       action = "Index",
                       ManualCategoryExtraID = UrlParameter.Optional
                   },
                     namespaces: new string[] { "Mitsubishi.UI.Controllers" }
               );


            //chi tiết tips
            context.MapRoute(
                name: "ManualDetail",
                url: "sotay/{title}-{itemId}.htm",
                defaults: new
                {
                    controller = "Manual",
                    action = "ManualDetail"
                },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );



            context.MapRoute(
             name: "event",
             url: "event.htm",
             defaults: new
             {
                 controller = "Content",
                 action = "Event"
             },
               namespaces: new string[] { "Mitsubishi.UI.Controllers" }
         );


            context.MapRoute(
             name: "catalogue",
             url: "catalogue.htm",
             defaults: new
             {
                 controller = "ProductCategory",
                 action = "catalogProduct"
             },
               namespaces: new string[] { "Mitsubishi.UI.Controllers" }
         );

            context.MapRoute(
              name: "NewsEvent",
              url: "news-event.htm",
              defaults: new { controller = "Content", action = "NewsEvent" },
                namespaces: new string[] { "Mitsubishi.UI.Controllers" }
          );


            context.MapRoute(
                name: "AboutMitsu",
                url: "about-mitsubishi.htm",
                defaults: new { controller = "Content", action = "AboutUs", itemKey = "ABOUT_MITSU" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "AboutUs",
                url: "about-us.htm",
                defaults: new { controller = "Content", action = "AboutUs", itemKey = "ABOUT_US" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "Privacy",
                url: "privacy.htm",
                defaults: new { controller = "Content", action = "NewsDetail", itemKey = "PRIVACY" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "TermsOfUse",
                url: "terms-of-use.htm",
                defaults: new { controller = "Content", action = "NewsDetail", itemKey = "TERMS_OF_USE" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "Careers",
                url: "careers.htm",
                defaults: new { controller = "Career", action = "Index" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );



            context.MapRoute(
                name: "ContactUs",
                url: "contact-us.htm",
                defaults: new { controller = "Home", action = "Contact" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "Sitemap",
                url: "sitemap.htm",
                defaults: new { controller = "Home", action = "Sitemap" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "ProductInquiry",
                url: "inquiry.htm",
                defaults: new { controller = "Home", action = "ProductInquiry" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "GeneralInquiry",
                url: "general-inquiry.htm",
                defaults: new { controller = "Home", action = "GeneralInquiry" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "WarrantyTerm",
                url: "warranty-term.htm",
                defaults: new { controller = "Home", action = "WarrantyTerm" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "WhereToBuy",
                url: "where-to-buy.htm",
                defaults: new { controller = "Home", action = "WhereToBuy" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            context.MapRoute(
                name: "AuthorizedCenter",
                url: "authorized-service-center.htm",
                defaults: new { controller = "Home", action = "AuthorizedCenter" },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );
            //toi route nay` no teim controller admin trong folder controlers ngoai`.
            // day nef.

            context.MapRoute(
                "Publish_default",
                "{controller}/{action}/{id}",
                new { action = "Index", controller = "Home", id = UrlParameter.Optional },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );



            context.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                  namespaces: new string[] { "Mitsubishi.UI.Controllers" }
            );

            //routes.MapRoute(
            //    name: "AboutMitsu1",
            //    url: "about-mitsubishi.htm",
            //    defaults: new { controller = "Content", action = "AboutUs", id = "1" }
            //);



        }
    }
}
