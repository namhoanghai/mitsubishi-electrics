﻿using System.Web;
using System.Web.Optimization;

namespace Mitsubishi.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jshome").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jquery-1.10.2.min.js",
                      "~/Scripts/jcarousellite_1.0.1.pack.js",
                      "~/Scripts/Mitsubishi.js",
                      "~/Scripts/script.js",
                      "~/Scripts/jquery.fancybox.pack.js",
                      "~/Scripts/jquery.flexslider-min.js",
                      "~/Scripts/modernizr.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jsbanner").Include(
                      "~/Scripts/shCore.js",
                      "~/Scripts/shBrushXml.js",
                      "~/Scripts/shBrushJScript.js",
                      "~/Scripts/jquery.easing.js",
                      "~/Scripts/jquery.mousewheel.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/csshome").Include(
                      "~/Content/Mitsubishi.css",
                      "~/Content/style-02.css",
                      "~/Content/products.css",
                      "~/Content/factory.css",
                      "~/Content/styles-mobile.css",
                      "~/Content/jquery.fancybox.css",
                      "~/Content/istyles.css",
                      "~/Content/owl.carousel.css",
                      "~/Content/owl.theme.css",
                      "~/Content/flexslider.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                 "~/Scripts/Mitsubishi.js",
            "~/Scripts/kendo/kendo.all.min.js",
                // "~/Scripts/kendo/kendo.timezones.min.js", // uncomment if using the Scheduler
            "~/Scripts/kendo/kendo.aspnetmvc.min.js"));

            bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
                //"~/Content/kendo/kendo.common-bootstrap.min.css",
                //"~/Content/kendo/kendo.bootstrap.min.css",
            "~/Content/kendo/kendo.common.min.css",
            "~/Content/kendo/kendo.rtl.min.css",
            "~/Content/kendo/kendo.silver.min.css"));

            //bundles.IgnoreList.Clear();
            BundleTable.EnableOptimizations = false;
        }
    }
}
