﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mitsubishi.UI.Startup))]
namespace Mitsubishi.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
