﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HtmlAgilityPack;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    [Authorize]
    public class ManualController : Controller
    {
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private readonly ManualService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();
        private static List<string> _lstSlideImages = new List<string>();
        private static string _fileImage = "";
        private static string _attachFileName = "";

        public ManualController()
        {
            _service = new ManualService();
        }

        public ActionResult Index()
        {
           // ViewData[GlobalConstants.LIST_CONTENT_CATEGORY] = ManualCategoryController.GetList();

            var tips = new ManualService().GetListAll();
            ViewBag.Products = tips;

            return View();
        }

        public ActionResult Edit()
        {
            var entity = new ManualContentEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var data = _service.Read();
            var serializer = new JavaScriptSerializer();
            var result = new ContentResult();
            serializer.MaxJsonLength = Int32.MaxValue; // Whatever max length you want here
            result.Content = serializer.Serialize(data.ToDataSourceResult(request));
            result.ContentType = "application/json";

            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(ManualContentEntity entity)
        {
            var result = false;

            if (!string.IsNullOrEmpty(_fileImage))
            {
                entity.Thumbnail = _fileImage;
            }
           
            //entity.ImagesContent = GetListImages(entity.LongDescription);

            if (entity.ItemID == 0)
            {
                result = (_service.Create(entity) > 0);

                _fileImage = string.Empty;

                if (result)
                {
                    ViewBag.ErrorMessage = "Tip category " + entity.TitleEN + " have been created successfully.";
                }
            }
            else
            {
                result = _service.Update(entity);
                _fileImage = string.Empty;
               
                if (result)
                {
                   // return RedirectToAction("Index");
                    return RedirectToAction("Edit", "Manual", new { itemId = entity.ItemID });
                }
            }

            return RedirectToAction("Edit");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, ManualContentEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Save(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath("~/images/sotaygiadung"), finalFileName);

                    file.SaveAs(physicalPath);


                    var r = new ResizeSettings { MaxWidth = 481, MaxHeight = 258, Mode = FitMode.Crop };
                    ImageBuilder.Current.Build("~/images/sotaygiadung/" + finalFileName, "~/images/sotaygiadung/thumb/" + finalFileName, r);


                    _fileImage = finalFileName;
                }
            }
            return Content("");
        }

       
    

        public ActionResult Remove(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/images/sotaygiadung"), fileName);

                    var physicalPath2 = Path.Combine(Server.MapPath("~/images/sotaygiadung/thumb"), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                    if (System.IO.File.Exists(physicalPath2))
                    {
                        System.IO.File.Delete(physicalPath2);
                    }
                    _fileImage = "";
                }
            }
            return Content("");
        }

     

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
    }
}

