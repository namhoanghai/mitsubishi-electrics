﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{

    public class MemberController : Controller
    {
        private readonly MemberService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();

        public MemberController()
        {
            _service = new MemberService();
        }

        public ActionResult Index()
        {

            ViewBag.Message = "Welcome to Mitsubishi";

            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            ViewBag.Message = "Welcome to Mitsubishi";
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(MemberEntity employeeLogin)
        {
            if (ModelState.IsValid)
            {
                var userId = new MemberService().GetCurrentUserId(employeeLogin.UserName, ToMd5(employeeLogin.Password));
                if (userId != 0)
                {
                    HttpContext.Session[GlobalConstants.USER_LOGIN] = employeeLogin.UserName;
                    HttpContext.Session[GlobalConstants.USER_ID] = userId;

                    FormsAuthentication.SetAuthCookie(employeeLogin.UserName, employeeLogin.RememberMe);

                    return RedirectToAction("Index", "Member");
                }
                else
                {
                    ModelState.AddModelError("", "Login data is incorrect!");
                    return View(employeeLogin);
                }
            }
            return View(employeeLogin);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, MemberEntity entity)
        {
            if (entity != null && ModelState.IsValid)
            {
                entity.Password = ToMd5(entity.Password);
                _service.Create(entity);

            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, MemberEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public static string ToMd5(string pass)
        {
            var myMd5 = new MD5CryptoServiceProvider();
            byte[] myPass = Encoding.UTF8.GetBytes(pass);
            myPass = myMd5.ComputeHash(myPass);
            var s = new StringBuilder();
            foreach (byte p in myPass)
            {
                s.Append(p.ToString("x").ToLower());
            }
            return s.ToString();
        }
    }
}

