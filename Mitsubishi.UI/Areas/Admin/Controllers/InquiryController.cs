﻿using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    [Authorize]
    public class InquiryController : Controller
    {
        private readonly InquiryService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();

        public InquiryController()
        {
            _service = new InquiryService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}

