﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Controllers;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{

    public class ContentCategoryController : Controller
    {
        private readonly ContentCategoryService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();

        public ContentCategoryController()
        {
            _service = new ContentCategoryService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public static List<ContentCategoryEntity> GetList()
        {
            List<ContentCategoryEntity> lst;

            if (Configuration.EnableCached)
            {
                if (System.Web.HttpContext.Current.Cache[GlobalConstants.LIST_CONTENT_CATEGORY] != null)
                {
                    lst = (List<ContentCategoryEntity>)System.Web.HttpContext.Current.Cache[GlobalConstants.LIST_CONTENT_CATEGORY];
                }
                else
                {
                    lst = new ContentCategoryService().GetListCombobox();

                    System.Web.HttpContext.Current.Cache.Insert(GlobalConstants.LIST_CONTENT_CATEGORY, lst);
                }
            }
            else
            {
                lst = new ContentCategoryService().GetListCombobox();
            }

            return lst;
        }

        public JsonResult AddNew(string title, string titleEn, int sortOrder, bool isActive, int? parentId, int? itemId)
        {
            var e = new ContentCategoryEntity();

            if (parentId != null)
            {
                e.ParentID = Convert.ToInt32(parentId);
            }

            e.Title = title;
            e.TitleEN = titleEn;
            e.SortOrder = sortOrder;
            e.IsActive = isActive;

            if (itemId != null)
            {
                e.ItemID = Convert.ToInt32(itemId);
            }

            var result = _service.Create(e);

            return Json(result > 0, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, ContentCategoryEntity entity)
        {
            if (entity != null && ModelState.IsValid)
            {
                _service.Create(entity);

            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, ContentCategoryEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListCombobox(string currentLanguage = GlobalConstants.LANGUAGE_VN)
        {
            var lst = GetListRoot(UtilsController.GetCurrentLanguage(GlobalConstants.SESS_ADMIN_LANGUAGE));

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public List<ContentCategoryEntity> GetListRoot(string currentLanguage = GlobalConstants.LANGUAGE_VN)
        {
            List<ContentCategoryEntity> lstRoot;

            var lst = new List<ContentCategoryEntity>();

            lstRoot = _service.GetListRoot();

            foreach (var rootItem in lstRoot)
            {
                lst.Add(rootItem);

                var childs = _service.GetListActive(rootItem.ItemID);

                if (childs.Any())
                {
                    foreach (var child in childs)
                    {
                        child.Title = "----- " + child.Title;
                        child.TitleEN = "----- " + child.TitleEN;

                        lst.Add(child);
                    }
                }
            }


            foreach (var entity in lst)
            {
                if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                {
                    entity.DataTextField = entity.TitleEN;
                }
                else
                {
                    entity.DataTextField = entity.Title;
                }
            }

            return lst;
        }

        public JsonResult GetItem(int itemId)
        {
            var entity = _service.GetItem(itemId);

            return Json(entity, JsonRequestBehavior.AllowGet);
        }
    }
}

