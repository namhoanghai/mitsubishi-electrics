﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using SpreadsheetGear;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    [Authorize]
    public class AgencyController : Controller
    {
        private readonly AgencyService _service;
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private static string _logo = string.Empty;
        
        public AgencyController()
        {
            _service = new AgencyService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            var entity = new AgencyEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }

        public JsonResult GetListLocation()
        {
            var lst = new LocationService().GetList();

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(AgencyEntity entity)
        {
            var result = false;

            if (!string.IsNullOrEmpty(_logo))
            {
                entity.Logo = _logo;
            }

            if (entity.ItemID == 0)
            {
                result = (_service.Create(entity) > 0);

                _logo = string.Empty;
            }
            else
            {
                result = _service.Update(entity);
                _logo = string.Empty;

                if (result)
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Edit");
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request, string searchCriteria)
        {
            return Json(_service.Read(searchCriteria).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, AgencyEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult SaveLogo(IEnumerable<HttpPostedFileBase> fileLogo)
        {
            if (fileLogo != null)
            {
                foreach (var file in fileLogo)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/images/logo/"), fileName);

                    file.SaveAs(physicalPath);
                    _logo = fileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveLogo(string[] fileLogo)
        {
            if (fileLogo != null)
            {
                foreach (var fullName in fileLogo)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/images/logo/"), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _logo = "";
                }
            }
            return Content("");
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ImportAgency()
        {
            var filePath = Server.MapPath("/Upload/MEVN Where to Buy List.xls");
            var workbook = Factory.GetWorkbook(filePath);
            var cells = workbook.Worksheets[0].Cells;

            var rowEmpty = 0;
            var i = 1;
            
            while (rowEmpty < 100)
            {
                var name = cells["E" + i].Text;
                var address = cells["F" + i].Text;
                var entity = new AgencyEntity();

                if (!string.IsNullOrEmpty(name) && name.Trim().ToUpper() != "FULL NAME")
                {
                    entity.Title = name;
                    entity.Address = address;

                    _service.Create(entity);
                }
                else
                {
                    rowEmpty++;
                }

                i++;
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
	}
}