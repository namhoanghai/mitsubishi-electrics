﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HtmlAgilityPack;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    public class ActivationController : Controller
    {
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private readonly ActivationService _service;
        private static string _fileImage = "";
        // GET: Admin/Activation
         public ActivationController()
        {
            _service = new ActivationService();
        }
        public ActionResult Index()
        {             
            return View();
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit()
        {
            var entity = new ActivationEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }




        public ActionResult SaveImage(IEnumerable<HttpPostedFileBase> fileImageActivation)
        {
            if (fileImageActivation != null)
            {
                foreach (var file in fileImageActivation)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath("~/images/activation/"), finalFileName);

                    file.SaveAs(physicalPath);

                    var r = new ResizeSettings { MaxWidth = 480, MaxHeight = 300, Mode = FitMode.Crop };
                    ImageBuilder.Current.Build("~/images/activation/" + finalFileName, "~/images/activation/" + finalFileName, r);

                    _fileImage = finalFileName;
                }
            }
            return Content("");
        
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, ActivationEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult RemoveImage(string[] fileImageActivation)
        {
            if (fileImageActivation != null)
            {
                foreach (var fullName in fileImageActivation)
                {
                    var fileName = Path.GetFileName(fullName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), finalFileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _fileImage = "";
                }
            }
            return Content("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(ActivationEntity entity)
        {
            var result = false;

            if (_fileImage.Any())
            {
               
            }
            if (_fileImage != "")
            {
                entity.Thumbnail = _fileImage;
            }

            int LastID = 0;
          
            if (entity.ItemID == 0)
            {
                LastID = _service.Create(entity);

                return RedirectToAction("Edit", "Activation", new { itemId = LastID });
               
            }
            else
            {
                result = _service.Update(entity);

               

                if (result)
                {
                    return RedirectToAction("Edit", "Activation", new { itemId = entity.ItemID });
                }
            }

            return RedirectToAction("Edit");
        }
    }
}