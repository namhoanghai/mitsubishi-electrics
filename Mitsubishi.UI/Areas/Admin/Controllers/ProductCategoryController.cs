﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Controllers;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    [Authorize]
    public class ProductCategoryController : Controller
    {
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private readonly ProductCategoryService _service;
        private static string _fileImage = "";
        private static string _fileThumbVideo = "";
        private static string _brochureName = "";
        private static string _iconName = "";
        private static string _iconFooterName = "";
        private static List<string> _lstSlideImages = new List<string>();
        private static List<string> _lstColorImages = new List<string>();
        private static string _colorName = string.Empty;
        private static string _colorImages = string.Empty;

        public ProductCategoryController()
        {
            _service = new ProductCategoryService();
        }

        public ActionResult Index()
        {
            //ViewData[GlobalConstants.LIST_PRODUCT_TYPE] = ProductTypeController.GetList();'
          
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditProductCategory()
        {
            var entity = new ProductCategoryEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItemFullDetail(itemId);
            }
            
          
            var customFieldService = new CustomFieldService();
            var merchantService = new MerchantService();

            entity.CustomFields = customFieldService.GetListCustomFields(entity.ItemID);
            entity.Merchants = merchantService.GetAllMechantsOfProduct(entity.ItemID);

            //ViewBag.ListRelated = new ProductCategoryService().GetListActive(entity.ParentID);

            return View(entity);
        }

        public ActionResult UpdateColor(string title)
        {
            _colorName = title;

            if (Session["ColorImages"] != null)
            {
                var lstImages = (List<string>)Session["ColorImages"];

                _colorImages = String.Join(";", lstImages.ToArray());
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }



        public JsonResult SaveProductColor(int productId, int colorId, string title, string images, int sortOrder, string code)
        {
            var color = new ProductColorEntity();
            color.ItemID = colorId;
            color.ProductID = productId;
            color.Title = title;
            color.Code = code;

            if (_lstColorImages.Any())
            {
                color.Images = String.Join(";", _lstColorImages.ToArray()) + ";" + images;
            }
            else
            {
                color.Images = images;
            }

            color.SortOrder = sortOrder;

            var service = new ProductColorService();

            if (color.ItemID != 0)
            {
                service.Update(color);
            }
            else
            {
                service.Create(color);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateItem(ProductCategoryEntity e)
        {
            var result = false;

            if (!string.IsNullOrEmpty(_iconName))
            {
                e.IconName = _iconName;
            }

            if (string.IsNullOrEmpty(e.Price))
            {
                e.Price = "";
            }
            if (string.IsNullOrEmpty(e.Promotion))
            {
                e.Promotion = "";
            }


            if (string.IsNullOrEmpty(e.LinkDienMayXanh))
            {
                e.LinkDienMayXanh = "";
            }
            if (string.IsNullOrEmpty(e.LinkNguyenKim))
            {
                e.LinkNguyenKim = "";
            }
            if (string.IsNullOrEmpty(e.LinkThienHoa))
            {
                e.LinkThienHoa = "";
            }
            if (string.IsNullOrEmpty(e.LinkChoLon))
            {
                e.LinkChoLon = "";
            }

            if (!string.IsNullOrEmpty(_iconFooterName))
            {
                e.IconFooter = _iconFooterName;
            }

            if (!string.IsNullOrEmpty(_fileImage))
            {
                e.ImageName = _fileImage;
            }

            if (!string.IsNullOrEmpty(_fileThumbVideo))
            {
                e.ThumbVideo = _fileThumbVideo;
            }

            if (!string.IsNullOrEmpty(_brochureName))
            {
                e.BrochureFileName = _brochureName;
            }





            if (Session[GlobalConstants.SESS_PRODUCT_IMAGES] != null)
            {
                var lst = (List<string>)Session[GlobalConstants.SESS_PRODUCT_IMAGES];

                if (lst.Any())
                {
                    e.Banners = string.Join(";", lst.ToArray());
                }
            }


            if (_lstSlideImages.Any())
            {
                e.SlideImages = String.Join(";", _lstSlideImages.ToArray()) + ";" + e.SlideImages;
            }





            if (e.ItemID == 0)
            {
                var itemId = _service.Create(e);
                result = (itemId > 0);

                Session[GlobalConstants.SESS_PRODUCT_IMAGES] = null;
                _fileImage = string.Empty;
                _fileThumbVideo = string.Empty;
                _brochureName = string.Empty;
                _iconName = string.Empty;
                _iconFooterName = string.Empty;
                _lstSlideImages = new List<string>();

                return Redirect("/Admin/ProductCategory/EditProductCategory?itemId=" + itemId);
            }
            else
            {
                result = _service.Update(e);

                Session[GlobalConstants.SESS_PRODUCT_IMAGES] = null;
                _fileImage = string.Empty;
                _fileThumbVideo = string.Empty;
                _brochureName = string.Empty;
                _iconName = string.Empty;
                _iconFooterName = string.Empty;
                _lstSlideImages = new List<string>();

                if (result)
                {
                    //return RedirectToAction("Index");
                    return RedirectToAction("EditProductCategory", "ProductCategory", new { itemId = e.ItemID });
                }
            }


            return RedirectToAction("EditProductCategory");
        }

        public JsonResult GetListCombobox(string currentLanguage = GlobalConstants.LANGUAGE_VN)
        {
            var lst = GetListRoot(UtilsController.GetCurrentLanguage(GlobalConstants.SESS_ADMIN_LANGUAGE));

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public List<ProductCategoryEntity> GetListRoot(string currentLanguage = GlobalConstants.LANGUAGE_VN)
        {
            List<ProductCategoryEntity> lstRoot;

            var lst = new List<ProductCategoryEntity>();

            lstRoot = _service.GetListRoot();

            foreach (var rootItem in lstRoot)
            {
                lst.Add(rootItem);

                var childs = _service.GetChilds(rootItem.ItemID);

                if (childs.Any())
                {
                    foreach (var child in childs)
                    {
                        child.Title = "----- " + child.Title;
                        child.TitleEN = "----- " + child.TitleEN;

                        lst.Add(child);

                        var subChilds = _service.GetChilds(child.ItemID);

                        if (subChilds.Any())
                        {
                            foreach (var subChild in subChilds)
                            {
                                subChild.Title = "---------- " + subChild.Title;
                                subChild.TitleEN = "---------- " + subChild.TitleEN;

                                lst.Add(subChild);

                                var subChildChilds = _service.GetChilds(subChild.ItemID);

                                if (subChildChilds.Any())
                                {
                                    foreach (var childChild in subChildChilds)
                                    {
                                        childChild.Title = "--------------- " + childChild.Title;
                                        childChild.TitleEN = "--------------- " + childChild.TitleEN;

                                        lst.Add(childChild);

                                        var subGrandChild = _service.GetChilds(childChild.ItemID);

                                        if (subGrandChild.Any())
                                        {
                                            foreach (var grandChild in subGrandChild)
                                            {
                                                grandChild.Title = "-------------------- " + grandChild.Title;
                                                grandChild.TitleEN = "-------------------- " + grandChild.TitleEN;

                                                lst.Add(grandChild);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            foreach (var entity in lst)
            {
                if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                {
                    entity.DataTextField = entity.TitleEN;
                }
                else
                {
                    entity.DataTextField = entity.Title;
                }
            }

            return lst;
        }

        public JsonResult GetItem(int itemId)
        {
            var entity = _service.GetItem(itemId);

            return Json(entity, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post), ValidateInput(false)]
        public JsonResult UpdateDescription(int? itemId, string language, string content)
        {
            var result = itemId != null && _service.UpdateDescription((int)itemId, language, content);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Destroy(int itemId)
        {
            var result = _service.Destroy(itemId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListByParent(int? parentId)
        {
            var parent = 0;

            if (parentId != null)
            {
                parent = Convert.ToInt32(parentId);
            }

            var lst = new ProductCategoryService().GetListRelated(parent);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListSubModels(int? parentId)
        {
            var parent = 0;

            if (parentId != null)
            {
                parent = Convert.ToInt32(parentId);
            }

            var lst = new ProductCategoryService().GetListSubModels(parent);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListProductTypes()
        {
            var lst = new ProductCategoryService().GetListActive(0);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListCategory(int parentId)
        {
            var lst = new ProductCategoryService().GetListActive(parentId);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SaveThumbVideo(IEnumerable<HttpPostedFileBase> fileThumbVideo)
        {
            if (fileThumbVideo != null)
            {
               

                foreach (var file in fileThumbVideo)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativeImagePath), finalFileName);

                    file.SaveAs(physicalPath);
                   
                    var r = new ResizeSettings { MaxWidth = 65, MaxHeight = 65, Mode = FitMode.Crop };
                    ImageBuilder.Current.Build("~/images/" + finalFileName, "~/images/" + finalFileName, r);


                    _fileThumbVideo = finalFileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveThumbVideo(string[] fileThumbVideo)
        {
            if (fileThumbVideo != null)
            {
                foreach (var fullName in fileThumbVideo)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativeImagePath), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _fileThumbVideo = "";
                }
            }
            return Content("");
        }

        #region "Uploads"
        public ActionResult Save(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativeImagePath), fileName);

                    file.SaveAs(physicalPath);
                    _fileImage = fileName;
                }
            }
            return Content("");
        }

        public ActionResult SaveBanners(IEnumerable<HttpPostedFileBase> fileBanners)
        {
            if (fileBanners != null)
            {
                var lstImages = new List<string>();

                if (Session[GlobalConstants.SESS_PRODUCT_IMAGES] != null)
                {
                    lstImages = (List<string>)Session[GlobalConstants.SESS_PRODUCT_IMAGES];
                }

                foreach (var file in fileBanners)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(Server.MapPath("~/images/banners/"), fileName);

                        file.SaveAs(physicalPath);

                        lstImages.Add(fileName);
                    }

                    Session[GlobalConstants.SESS_PRODUCT_IMAGES] = lstImages;
                }
            }
            return Content("");
        }

        public ActionResult Remove(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativeImagePath), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _fileImage = "";
                }
            }
            return Content("");
        }

        public ActionResult RemoveBanners(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/images/banners/"), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                    _fileImage = "";
                }
            }
            return Content("");
        }

        public ActionResult SaveBrochure(IEnumerable<HttpPostedFileBase> fileBrochure)
        {
            if (fileBrochure != null)
            {
                foreach (var file in fileBrochure)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.DownloadPath)), fileName);

                    file.SaveAs(physicalPath);
                    _brochureName = fileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveBrochure(string[] fileBrochure)
        {
            if (fileBrochure != null)
            {
                foreach (var fullName in fileBrochure)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.DownloadPath)), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _brochureName = "";
                }
            }
            return Content("");
        }

        public ActionResult SaveIcon(IEnumerable<HttpPostedFileBase> fileIcon)
        {
            if (fileIcon != null)
            {
                foreach (var file in fileIcon)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string ext = Path.GetExtension(file.FileName);

                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), string.Format("{0:yyyy-MM-dd-HHmmss}", DateTime.Now) + ext);
                    }

                    file.SaveAs(physicalPath);

                    _iconName = fileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveIcon(string[] fileIcon)
        {
            if (fileIcon != null)
            {
                foreach (var fullName in fileIcon)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.DownloadPath)), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _iconName = "";
                }
            }
            return Content("");
        }

        public ActionResult SaveIconFooter(IEnumerable<HttpPostedFileBase> fileIconFooter)
        {
            if (fileIconFooter != null)
            {
                foreach (var file in fileIconFooter)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string ext = Path.GetExtension(file.FileName);

                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), string.Format("{0:yyyy-MM-dd-HHmmss}", DateTime.Now) + ext);
                    }

                    file.SaveAs(physicalPath);

                    _iconFooterName = fileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveIconFooter(string[] fileIconFooter)
        {
            if (fileIconFooter != null)
            {
                foreach (var fullName in fileIconFooter)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.DownloadPath)), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _iconFooterName = "";
                }
            }
            return Content("");
        }

        public ActionResult SaveSlideImages(IEnumerable<HttpPostedFileBase> slides)
        {
            if (slides != null)
            {
                //var lstImages = new List<string>();

                //if (Session[GlobalConstants.SESS_PRODUCT_IMAGES] != null)
                //{
                //    lstImages = (List<string>)Session[GlobalConstants.SESS_PRODUCT_IMAGES];
                //}

                foreach (var file in slides)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(Server.MapPath("/images/"), finalFileName);

                        file.SaveAs(physicalPath);

                        var r = new ResizeSettings { MaxWidth = 90, MaxHeight = 90, Mode = FitMode.Crop };
                        ImageBuilder.Current.Build("~/images/" + finalFileName, "~/images/icon/" + finalFileName, r);

                        _lstSlideImages.Add(finalFileName);
                    }

                    //Session[GlobalConstants.SESS_PRODUCT_IMAGES] = lstImages;
                }
            }
            return Content("");
        }
        public ActionResult removeColorImage(string image)
        {
          
            List<string> ar_tmp = new List<string>();
            for (int i = 0; i < _lstColorImages.Count; i++)
            {
                if (image != _lstColorImages[i])
                {
                    ar_tmp.Add(_lstColorImages[i]);
                }
            }
            _lstColorImages = ar_tmp;
                return Content("");
        }
        public ActionResult SaveColorImages(IEnumerable<HttpPostedFileBase> colorImages)
        {
            if (colorImages != null)
            {
                _lstColorImages = new List<string>();

                foreach (var file in colorImages)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(Server.MapPath("/images/"), finalFileName);

                        file.SaveAs(physicalPath);

                        System.IO.File.Copy(physicalPath, Server.MapPath("~/images/largeImage/" + finalFileName));

                        var r = new ResizeSettings { MaxWidth = 90, MaxHeight = 90, Mode = FitMode.Crop };
                        ImageBuilder.Current.Build("~/images/" + finalFileName, "~/images/icon/" + finalFileName, r);

                        var thumbnail = new ResizeSettings { MaxWidth = 180, MaxHeight = 180, Mode = FitMode.Crop };
                        ImageBuilder.Current.Build("~/images/" + finalFileName, "~/images/thumbnail/" + finalFileName, thumbnail);

                        _lstColorImages.Add(finalFileName);
                    }
                }
            }

            return Content("");
        }
        #endregion

        public JsonResult RemoveSlideImage(string slideImages, string selectImage)
        {
            var lst = slideImages.Split(';').ToList();

            lst.Remove(selectImage);

            return Json(String.Join(";", lst.ToArray()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveColor(string itemId, int colorId)
        {
            var service = new ProductColorService();

            var item = service.GetItem(colorId);

            if (item != null)
            {
                if (item.ListImages.Any())
                {
                    foreach (var image in item.ListImages)
                    {
                        if (System.IO.File.Exists(Server.MapPath("~/images/" + image)))
                        {
                            System.IO.File.Delete(Server.MapPath("~/images/" + image));
                        }

                        if (System.IO.File.Exists(Server.MapPath("~/images/icon/" + image)))
                        {
                            System.IO.File.Delete(Server.MapPath("~/images/icon/" + image));
                        }

                        if (System.IO.File.Exists(Server.MapPath("~/images/thumbnail/" + image)))
                        {
                            System.IO.File.Delete(Server.MapPath("~/images/thumbnail/" + image));
                        }

                        if (System.IO.File.Exists(Server.MapPath("~/images/largeImage/" + image)))
                        {
                            System.IO.File.Delete(Server.MapPath("~/images/largeImage/" + image));
                        }
                    }
                }
            }

            var result = service.Destroy(colorId);

            var lst = service.GetList(Convert.ToInt32(itemId));

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetColorItem(int colorId)
        {
            var result = new ProductColorService().GetItem(colorId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetJsonComboBox(int parentId)
        {
            var result = _service.GetListComboBox(parentId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListBanners()
        {
            var lst = new BannerService().Read();

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult AddCustomField(CustomField cf)
        {
            var customFieldService = new CustomFieldService();
            var cfList = customFieldService.GetListCustomFields(cf.ProductId);

            if (cfList.Any(c => c.Alias == cf.Alias))
            {
                return Json("alias", JsonRequestBehavior.AllowGet);
            }
            if (cfList.Any(c => c.Title == cf.Title))
            {
                return Json("title", JsonRequestBehavior.AllowGet);
            }

            customFieldService.Create(cf);

            var result = customFieldService.GetCustomFieldByAlias(cf.Alias);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomFieldListPartial(int? productId)
        {
            var cfList = new List<CustomField>();
            if (!productId.HasValue) return PartialView(cfList);

            var customFieldService = new CustomFieldService();

            cfList = customFieldService.GetListCustomFields(productId.Value);

            return PartialView(cfList);
        }

        [HttpPost]
        public ActionResult GetCustomField(int Id)
        {
            var customFieldService = new CustomFieldService();

            var cf = customFieldService.GetCustomField(Id);

            return Json(cf, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteCustomField(int Id)
        {
            var customFieldService = new CustomFieldService();

            var result = customFieldService.Delete(Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateCustomField(CustomField cf)
        {
            var customFieldService = new CustomFieldService();

            var result = customFieldService.Update(cf);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReadMerchants(int? productId)
        {
            var merchantService = new MerchantService();
            var result = merchantService.GetAllMechants();

            if (productId.HasValue)
            {
                result.ForEach(r =>
                {
                    if (merchantService.IsMerchantLinked(r.Id, productId.Value))
                    {
                        var temp = r.Name;
                        r.Name = temp + " (linked)";
                    }
                });
            }

            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult GetMerchantLink(int productId, int merchantId)
        {
            var merchantService = new MerchantService();

            return Json(merchantService.GetMerchantLink(merchantId, productId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddUpdateMerchant(int productId, int merchantId, string link)
        {
            Uri uriResult;
            var testUrl = Uri.TryCreate(link, UriKind.Absolute, out uriResult);
            if (!(testUrl && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps)))
            {
                return Json("Your link is not acceptable", JsonRequestBehavior.AllowGet);
            }

            var merchantService = new MerchantService();

            try
            {
                if (merchantService.IsMerchantLinked(merchantId, productId))
                {
                    merchantService.UpdateProductMerchant(productId, merchantId, link);

                    return Json("Merchant updated successfully", JsonRequestBehavior.AllowGet);
                }

                merchantService.CreateProductMerchant(productId, merchantId, link);

                return Json("Merchant linked successfully", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("Something wrong happened", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RemoveProductMerchant(int productId, int merchantId)
        {
            var merchantService = new MerchantService();

            if (!merchantService.IsMerchantLinked(merchantId, productId))
                return Json(false, JsonRequestBehavior.AllowGet);

            if (merchantService.DeleteProductMerchant(productId, merchantId))
                return Json("Merchant unlinked successfully", JsonRequestBehavior.AllowGet);

            return Json("Something wrong happened", JsonRequestBehavior.AllowGet);
        }

        public void ConvertLink()
        {
            var productsCategory = _service.GetList();
            var merchantService = new MerchantService();

            productsCategory.ForEach(p =>
            {
                if (p.LinkChoLon != null)
                {
                    merchantService.CreateProductMerchant(p.ItemID, -1, p.LinkChoLon);
                }
                if (p.LinkDienMayXanh != null)
                {
                    merchantService.CreateProductMerchant(p.ItemID, -1, p.LinkDienMayXanh);
                }
                if (p.LinkNguyenKim != null)
                {
                    merchantService.CreateProductMerchant(p.ItemID, -1, p.LinkNguyenKim);
                }
                if (p.LinkThienHoa != null)
                {
                    merchantService.CreateProductMerchant(p.ItemID, -1, p.LinkThienHoa);
                }
            });
        }
    }
}

