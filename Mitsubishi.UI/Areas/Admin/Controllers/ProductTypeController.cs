﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Controllers;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{

    public class ProductTypeController : Controller
    {
        private readonly ProductTypeService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();
        private static string fileImage = "";
        public ProductTypeController()
        {
            _service = new ProductTypeService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListCombobox([DataSourceRequest] DataSourceRequest request)
        {
            var lst = GetList(UtilsController.GetCurrentLanguage(GlobalConstants.SESS_ADMIN_LANGUAGE));

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public static List<ProductTypeEntity> GetList(string currentLanguage = GlobalConstants.LANGUAGE_VN)
        {
            List<ProductTypeEntity> lst;

            if (Configuration.EnableCached)
            {
                if (System.Web.HttpContext.Current.Cache[GlobalConstants.LIST_PRODUCT_TYPE] != null)
                {
                    lst = (List<ProductTypeEntity>)System.Web.HttpContext.Current.Cache[GlobalConstants.LIST_PRODUCT_TYPE];
                }
                else
                {
                    lst = new ProductTypeService().GetList();

                    System.Web.HttpContext.Current.Cache.Insert(GlobalConstants.LIST_PRODUCT_TYPE, lst);
                }
            }
            else
            {
                lst = new ProductTypeService().GetList();
            }

            foreach (var entity in lst)
            {
                if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                {
                    entity.DataTextField = entity.TitleEN;
                }
                else
                {
                    entity.DataTextField = entity.Title;
                }
            }


            return lst;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, ProductTypeEntity entity)
        {
            if (entity != null && ModelState.IsValid)
            {
                _service.Create(entity);

            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, ProductTypeEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Save(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                    file.SaveAs(physicalPath);
                    fileImage = fileName;
                }
            }
            return Content("");
        }

        public ActionResult Remove(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                    fileImage = "";
                }
            }
            return Content("");
        }
    }
}

