﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    public class MerchantController : Controller
    {
        private readonly MerchantService _service;
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private static string _merchant = string.Empty;
        public MerchantController()
        {
            _service = new MerchantService();
        }
        // GET: Admin/Merchant
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            var entity = new MerchantEntity();

            if (Request.QueryString["Id"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["Id"]);

                entity = _service.GetMerchantById(itemId);
            }

            return View(entity);
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = Json(_service.GetAllMechants().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return result;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(MerchantEntity entity)
        {
            var result = false;

            if (!string.IsNullOrEmpty(_merchant))
            {
                entity.Image = _merchant;
            }

            if (entity.Id == 0)
            {
                result = _service.CreateMerchant(entity);

                _merchant = string.Empty;
            }
            else
            {
                result = _service.UpdateMerchant(entity);

                _merchant = string.Empty;

                if (result)
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Edit");
        }

        public ActionResult SaveMerchantImage(IEnumerable<HttpPostedFileBase> fileMerchant)
        {
            if (fileMerchant != null)
            {
                foreach (var file in fileMerchant)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath("~/images/merchants/"), finalFileName);

                    file.SaveAs(physicalPath);

                    var r = new ResizeSettings { MaxWidth = 146, MaxHeight = 60, Mode = FitMode.Crop };
                    ImageBuilder.Current.Build("~/images/merchants/" + finalFileName, "~/images/merchants/" + finalFileName, r);

                    _merchant = finalFileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveMerchantImage(string[] fileMerchant)
        {
            if (fileMerchant != null)
            {
                foreach (var fullName in fileMerchant)
                {
                    var fileName = Path.GetFileName(fullName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), finalFileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _merchant = "";
                }
            }
            return Content("");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, MerchantEntity entity)
        {
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(entity.Image) &&
                    System.IO.File.Exists(Server.MapPath("~/images/merchants/" + entity.Image)))
                {
                    System.IO.File.Delete(Server.MapPath("~/images/merchants/" + entity.Image));
                }

                _service.DeleteMerchant(entity.Id);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }
    }
}