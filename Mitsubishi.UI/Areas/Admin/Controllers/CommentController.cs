﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HtmlAgilityPack;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    public class CommentController : Controller
    {
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private readonly CommentService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();

        public CommentController()
        {
            _service = new CommentService();
        }
        // GET: Admin/Comment
        public ActionResult Index()
        {

            var tmp = new CommentService().GetList();
            ViewBag.Comment = tmp;
            return View();
        }

        public ActionResult Edit()
        {
            var entity = new CommentEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(CommentEntity entity)
        {
            var result = false;

            if (entity.ItemID == 0)
            {
                result = (_service.Create(entity) > 0);

                if (result)
                {
                    ViewBag.ErrorMessage = "Comment have been created successfully.";
                }
            }
            else
            {
                result = _service.Update(entity);
              
                if (result)
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Edit");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, CommentEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }



      



        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}