﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Controllers;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    public class ManualCategoryController : Controller
    {
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private readonly ManualCategoryService _service;
        private static string _fileImage = "";
         public ManualCategoryController()
        {
            _service = new ManualCategoryService();
        }
        // GET: Admin/ManualCategory
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateItem(ManualCategoryEntity e)
        {
            var result = false;

          

            if (!string.IsNullOrEmpty(_fileImage))
            {
                e.Thumbnail = _fileImage;
            }

            if (e.ItemID == 0)
            {
                var itemId = _service.Create(e);
                result = (itemId > 0);

                _fileImage = string.Empty;
               

                return Redirect("/Admin/ManualCategory/EditManualCategory?itemId=" + itemId);
            }
            else
            {
                result = _service.Update(e);

               
                _fileImage = string.Empty;
                

                if (result)
                {
                    return RedirectToAction("Index");
                }
            }


            return RedirectToAction("EditManualCategory");
        }

        public JsonResult GetListCombobox(string currentLanguage = GlobalConstants.LANGUAGE_VN, int TaxonomyId = 0)
        {
            var lst = GetListRoot(UtilsController.GetCurrentLanguage(GlobalConstants.SESS_ADMIN_LANGUAGE), TaxonomyId);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public List<ManualCategoryEntity> GetListRoot(string currentLanguage = GlobalConstants.LANGUAGE_VN, int TaxonomyId = 0)
        {
            List<ManualCategoryEntity> lstRoot;

            var lst = new List<ManualCategoryEntity>();

            lstRoot = _service.GetListRoot(true, TaxonomyId);

            foreach (var rootItem in lstRoot)
            {
                lst.Add(rootItem);

                var childs = _service.GetChilds(rootItem.ItemID);

                if (childs.Any())
                {
                    foreach (var child in childs)
                    {
                        child.Title = "----- " + child.Title;
                        child.TitleEN = "----- " + child.TitleEN;

                        lst.Add(child);

                        var subChilds = _service.GetChilds(child.ItemID);

                        if (subChilds.Any())
                        {
                            foreach (var subChild in subChilds)
                            {
                                subChild.Title = "---------- " + subChild.Title;
                                subChild.TitleEN = "---------- " + subChild.TitleEN;

                                lst.Add(subChild);

                                var subChildChilds = _service.GetChilds(subChild.ItemID);

                                if (subChildChilds.Any())
                                {
                                    foreach (var childChild in subChildChilds)
                                    {
                                        childChild.Title = "--------------- " + childChild.Title;
                                        childChild.TitleEN = "--------------- " + childChild.TitleEN;

                                        lst.Add(childChild);

                                        var subGrandChild = _service.GetChilds(childChild.ItemID);

                                        if (subGrandChild.Any())
                                        {
                                            foreach (var grandChild in subGrandChild)
                                            {
                                                grandChild.Title = "-------------------- " + grandChild.Title;
                                                grandChild.TitleEN = "-------------------- " + grandChild.TitleEN;

                                                lst.Add(grandChild);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            foreach (var entity in lst)
            {
                if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                {
                    entity.DataTextField = entity.TitleEN;
                }
                else
                {
                    entity.DataTextField = entity.Title;
                }
            }

            return lst;
        }

        public JsonResult GetItem(int itemId)
        {
            var entity = _service.GetItem(itemId);

            return Json(entity, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditManualCategory()
        {
            var entity = new ManualCategoryEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItemFullDetail(itemId);
            }

            //ViewBag.ListRelated = new ProductCategoryService().GetListActive(entity.ParentID);

            return View(entity);
        }

        public ActionResult Save(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativeImagePath), fileName);

                    file.SaveAs(physicalPath);
                    _fileImage = fileName;
                }
            }
            return Content("");
        }

        public JsonResult Destroy(int itemId)
        {
            var result = _service.Destroy(itemId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIs_Active(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}