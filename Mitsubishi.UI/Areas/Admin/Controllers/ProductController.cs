﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Controllers;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{

    public class ProductController : Controller
    {
        private readonly ProductService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();
        private static string fileImage = "";
        public ProductController()
        {
            _service = new ProductService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, ProductEntity entity, string content, string contentEng, int? parentId)
        {
            if (entity != null && ModelState.IsValid)
            {
                if (parentId != null)
                {
                    entity.CategoryID = Convert.ToInt32(parentId);
                }
                entity.LongDescription = content;
                entity.LongDescriptionEng = contentEng;
                if (!String.IsNullOrEmpty(fileImage))
                    entity.ImageName = fileImage;

                _service.Create(entity);

            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, ProductEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Save(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                    file.SaveAs(physicalPath);
                    fileImage = fileName;
                }
            }
            return Content("");
        }

        public ActionResult Remove(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                    fileImage = "";
                }
            }
            return Content("");
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}

