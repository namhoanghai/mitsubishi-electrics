﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HtmlAgilityPack;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    [Authorize]
    public class ContentController : Controller
    {
        private readonly ContentService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();
        private static List<string> _lstSlideImages = new List<string>(); 
        private static string _fileImage = "";
        private static string _attachFileName = "";

        public ContentController()
        {
            _service = new ContentService();
        }

        public ActionResult Index()
        {
            ViewData[GlobalConstants.LIST_CONTENT_CATEGORY] = ContentCategoryController.GetList();
            return View();
        }

        public ActionResult Edit()
        {
            var entity = new ContentEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(ContentEntity entity)
        {
            var result = false;

            if (_lstSlideImages.Any())
            {
                entity.SlideImages = String.Join(";", _lstSlideImages.ToArray()) + ";" + entity.SlideImages;
            }
            if (_fileImage != "")
            {
                entity.Thumbnail = _fileImage;
            }
            
            //entity.ImagesContent = GetListImages(entity.LongDescription);

            if (entity.ItemID == 0)
            {
                result = (_service.Create(entity) > 0);

                _lstSlideImages = new List<string>();
                _fileImage = "";
                if (result)
                {
                    ViewBag.ErrorMessage = "Product category " + entity.TitleEN + " have been created successfully.";
                }
            }
            else
            {
                result = _service.Update(entity);

                _fileImage = "";

                _lstSlideImages = new List<string>();

                if (result)
                {
                    return RedirectToAction("Edit", "Content", new { itemId = entity.ItemID });
                }
            }

            return RedirectToAction("Edit");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, ContentEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Save(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);

                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath("~/images/news/"), finalFileName);

                    file.SaveAs(physicalPath);

                    var r = new ResizeSettings { MaxWidth = 481, MaxHeight = 258, Mode = FitMode.Crop };
                    ImageBuilder.Current.Build("~/images/news/" + finalFileName, "~/images/news/thumb/" + finalFileName, r);

                    _fileImage = finalFileName;
                }
            }
            return Content("");
        }

        public ActionResult Remove(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/images/news"), fileName);
                    var physicalPath2 = Path.Combine(Server.MapPath("~/images/news"), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                    if (System.IO.File.Exists(physicalPath2))
                    {
                        System.IO.File.Delete(physicalPath2);
                    }
                    _fileImage = "";
                }
            }
            return Content("");
        }

        public ActionResult SaveAttachFile(IEnumerable<HttpPostedFileBase> attachFile)
        {
            if (attachFile != null)
            {
                foreach (var file in attachFile)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Upload/Document"), fileName);

                    file.SaveAs(physicalPath);
                    _attachFileName = fileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveAttachFile(string[] fileNames)
        {
            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/Upload/Document"), fileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }
                    _attachFileName = "";
                }
            }
            return Content("");
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveSlideImages(IEnumerable<HttpPostedFileBase> slides)
        {
            var folderPath = "~/images/slideImages/";
            var largePath = "~/images/slideImages/largeImage/";
            var thumbPath = "~/images/slideImages/thumbnailImage/";

            if (!Directory.Exists(Server.MapPath(folderPath)))
            {
                Directory.CreateDirectory(Server.MapPath(folderPath));
            }

            if (!Directory.Exists(Server.MapPath(largePath)))
            {
                Directory.CreateDirectory(Server.MapPath(largePath));
            }

            if (!Directory.Exists(Server.MapPath(thumbPath)))
            {
                Directory.CreateDirectory(Server.MapPath(thumbPath));
            }

            if (slides != null)
            {
                foreach (var file in slides)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(Server.MapPath(largePath), finalFileName);

                        file.SaveAs(physicalPath);

                        var r = new ResizeSettings { MaxWidth = 185, MaxHeight = 125, Mode = FitMode.Crop };
                        ImageBuilder.Current.Build(largePath + finalFileName, thumbPath + finalFileName, r);

                        _lstSlideImages.Add(finalFileName);
                    }
                }
            }
            return Content("");
        }
    }
}

