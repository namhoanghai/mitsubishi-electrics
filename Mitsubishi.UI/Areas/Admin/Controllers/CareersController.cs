﻿using System;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    [Authorize]
    public class CareersController : Controller
    {
        private readonly CareersService _service;
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();

        public CareersController()
        {
            _service = new CareersService();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            var entity = new CareersEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(CareersEntity entity)
        {
            var result = false;

            if (entity.ItemID == 0)
            {
                result = (_service.Create(entity) > 0);

                if (result)
                {
                    ViewBag.ErrorMessage = "Product category " + entity.TitleEN + " have been created successfully.";
                }
            }
            else
            {
                result = _service.Update(entity);

                if (result)
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Edit");
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, CareersEntity entity)
        {
            if (entity != null && ModelState.IsValid)
            {
                _service.Create(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, CareersEntity entity)
        {
            if (entity != null)
            {
                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}

