﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    public class BannerController : Controller
    {
        private readonly BannerService _service;
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private static string _banner = string.Empty;

        public BannerController()
        {
            _service = new BannerService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(BannerEntity entity)
        {
            var result = false;

            if (!string.IsNullOrEmpty(_banner))
            {
                entity.ImageName = _banner;
            }

            if (entity.ItemID == 0)
            {
                result = (_service.Create(entity) > 0);

                _banner = string.Empty;
            }
            else
            {
                result = _service.Update(entity);

                _banner = string.Empty;

                if (result)
                {
                    return RedirectToAction("Index");
                }
            }

            return RedirectToAction("Edit");
        }

        public ActionResult Edit()
        {
            var entity = new BannerEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            var result = Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return result;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, BannerEntity entity)
        {
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(entity.ImageName) &&
                    System.IO.File.Exists(Server.MapPath("~/images/banners/" + entity.ImageName)))
                {
                    System.IO.File.Delete(Server.MapPath("~/images/banners/" + entity.ImageName));
                }

                _service.Destroy(entity);
            }

            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult SaveBanner(IEnumerable<HttpPostedFileBase> fileBanner)
        {
            if (fileBanner != null)
            {
                foreach (var file in fileBanner)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath("~/images/banners/"), finalFileName);

                    file.SaveAs(physicalPath);

                    _banner = finalFileName;
                }
            }
            return Content("");
        }

        public ActionResult RemoveBanner(string[] fileBanner)
        {
            if (fileBanner != null)
            {
                foreach (var fullName in fileBanner)
                {
                    var fileName = Path.GetFileName(fullName);
                    var finalFileName = string.Format("{0:yyMMMddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), finalFileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _banner = "";
                }
            }
            return Content("");
        }

        public JsonResult SetIsActive(int itemId, bool isActive)
        {
            var result = _service.SetIsActive(itemId, isActive);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListUrl()
        {
            var lst = new WebUrlService().Read();

            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}