﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HtmlAgilityPack;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Web;
using System.IO;

namespace Mitsubishi.UI.Areas.Admin.Controllers
{
    public class VideoController : Controller
    {
        private readonly ServiceConfiguration _config = new ServiceConfiguration();
        private readonly VideoService _service;
        private static string _fileImage = "";
        public VideoController()
        {
            _service = new VideoService();
        }
        // GET: Admin/Video
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_service.Read().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit()
        {
            var entity = new VideoEntity();

            if (Request.QueryString["itemId"] != null)
            {
                var itemId = Convert.ToInt32(Request.QueryString["itemId"]);

                entity = _service.GetItem(itemId);
            }

            return View(entity);
        }
        public ActionResult SaveImage(IEnumerable<HttpPostedFileBase> fileImageVideo)
        {
            if (fileImageVideo != null)
            {
                foreach (var file in fileImageVideo)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var finalFileName = string.Format("{0:yymmddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath("~/images/video/"), finalFileName);

                    file.SaveAs(physicalPath);

                    var r = new ResizeSettings { MaxWidth = 770, MaxHeight = 407, Mode = FitMode.Crop };
                    ImageBuilder.Current.Build("~/images/video/" + finalFileName, "~/images/video/" + finalFileName, r);

                    _fileImage = finalFileName;
                }
            }
            return Content("");
        }
        public ActionResult RemoveImage(string[] fileImageVideo)
        {
            if (fileImageVideo != null)
            {
                foreach (var fullName in fileImageVideo)
                {
                    var fileName = Path.GetFileName(fullName);
                    var finalFileName = string.Format("{0:yymmddHHmmss}", DateTime.Now) + "_" + fileName;

                    var physicalPath = Path.Combine(Server.MapPath(_config.RelativePath(_config.ImagePath)), finalFileName);

                    if (System.IO.File.Exists(physicalPath))
                    {
                        System.IO.File.Delete(physicalPath);
                    }

                    _fileImage = "";
                }
            }
            return Content("");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult CreateItem(VideoEntity entity)
        {
            var result = false;

            if (_fileImage.Any())
            {

            }
            if (_fileImage != "")
            {
                entity.Thumbnail = _fileImage;
            }

            int LastID = 0;

            if (entity.ItemID == 0)
            {
                LastID = _service.Create(entity);

                return RedirectToAction("Edit", "Video", new { itemId = LastID });

            }
            else
            {
                result = _service.Update(entity);



                if (result)
                {
                    return RedirectToAction("Edit", "Video", new { itemId = entity.ItemID });
                }
            }

            return RedirectToAction("Edit");
        }

    }
}