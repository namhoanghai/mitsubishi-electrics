﻿using System.Web.Mvc;

namespace Mitsubishi.UI.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                 "admin-huc",
                 "quantri",
                 new { controller = "Home", action = "Index"},
                 new string[] { "Mitsubishi.UI.Areas.Admin.Controllers" }
             );

           
            context.MapRoute(
                 "admin_default",
                 "Admin/{controller}/{action}/{id}",
                 new { action = "Index", id = UrlParameter.Optional },
                 new string[] { "Mitsubishi.UI.Areas.Admin.Controllers" }
             );

        }
    }
}