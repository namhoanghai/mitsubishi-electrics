﻿using System.Linq;
using System.Web.Mvc;

namespace Mitsubishi.UI.Filters
{
    public class SecurityCheck : ActionFilterAttribute, IActionFilter
    {
        public string Role { get; set; }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            //filterContext.HttpContext.User.Identity.Name
            using (var db = new Models.ApplicationDbContext())
            {
                var find = db.Users.Where(m => m.UserName == filterContext.HttpContext.User.Identity.Name).FirstOrDefault();
                if(find==null)
                {
                    filterContext.Result = new JsonResult() {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new { success = false, msg="Không có quyền truy cập"}
                    };
                }
                else
                {
                    if(string.IsNullOrWhiteSpace(find.role) || !Role.Contains(find.role))
                    {

                        filterContext.Result = new JsonResult()
                        {
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                            Data = new { success = false }
                        };
                    }
                }


            }
        }
    }
}