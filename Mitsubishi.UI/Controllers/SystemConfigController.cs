﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mitsubishi.UI.Controllers
{
    public class SystemConfigController : Controller
    {
        private Models.Digi digicontext = new Models.Digi();
        // GET: SystemConfig
        public ActionResult Index()
        {            
            return View();
        }

        public void getconfig()
        {
            if (HttpRuntime.Cache.Get("__configs") != null)
            {
                ViewBag.__configs = HttpRuntime.Cache.Get("__configs");
            }
        }
    }
}