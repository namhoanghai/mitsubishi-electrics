﻿using System.Linq;
using System.Web.Mvc;

namespace Mitsubishi.UI.Controllers.Base
{
    public class BaseController : Controller
    {
        protected Models.Digi _db;

        public BaseController()
        {
            _db = new Models.Digi();

            ViewBag.configs = _db.SystemConfigs.ToDictionary(m=>m.ParamName,m=>m);
        }

    }
   
}