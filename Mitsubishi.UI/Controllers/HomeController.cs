﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.ServiceModel.Syndication;
using System.Net;
using System.Net.Mail;
using Mitsubishi.UI.Controllers.Base;

namespace Mitsubishi.UI.Controllers
{
    public class HomeController : BaseController
    {
        private static readonly ServiceConfiguration Configuration = new ServiceConfiguration();
        
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductPage()
        {
            return View();
        }
        public ActionResult Rss()
        {
            
            /*
            List<SyndicationItem> items = new SyndicationItem[] 
            {
                new SyndicationItem("Blog 1", "Joe's 1st Blog", null),
                new SyndicationItem("Blog 1", "Joe's 1st Blog", null),
                new SyndicationItem("Blog 1", "Joe's 1st Blog", null)
            }.ToList();
          */
            List<SyndicationItem> items = new List<SyndicationItem>();
            var lstSotay = new ManualService().GetListAll();

            foreach (var entity in lstSotay)
            {
                var des = System.Net.WebUtility.HtmlDecode(entity.LongDescription);
                des = Regex.Replace(des, "<.*?>", string.Empty).Trim();
                if (des.Length > 300) des = des.Substring(0, 300);

                var url = Url.RouteUrl("ManualCategoryExtra", new
                    {
                       
                        title = Utils.ConvertUrf8ToAscii(entity.Title),
                        ManualCategoryExtraID = entity.ItemID
                    });
                SyndicationItem item = new SyndicationItem(entity.Title, des, new Uri("https://mitsubishi-electric.vn" + url), "" + entity.ItemID + "", DateTimeOffset.Now);
                 items.Add(item);
            }
           


            SyndicationFeed feed = new SyndicationFeed();
            feed.Items = items;


            return new RssResult(feed);
        }
        private static List<LocalizationEntity> InitLocalization()
        {
            List<LocalizationEntity> lst;

            if (Configuration.EnableCached)
            {
                var cachedName = GlobalConstants.CACHED_LOCALIZATION;

                if (System.Web.HttpContext.Current.Cache[cachedName] != null)
                {
                    lst = (List<LocalizationEntity>)System.Web.HttpContext.Current.Cache[cachedName];
                }
                else
                {
                    lst = new LocalizationService().GetList();

                    System.Web.HttpContext.Current.Cache.Insert(cachedName, lst, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
                }
            }
            else
            {
                lst = new LocalizationService().GetList();
            }

            return lst;
        }

        public static string GetParamValue(string name)
        {
            var entity = new ParamConfigService().GetItem(name);

            if (entity != null)
                return entity.ParamValue;

            return name;
        }

        public static string GetTextValue(string itemKey)
        {
            var result = itemKey;

            var lst = InitLocalization();

            if (lst != null)
            {
                var obj = lst.FirstOrDefault(x => x.ItemKey == itemKey && x.LanguageID == UtilsController.GetCurrentLanguage());

                if (obj != null && !string.IsNullOrEmpty(obj.Title))
                {
                    result = obj.Title;
                }
            }

            return result;
        }

        public static List<NavigationEntity> GetListNavigation(int parentId = 0)
        {
            List<NavigationEntity> lst;

            if (Configuration.EnableCached)
            {
                var cachedName = GlobalConstants.CACHED_NAV_SUB + parentId;

                if (System.Web.HttpContext.Current.Cache[cachedName] != null)
                {
                    lst = (List<NavigationEntity>)System.Web.HttpContext.Current.Cache[cachedName];
                }
                else
                {
                    lst = new NavigationService().GetList(parentId);

                    System.Web.HttpContext.Current.Cache.Insert(cachedName, lst, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
                }
            }
            else
            {
                lst = new NavigationService().GetList(parentId);
            }

            return lst;
        }

        public static NavigationEntity GetItem(int itemId)
        {
            var entity = new NavigationService().GetItem(itemId);

            return entity;
        }
        
        public ActionResult GeneralInquiry()
        {
            var lst = new List<ProductCategoryEntity>();
            var ids = Request.QueryString["selected"];

            if (!string.IsNullOrEmpty(ids))
            {
                var arr = ids.Split(',').ToList();
                var service = new ProductCategoryService();

                foreach (var id in arr)
                {
                    if (!string.IsNullOrEmpty(id) && id != "0")
                    {
                        lst.Add(service.GetItem(Convert.ToInt32(id)));        
                    }
                }
            }

            Session["enqiry_list"] = lst;
            return View(lst);
        }

        public ActionResult ProductInquiry()
        {
            ViewData[GlobalConstants.LIST_ROOM_AIR_CONDITIONER] = new ProductCategoryService().GetListByParentId(14, false);
            ViewData[GlobalConstants.LIST_PACKAGE_AIR_CONDITIONER] = new ProductCategoryService().GetListByParentId(107, false);
            
            ViewData[GlobalConstants.LIST_REFRIGERATOR] = new ProductCategoryService().GetListByParentId(2, false);
            ViewData[GlobalConstants.LIST_ELECTRICFANS] = new ProductCategoryService().GetListByParentId(4, false);
            ViewData[GlobalConstants.LIST_VENTILATOR] = new ProductCategoryService().GetListByParentId(3, false);
            ViewData[GlobalConstants.LIST_JETTOWEL] = new ProductCategoryService().GetListByParentId(5, false);

            ViewData[GlobalConstants.LIST_VRF] = new ProductCategoryService().GetListByParentId(110, false);
            ViewData[GlobalConstants.LIST_LOSSNAY] = new ProductCategoryService().GetListByParentId(105, false);
            ViewData[GlobalConstants.LIST_MULTISPLIT] = new ProductCategoryService().GetListByParentId(125, false);

            ViewData[GlobalConstants.LIST_FACTORY_AUTOMATION] = new ProductCategoryService().GetListByParentId(13, false);
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SendEmailInquiry(string comment, string name, string company, string address, string city,
            string country, string phone, string email, string radioRequest)
        {
            var result = false;
            var title = "Inquiry";
            var sb = new StringBuilder();

            
            sb.Append("<table width='100%;' cellspacing='3' cellpadding='3'>");
            sb.Append("<tr><td style='width:120px;'>Comment</td><td><h2>Product Inquiry</h2></td>");
            sb.Append("<tr><td style='width:120px;'>Type</td><td>" + radioRequest + "</td>");
            sb.Append("<tr><td style='width:120px;'>Comment</td><td>" + comment + "</td>");
            sb.Append("<tr><td style='width:120px;'>Name</td><td>" + name + "</td>");
            sb.Append("<tr><td style='width:120px;'>Company</td><td>" + company + "</td>");
            sb.Append("<tr><td style='width:120px;'>Address</td><td>" + address + "</td>");
            sb.Append("<tr><td style='width:120px;'>City</td><td>" + city + "</td>");
            sb.Append("<tr><td style='width:120px;'>Country</td><td>" + country + "</td>");
            sb.Append("<tr><td style='width:120px;'>Phone</td><td>" + phone + "</td>");
            sb.Append("<tr><td style='width:120px;'>Email</td><td>" + email + "</td>");
            sb.Append("</table>");

            //list of selected product
            var lst = new List<ProductCategoryEntity>();
            if (Session["enqiry_list"]!=null)
            {
                lst = (List<ProductCategoryEntity>)Session["enqiry_list"];
            }

            sb.Append("<div>");
            sb.Append("<p><strong>List Products</strong></p>");
            foreach (var p in lst)
            {                
                sb.Append("<div> - "+p.Title+"</div>");
            }
            sb.Append("</div>");

            result = Utils.SendInquiry(title, sb.ToString());

            if (result && new InquiryService().Create(comment, name, company, address, city, country, phone, email, 0) > 0)
            {
                Session["enqiry_list"] = null;
                return RedirectToAction("SendInquirySuccess");
            }
            else
            { return View("Error"); }

          
        }

        public ActionResult testemail()
        {
            var result = false;

            var config = new ParamConfigService();

            var mailMessage = new MailMessage();

            var emailTo = config.GetItem("TIP_EMAIL").ParamValue;

            var smtpHost = config.GetItem("SMTP_HOST").ParamValue;
            var smtpPort = config.GetItem("SMTP_PORT").ParamValue;
            var authenName = config.GetItem("AUTHENTICATION_NAME").ParamValue;
            var authenPassword = config.GetItem("AUTHENTICATION_PASSWORD").ParamValue;

            var mailClient = new SmtpClient(smtpHost, Convert.ToInt16(smtpPort));

            mailClient.UseDefaultCredentials = false;
            mailClient.EnableSsl = true;
            mailClient.Credentials = new NetworkCredential(authenName, authenPassword);
            mailClient.Timeout = 100000;

            mailMessage.IsBodyHtml = true;
            mailMessage.From = new MailAddress(authenName);
            mailMessage.Subject = "Test";

            string body = "test";

            mailMessage.Body = !string.IsNullOrEmpty(body) ? body : string.Empty;


            mailMessage.To.Clear();

            mailMessage.To.Add("nam.hoang@digipencil.vn");

           
                mailClient.Send(mailMessage);

                result = true;
                return View();
           
        }

        public ActionResult SendInquirySuccess()
        {
            return View();
        }

        public ActionResult WarrantyTerm()
        {
            if (Request.Url != null)
            {
                var url = Request.Url.AbsolutePath;

                var nav = new NavigationService().GetItem(url);

                if (nav != null)
                {
                    ViewBag.CurrentPage = nav.ItemID;
                }
            }

            var itemId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WARRANTY_TERM"]);

            var item = new ContentService().GetItem(itemId);

            return View(item);
        }

        public ActionResult WhereToBuy()
        {
            if (Request.Url != null)
            {
                var url = Request.Url.AbsolutePath;

                var nav = new NavigationService().GetItem(url);

                if (nav != null)
                {
                    ViewBag.CurrentPage = nav.ItemID;
                }
            }

            ViewData["ProductTypes"] = new ProductCategoryService().GetListActive(0);

            return View();
        }

        public JsonResult GetListAgency(int? locationId)
        {
            var lst = new AgencyService().GetList(locationId);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListAgency1(int? locationId, int? typeId)
        {
            var lst = new AgencyService().GetList(locationId);

            if (typeId != 0 && typeId != null) lst = lst.Where(c => c.ProductTypeID == typeId).ToList();

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FindAgencyNearBy(int? typeId, int? categoryId, string myPosition, int distance)
        {
            var lst = new AgencyService().GetList(typeId, categoryId, myPosition, distance * 1000);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListAuthorized(int locationId)
        {
            List<AgencyEntity> lst;

            lst = new AgencyService().GetListAuthorized(locationId);
            

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FindAuthorizedNearBy(string myPosition, int distance)
        {
            var lst = new AgencyService().GetListAuthorized(myPosition, distance * 1000);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListLocation()
        {
            var lst = new LocationService().GetList();

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListProductType()
        {
            var lst = new ProductTypeService().GetList();

            if (lst.Count == 0) { 
                var protype = new ProductTypeEntity();
                protype.Title = "Thiết bị gia dụng";
                protype.ItemID = 1;

                lst.Add(protype);
                
                var protype1 = new ProductTypeEntity();
                protype1.Title = "Điều hòa không khí dự án";
                protype1.ItemID = 12;

                lst.Add(protype1);

                var protype2 = new ProductTypeEntity();
                protype2.Title = "Tự động hóa công nghiệp";
                protype2.ItemID = 13;

                lst.Add(protype2);
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AuthorizedCenter()
        {
            if (Request.Url != null)
            {
                var url = Request.Url.AbsolutePath;

                var nav = new NavigationService().GetItem(url);

                if (nav != null)
                {
                    ViewBag.CurrentPage = nav.ItemID;
                }
            }

            //var lst = new AgencyService().GetListAuthorized();

            return View();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            var itemId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CONTACT_US"]);

            var entity = new ContentService().GetItem(itemId);
            
            return View(entity);
        }

        public ActionResult Sitemap()
        {
            return View();
        }

        public ActionResult Upload()
        {
            DirectoryInfo info = new DirectoryInfo(Server.MapPath("/Content"));
            FileInfo[] files = info.GetFiles().OrderByDescending(p => p.LastWriteTime).ToArray();
            var lstFiles = new List<FileInfo>();

            foreach (var file in files)
            {
                lstFiles.Add(file);
            }

            return View(lstFiles);

        }

        public ActionResult Js()
        {
            DirectoryInfo info = new DirectoryInfo(Server.MapPath("/Scripts"));
            FileInfo[] files = info.GetFiles().OrderByDescending(p => p.LastWriteTime).ToArray();
            var lstFiles = new List<FileInfo>();

            foreach (var file in files)
            {
                lstFiles.Add(file);
            }

            return View(lstFiles);

        }

        public JsonResult RemoveFile(string fileName)
        {
            var path = Server.MapPath("~/Content/" + fileName);

            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveFileUpload()
        {
            var fileName = string.Empty;

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("/Content/"), fileName);
                    file.SaveAs(path);
                }
            }

            return RedirectToAction("Upload");
        }

        [HttpPost]
        public ActionResult SaveJs()
        {
            var fileName = string.Empty;

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("/Scripts/"), fileName);
                    file.SaveAs(path);
                }
            }

            return RedirectToAction("Js");
        }

        public static List<BannerEntity> GetListBanners()
        {
            var lst = new BannerService().GetList();

            return lst;
        }

        public ActionResult Search()
        {
            return View();
        }
        public ActionResult NotFoundPage()
        {
            return View("_404");
        }
        public ActionResult ErrorServer()
        {
            return View("_500");
        }
        public ActionResult ErrorDefault()
        {

            return View("_Error");
        }
    }
}