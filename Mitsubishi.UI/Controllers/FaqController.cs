﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Text;

namespace Mitsubishi.UI.Controllers
{
    public class FaqController : Controller
    {
        // GET: Publish/Faq
        public ActionResult Index()
        {
            var lst = new FaqService().GetList(12, true);
            
            return View(lst);
        }
        public List<FaqEntity> GetList(int count)
        {
            var lst = new FaqService().GetList(count, true);

            return lst;
        }
        
        public ActionResult DatCauHoi()
        {            
            return View();
        }
         [AcceptVerbs(HttpVerbs.Post)]
         public ActionResult Faq_result(string name, string email, string question)
         {
             if (name != "" && email != "" && question!="")
             {
                 FaqEntity e = new FaqEntity();
                 e.Question = question;
                 e.Name = name;
                 e.Email = email;
                 e.Order = 1;
                 e.IsActive = false;

                 FaqService _service = new FaqService();
                 var a = _service.Create(e);
                

                 return RedirectToAction("DatCauHoi", "Faq");
             }
             return View();
         }
    }
}