﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Text;

namespace Mitsubishi.UI.Controllers
{
    public class VideoController : Controller
    {
        // GET: Publish/Video
        public ActionResult Index()
        {
            return View();
        }
        public List<VideoEntity> GetList(int count)
        {
            var lst = new VideoService().GetList(count, true);
            return lst;
        }
    }
}