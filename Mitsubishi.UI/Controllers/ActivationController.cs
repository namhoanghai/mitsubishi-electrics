﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Text;

namespace Mitsubishi.UI.Controllers
{
    public class ActivationController : Controller
    {
        // GET: Publish/Activation
        public ActionResult Index()
        {
            return View();
        }

        public List<ActivationEntity> GetList(int count)
        {
            var lst = new ActivationService().GetList(count,true);
            return lst;
        }

        public ActionResult DiaDiemActivation()
        {
            var lst = new ActivationService().GetList(-1);
            return View(lst);
        }
    }
}