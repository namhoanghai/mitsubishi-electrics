﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Text;
using System.Text.RegularExpressions;

using System.Net;


using PagedList; 

namespace Mitsubishi.UI.Controllers
{
    public class ManualController : Controller
    {
       
        // GET: Publish/Manual
        public ActionResult Index()
        {

            string str = "";

            ViewBag.Url = "http://mitsubishi-electric.vn/so-tay-gia-dung.htm";

           

            if (this.Request.QueryString["s"] != "" && Request.QueryString["s"] != null)
            {
               // var c_page = this.Request.QueryString["page"];
               // str = this.Request.QueryString["s"];
               // int t_page = 1;
               // if (c_page != null)
                //{
                //    t_page = int.Parse(c_page);
                //}

               // var filteredItems = new ManualController().GetListItemBySearch(str, t_page);

               return View();
            }
            else
            {
              //  var c_page = this.Request.QueryString["page"];
               // int t_page = 1;
              //  if (c_page != null)
              //  {
               //     t_page = int.Parse(c_page);
               // }
               // var lst2 = new ManualController().GetListItemByCategory(str, t_page);

                return View();
            }

           
        }

        public ActionResult Playlist()
        {
            return View();
        }

        public ActionResult SotayFaq()
        {
            return View();
        }

        //gửi email hỏi đáp
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SendEmail(string name, string email, string question)
        {
            var result = false;
            var title = "Sổ tay gia dụng";

            var sb = new StringBuilder();
            sb.Append("<table width='100%;' cellspacing='3' cellpadding='3'>");
            sb.Append("<tr><td style='width:120px;'>Chuyên mục</td><td><h2>Sổ tay gia dụng</h2></td>");
            sb.Append("<tr><td style='width:120px;'>Họ tên</td><td>" + name + "</td>");
            sb.Append("<tr><td style='width:120px;'>Email</td><td>" + email + "</td>");
            sb.Append("<tr><td style='width:120px;'>Câu hỏi</td><td>" + question + "</td>");
            sb.Append("</table>");
            result = Utils.SendTipQuestion(email , title, sb.ToString());


            //true 
             return Json(result,JsonRequestBehavior.AllowGet);
            
           
            /*
            var message = new MailMessage();
            message.To.Add(new MailAddress(ConfigurationManager.AppSettings["ToEmail"]));  // replace with valid value 
            message.From = new MailAddress(ConfigurationManager.AppSettings["UserEmail"]);  // replace with valid value
            message.Subject = ConfigurationManager.AppSettings["SubjectEmail"];
            message.Body = body;
            message.IsBodyHtml = true;


            var smtp = new SmtpClient();
           
                var credential = new NetworkCredential
                {
                    UserName = ConfigurationManager.AppSettings["UserEmail"],
                    Password = ConfigurationManager.AppSettings["PassEmail"]
                };
                smtp.Credentials = credential;
                smtp.Host = ConfigurationManager.AppSettings["HostEmail"];
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["PortEmail"]);
                
                smtp.EnableSsl = true;
                smtp.SendMailAsync(message);
             * */
               
        }

        public ActionResult Autocomplete_Search(string term)
        {
            //tìm kiếm ajax, tìm trong title và description
            IEnumerable<ManualContentEntity> lst = new ManualService().GetListAll();
            var filteredItems = lst.Where(s =>
                RemoveSign4VietnameseString(s.Title).ToLower().Contains(RemoveSign4VietnameseString(term).ToLower())).Select(w => w).Take(3).ToList();

            IList<SelectListItem> List = new List<SelectListItem>();
            foreach (ManualContentEntity e in filteredItems)
            {
                var url = Url.RouteUrl("ManualDetail", new
                {
                    title = Utils.ConvertUrf8ToAscii(e.Title),
                    itemId = e.ItemID
                });

                List.Add(new SelectListItem { Text = e.Title, Value = url});
            }

            return Json(List, JsonRequestBehavior.AllowGet);
        }



        public List<ProductCategoryEntity> GetListProductByCatId(int catId, int count)
        {

            var lst = new ProductCategoryService().GetListActive(catId, count);

            return lst;
        }

        public List<ManualCategoryEntity> GetListRoot()
        {
            var lst = new ManualCategoryService().GetListRoot();
            return lst;
        }
       
        public ActionResult ManualDetail()
        {
            var tmp = new ManualCategoryEntity();
            //Mặc định lấy Parent = 0
            int str = 0;
            if (RouteData.Values["itemId"] != null)
            {
                str = int.Parse(RouteData.Values["itemId"].ToString());

            }

           
            var item = new ManualService().GetItem(str);

            var url = Url.RouteUrl("ManualDetail", new
            {
                title = Utils.ConvertUrf8ToAscii(item.Title),
                itemId = item.ItemID
            });

            ViewBag.Url = "https://mitsubishi-electric.vn" + url;

            var des = item.LongDescription;

            des = Regex.Replace(des, "<.*?>", string.Empty).Trim();

            if (des.Length > 250) ViewBag.Description = des.Substring(0, 250);
            else ViewBag.Description = des;

            return View(item);
        }

        public ActionResult ListItemManual()
        {            
            return View();
        }

        public PagedList.IPagedList<ManualContentEntity> GetListItemByCategory(String str, int? page, string ManualCategoryExtraID, int Isvideo = 0)
        {
            var lst = new ManualService().GetList(str, 0, ManualCategoryExtraID, Isvideo);
            int pageSize = 13 ;
            int pageNumber = (page ?? 1);

            return lst.ToPagedList(pageNumber , pageSize );
            //return lst;
        }



        public ActionResult GetListItemByCategory_Ajax(string termID, int? page , string ManualCategoryExtraID)
        {
            var lst = new ManualService().GetList(termID, 0,  ManualCategoryExtraID);
           // int pageSize = 6;
           // int pageNumber = (page ?? 1);

          //  return lst.ToPagedList(pageNumber, pageSize);
            //return lst;

            var html = "<div class=\"top-item-article\">";
            foreach (var item in lst)
            {
                var title = item.Title;

                Regex regex2 = new Regex(" ");
                string[] substrings2 = regex2.Split(title);
                string tmp2 = "";

                foreach (string match in substrings2)
                {

                    tmp2 += match + " ";
                    if (tmp2.Length > 35)
                    {
                        tmp2 += "...";
                        break;
                    }
                }
                title = tmp2;                
                
                var url = Url.RouteUrl("ManualDetail", new
                {
                    title = Utils.ConvertUrf8ToAscii(title),
                    itemId = item.ItemID
                });

                var des = Regex.Replace(item.LongDescription, "<.*?>", string.Empty).Trim();
                des = HttpUtility.HtmlDecode(des);

                Regex regex = new Regex(" ");
                string[] substrings = regex.Split(des);
                string tmp = "";
               
                foreach (string match in substrings)
                {
                    
                    tmp += match + " ";
                    if (tmp.Length > 90)
                    {
                        tmp += "...";
                        break;
                    }
                }
                des = tmp;
                
              //  if (des.Length > 90)
               // {
                    
               //     des = des.Substring(0, 90) + "...";
               // }


                html += "<div class=\"col-md-4 col-sm-6 col-xs-12 item-manual\">";
                html += "<div class=\"thumbnail-item\">";
                html += "   <a href=\"" + url + "\"><img src=\"/images/sotaygiadung/" + @item.Thumbnail + "\" title=\"" + title + "\" alt=\"" + title + "\"><span class=\"hover-element-article\"></span></a>";
                html += "</div>";
                html += "<div class=\"\">";
                html += "<p class=\"cat-label\">" + @item.Category.Title + "</p>";
                html += "     <h2 class=\"title-manual\"><a  href=\"" + url + "\">" + title + "</a></h3>";
                html += "<p class=\"createDate\">"+string.Format("{0:dd/MM/yyyy}", item.CreatedDate)+"</p>";
                html += "    <p class=\"description\">" + des + "";
                html += " </div>";
                html += "<div class=\"clear\"></div>";
                html += "</div>";

            }
            html += "<div class=\"clear\"></div>";
            html += "</div>";


            return Json(html, JsonRequestBehavior.AllowGet);
        }


        public List<ManualContentEntity> Related( int count, int exclude , int catID)
        {
            var lst = new ManualService().GetListRelated(exclude, count, catID);
            return lst;

        }
    
        public List<ManualContentEntity> ManualInProduct_Block(string str , int count)
        {
            var lst = new ManualService().GetListWithCount(str,count);
            return lst;
           
        }

        //check exist cat ID in array catID
        public int ExistCatID(int catID, List<int> lst)
        {
            bool kt = false;
            kt = lst.Contains(catID);
            if (kt == false) return 0;
            else return 1;
            
            
        }

        public PagedList.IPagedList<ManualContentEntity> GetListItemBySearch(String str  , int? page = 1)
        {

            var lst = new ManualService().GetListAll();

            var filteredItems = lst.Where(s =>
                RemoveSign4VietnameseString(s.Title).ToLower().Contains(RemoveSign4VietnameseString(str).ToLower())).Select(w => w).ToList();

            
            int pageSize = 13;
            int pageNumber = (page ?? 1);

            return filteredItems.ToPagedList(pageNumber, pageSize);
            //return filteredItems;
        }

        public ManualContentEntity GetFirstItem()
        {
            var entity = new ManualService().GetFirstItem();

            return entity;
        }



        public List<ManualCategoryEntity> GetListByLevel(int level)
        {
            var lst = new ManualCategoryService().GetListByLevel(level);
            return lst;

      }

        public  ManualCategoryEntity GetItem(int itemId)
        {
            var entity = new ManualCategoryService().GetItem(itemId);

            return entity;
        }

        public ManualContentEntity GetItemManual(int itemId)
        {
            var item = new ManualService().GetItem(itemId);

            return item;
        }

        public  List<ManualCategoryEntity> GetList(string taxonomyId)
        {
            var lst = new ManualCategoryService().GetListCategory(taxonomyId);

            return lst;
        
        }

        public List<ManualContentEntity> GetListVideo(string cats, string count)
        {
            var lst = new ManualService().GetListVideo( cats,  count);
            return lst;
        }
        public List<ManualContentEntity> GetListVideoRelated(string cats, string count, int exID)
        {
            var lst = new ManualService().GetListVideoRelated(cats, count ,   exID);
            return lst;
        }

        private static readonly string[] VietnameseSigns = new string[]
            {

                "aAeEoOuUiIdDyY",

                "áàạảãâấầậẩẫăắằặẳẵ",

                "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

                "éèẹẻẽêếềệểễ",

                "ÉÈẸẺẼÊẾỀỆỂỄ",

                "óòọỏõôốồộổỗơớờợởỡ",

                "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

                "úùụủũưứừựửữ",

                "ÚÙỤỦŨƯỨỪỰỬỮ",

                "íìịỉĩ",

                "ÍÌỊỈĨ",

                "đ",

                "Đ",

                "ýỳỵỷỹ",

                "ÝỲỴỶỸ"

            };



        public static string RemoveSign4VietnameseString(string str)
        {

            //Tiến hành thay thế , lọc bỏ dấu cho chuỗi

            for (int i = 1; i < VietnameseSigns.Length; i++)
            {

                for (int j = 0; j < VietnameseSigns[i].Length; j++)

                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);

            }

            return str;

        }

    }
}