﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using Mitsubishi.UI.Controllers.Base;

namespace Mitsubishi.UI.Controllers
{

    public class ProductTypeController : BaseController
    {
        public ProductTypeController()
        {
        }

        public ActionResult Index()
        {
            var lst = new List<ProductCategoryEntity>();

            var str = (string)this.RouteData.Values["productTypeId"];

            if (!string.IsNullOrEmpty(str))
            {
                var id = Convert.ToInt32(str);

                lst = new ProductCategoryService().GetListActive(id,15);
            }

            return View(lst);
        }

        public ActionResult BusinessOwner() { return View(); }

        public static List<ProductCategoryEntity> GetList()
        {
            var lst = new ProductCategoryService().GetListRoot(true);

            return lst;
        }

        public static ProductTypeEntity GetItem(int itemId)
        {
            var item = new ProductTypeService().GetItem(itemId);

            return item;
        }
    }
}

