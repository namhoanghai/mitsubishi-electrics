﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ImageResizer;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
namespace Mitsubishi.UI.Controllers
{
    public class CommentController : Controller
    {
       
        
        public ActionResult Index()
        {
           
            return View();
        }
        public List<CommentEntity> GetListByProduct(int itemID)
        {
            var tmp = new CommentService().GetListByProduct(itemID);
            return tmp;
        }


        //gửi email hỏi đáp
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(string name, string email, string content, int ProductID)
        {
            
            CommentEntity e = new CommentEntity();
            e.Name = name;
            e.Description = content;
            e.Email = e.Email;
            e.IsActive = false;
            e.ProductID = ProductID;

            var kq = new CommentService().Create(e);


            //true 
            return Json(kq, JsonRequestBehavior.AllowGet);


           
        }


    }
}