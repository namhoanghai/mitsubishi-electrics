﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;
using System.Text;
using Mitsubishi.UI.Controllers.Base;

namespace Mitsubishi.UI.Controllers
{

    public class ProductCategoryController : BaseController
    {
        private readonly ProductCategoryService _service;
        public static ProductCategoryEntity currentItem = null;

        public ProductCategoryController()
        {
            _service = new ProductCategoryService();
        }

        private void AddItemViewed(ProductCategoryEntity item)
        {
            var lst = new List<ProductCategoryEntity>();

            if (Session[GlobalConstants.SESS_PRODUCT_VIEWED] != null)
            {
                lst = (List<ProductCategoryEntity>) Session[GlobalConstants.SESS_PRODUCT_VIEWED];
            }

            var result = lst.Select(x => x.ItemID == item.ItemID);

            if (!result.Any())
            {
                lst.Add(item);

                Session[GlobalConstants.SESS_PRODUCT_VIEWED] = lst;
            }
        }

        public ActionResult Index()
        {
            var currentLanguage = UtilsController.GetCurrentLanguage();

            var lst = new List<ProductCategoryEntity>();

            var str = (string)RouteData.Values["productCategoryId"];

            if (!string.IsNullOrEmpty(str))
            {
                var id = Convert.ToInt32(str);

                var item = _service.GetItemFullDetail(id);

                

                lst = _service.GetListActive(id,15);
               
                
                int count = lst.Count();



                //chỉ có 1 sp
                if (count == 1)
                {
                    //kiểm tra xem đây có phải là danh mục cuối chưa, nếu còn chuyên mục con thì hiển thị bình thường
                    //Nếu chỉ có 1 chuyên mục và là chuyên mục cuối thì vào luôn
                    var sub_lst = _service.GetListActive(lst[0].ItemID,15);
                    int sub_count = sub_lst.Count();
                    if ( sub_count > 0 )                    
                    {


                        //breadcumb
                        var entity = lst[0];
                        string BreadcumbStr = "";
                        var ProductTypeTitle = entity.ProductType.Title;
                        var ProductTypeID = entity.ProductType.ItemID;

                        
                        var tmp = entity;

                        for (int i = 0; i < 3; i++)
                        {

                            if (tmp.ParentID != 0 && tmp.ParentCategory != null)
                            {
                                var entityP = tmp.ParentCategory;

                                var title = entityP.Title;

                                if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                                {
                                    title = entity.TitleEN;
                                }

                                var url = "";

                                if (entityP.ParentID != 0 && entityP.ParentCategory != null)
                                {
                                    url = Url.RouteUrl("ProductCategory", new
                                    {
                                        ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                                        ProductTypeId = ProductTypeID,
                                        ProductCategoryName = Utils.ConvertUrf8ToAscii(title),
                                        productCategoryId = entityP.ItemID
                                    });
                                }
                                //product type
                                else
                                {
                                    url = Url.RouteUrl("ProductTypes", new
                                    {
                                        ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                                        productTypeId = ProductTypeID
                                    });
                                }


                                string strbread = "<a href=\"" + @url + "\"> " + title + " </a>/";
                                BreadcumbStr = strbread + BreadcumbStr;

                                tmp = entityP;
                            }
                            ViewBag.Breadcumb = BreadcumbStr + " " + Utils.StripTag(entity.Title);


                        }
                        //end breadcumb




                        return View(lst);
                    }
                    else
                    {
                        var the_item = new ProductCategoryService().GetFirstItem(id);


                        //breadcumb
                        var entity = the_item;
                        string BreadcumbStr = "";
                        var ProductTypeTitle = entity.ProductType.Title;
                        var ProductTypeID = entity.ProductType.ItemID;


                        var tmp = entity;

                        for (int i = 0; i < 3; i++)
                        {

                            if (tmp.ParentID != 0 && tmp.ParentCategory != null)
                            {
                                var entityP = tmp.ParentCategory;

                                var title = entityP.Title;

                                if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                                {
                                    title = entity.TitleEN;
                                }

                                var url = "";

                                if (entityP.ParentID != 0 && entityP.ParentCategory != null)
                                {
                                    url = Url.RouteUrl("ProductCategory", new
                                    {
                                        ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                                        ProductTypeId = ProductTypeID,
                                        ProductCategoryName = Utils.ConvertUrf8ToAscii(title),
                                        productCategoryId = entityP.ItemID
                                    });
                                }
                                //product type
                                else
                                {
                                    url = Url.RouteUrl("ProductTypes", new
                                    {
                                        ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                                        productTypeId = ProductTypeID
                                    });
                                }


                                string strbread = "<a href=\"" + @url + "\"> " + title + " </a>/";
                                BreadcumbStr = strbread + BreadcumbStr;

                                tmp = entityP;
                            }
                            ViewBag.Breadcumb = BreadcumbStr + " " + Utils.StripTag(entity.Title);


                        }
                        //end breadcumb

                        return View("Detail", the_item);
                    }
                    
                }
                //không còn sp con nào nữa
                else if (count == 0)
                {

                    //breadcumb
                    var entity = item;
                    string BreadcumbStr = "";
                    var ProductTypeTitle = entity.ProductType.Title;
                    var ProductTypeID = entity.ProductType.ItemID;


                    var tmp = entity;

                    for (int i = 0; i < 3; i++)
                    {

                        if (tmp.ParentID != 0 && tmp.ParentCategory != null)
                        {
                            var entityP = tmp.ParentCategory;

                            var title = entityP.Title;

                            if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                            {
                                title = entity.TitleEN;
                            }

                            var url = "";

                            if (entityP.ParentID != 0 && entityP.ParentCategory != null)
                            {
                                url = Url.RouteUrl("ProductCategory", new
                                {
                                    ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                                    ProductTypeId = ProductTypeID,
                                    ProductCategoryName = Utils.ConvertUrf8ToAscii(title),
                                    productCategoryId = entityP.ItemID
                                });
                            }
                            //product type
                            else
                            {
                                url = Url.RouteUrl("ProductTypes", new
                                {
                                    ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                                    productTypeId = ProductTypeID
                                });
                            }


                            string strbread = "<a href=\"" + @url + "\"> " + title + " </a>/";

                            //if (i == 0)
                            //{
                            //    strbread = " " + title + " /";
                            //}


                            BreadcumbStr = strbread + BreadcumbStr;

                            tmp = entityP;
                        }


                        ViewBag.Breadcumb = BreadcumbStr + " " + Utils.StripTag(entity.Title);


                    }
                    //end breadcumb


                    return View("Detail", item);
                }

                //breadcumb
                var entity2 = lst[0];
                string BreadcumbStr2 = "";
                var ProductTypeTitle2 = entity2.ProductType.Title;
                var ProductTypeID2 = entity2.ProductType.ItemID;

             
                var tmp2 = entity2;

                for (int i = 0; i < 3; i++)
                {

                    if (tmp2.ParentID != 0 && tmp2.ParentCategory != null)
                    {
                        var entityP = tmp2.ParentCategory;

                        var title = entityP.Title;

                        if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                        {
                            title = entity2.TitleEN;
                        }

                        var url = "";

                        if (entityP.ParentID != 0 && entityP.ParentCategory != null)
                        {
                            url = Url.RouteUrl("ProductCategory", new
                            {
                                ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle2),
                                ProductTypeId = ProductTypeID2,
                                ProductCategoryName = Utils.ConvertUrf8ToAscii(title),
                                productCategoryId = entityP.ItemID
                            });
                        }
                            //product type
                        else{
                             url = Url.RouteUrl("ProductTypes", new
                            {
                                ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle2),
                                productTypeId = ProductTypeID2
                            });
                        }

                        string strbread = "<a href=\"" + @url + "\"> " + title + " </a>/";

                        BreadcumbStr2 = strbread + BreadcumbStr2;

                        tmp2 = entityP;
                    }
                    ViewBag.Breadcumb = BreadcumbStr2 ;


                }
                //end breadcumb
               
            }

            return View(lst);
        }

        //get first product

        public ProductCategoryEntity GetFirstProduct(int productID)
        {
            var item = new ProductCategoryService().GetFirstItem(productID);
            return item;
        }


        //chi tiết sản phẩm
        public ActionResult Detail()
        {
            var str = (string)RouteData.Values["productCategoryId"];



            var entity = GetItem(int.Parse(str));


            //var e = new List<ProductCategoryEntity>();
            
            //breadcumb
            string BreadcumbStr = "";
            var ProductTypeTitle = entity.ProductType.Title;
            var ProductTypeID = entity.ProductType.ItemID;

            var currentLanguage = UtilsController.GetCurrentLanguage();

            var tmp = entity;

            for (int i = 0; i < 3; i++)
            {

                if (tmp.ParentID != 0 && tmp.ParentCategory != null)
                {
                    var entityP = tmp.ParentCategory;

                    var title = entityP.Title;

                    if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                    {
                        title = entity.TitleEN;
                    }

                    var url = "";

                    if (entityP.ParentID != 0 && entityP.ParentCategory != null)
                    {
                        url = Url.RouteUrl("ProductCategory", new
                        {
                            ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                            ProductTypeId = ProductTypeID,
                            ProductCategoryName = Utils.ConvertUrf8ToAscii(title),
                            productCategoryId = entityP.ItemID
                        });
                    }
                    //product type
                    else
                    {
                        url = Url.RouteUrl("ProductTypes", new
                        {
                            ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                            productTypeId = ProductTypeID
                        });
                    }

                    string strbread = "<a href=\"" + @url + "\"> " + title + " </a>/";

                    if (i == 0)
                    {
                        strbread = " "+ title + " / ";
                    }
                    
                    BreadcumbStr = strbread + BreadcumbStr ;

                    tmp = entityP ;
                }
                ViewBag.Breadcumb = BreadcumbStr + " " + Utils.StripTag(entity.Title);
                //end breadcumb

            }




            return View(entity);
          
        }

        public static List<ProductCategoryEntity> GetList()
        {
            var lst = new ProductCategoryService().GetListRoot(true);

            return lst;
        }

        public ActionResult ListProductByCategory()
        {
            //var lst = new ProductService().GetListProductByCategory(11);
            var parentId = (string)RouteData.Values["productCategoryId"];

            int tmp_id = int.Parse(parentId);
            var lst = new ProductCategoryService().GetChilds(tmp_id);

            int count = lst.Count();
            
            //nếu 
            if (count > 1)
            {
                return View(lst);
            }
                //nếu chỉ có 1 sp sẽ vào thẳng SP đó luôn
            else if(count == 1)
            {
                int ItemID = lst[0].ItemID;
                var item = new ProductCategoryService().GetFirstItem(ItemID);
                return View("Detail", item);
            }
                //nếu ko có sản phẩm nào cuối nữa sẽ vào thằng hiện tại
            else if(count == 0){
                //get item id
                //var item = new ProductCategoryService().GetFirstItem(ItemID);
                var item = GetItem(int.Parse(parentId));
                return View("Detail", item);
            }
            return View(lst);
            
        
        }

        //kiểm tra là Product line ko
        public static bool  checkProductLine(string value)
        {
            string tmp = GlobalConstants.PRODUCT_LINE_ID;
           
            var ProductLineIDs = tmp.Split(',');
            
            foreach (var k in ProductLineIDs)
            {
                if (k == value)
                {
                    return true;
                }
            }            

            return false;
        }

      
       
        public  string getBreadcumb(ProductCategoryEntity entity)
        {
           









            var e = new List<ProductCategoryEntity>();

            string BreadcumbStr = "";
            var ProductTypeTitle = entity.ProductType.Title;
            var ProductTypeID = entity.ProductType.ItemID;

            var currentLanguage = UtilsController.GetCurrentLanguage();

            var tmp = entity;

            for (int i = 0; i < 3; i++)
            {

                if (tmp.ParentID != 0 && tmp.ParentCategory != null)
                {
                    var entityP = tmp.ParentCategory;

                    var title = entityP.Title;

                    if (currentLanguage == GlobalConstants.LANGUAGE_EN)
                    {
                        title = entity.TitleEN;
                    }



                    var url = Url.RouteUrl("ProductCategory", new
                    {
                        ProductTypeName = Utils.ConvertUrf8ToAscii(ProductTypeTitle),
                        ProductTypeId = ProductTypeID,
                        ProductCategoryName = Utils.ConvertUrf8ToAscii(title),
                        productCategoryId = entity.ItemID
                    });
                 
                  

                    string str = "<a href=\"#\"> " + title + " </a>/";
                    BreadcumbStr = str + BreadcumbStr;

                    tmp = entityP;
                }

               

            }

            return BreadcumbStr;
        }


        //lấy dánha sách các sp ccũ, đã coi
        public static List<ProductCategoryEntity> GetListViewed()
        {
            var lst = new List<ProductCategoryEntity>();

            if (System.Web.HttpContext.Current.Session[GlobalConstants.SESS_PRODUCT_VIEWED] != null)
            {
                lst = (List<ProductCategoryEntity>)System.Web.HttpContext.Current.Session[GlobalConstants.SESS_PRODUCT_VIEWED];
            }
            
            return lst;
        }

        public static string GetProductCategoryId()
        {
            var str = string.Empty;

            var ht = new HttpContextWrapper(System.Web.HttpContext.Current);
            
            var data = RouteTable.Routes.GetRouteData(ht);

            if (data != null && data.Values.Count > 0 && data.Values.ContainsKey("productCategoryId"))
            {
                str = data.Values["productCategoryId"].ToString();
            }

            return str;
        }

        public static string GetProductTypeId()
        {
            var str = string.Empty;

            var ht = new HttpContextWrapper(System.Web.HttpContext.Current);

            var data = RouteTable.Routes.GetRouteData(ht);

            if (data != null && data.Values.Count > 0 && data.Values.ContainsKey("productTypeId"))
            {
                str = data.Values["productTypeId"].ToString();
            }

            return str;
        }

        public static bool IsProductCategory()
        {
            var result = false;

            var ht = new HttpContextWrapper(System.Web.HttpContext.Current);

            var data = RouteTable.Routes.GetRouteData(ht);

            if (data != null && data.Values.Count > 0 && data.Values.ContainsKey("productCategoryId"))
            {
                result = true;
            }

            return result;
        }

        public static bool IsSubCategory()
        {
            var result = false;

            var ht = new HttpContextWrapper(System.Web.HttpContext.Current);

            var data = RouteTable.Routes.GetRouteData(ht);

            if (data != null && data.Values.Count > 0 && data.Values.ContainsKey("subCategoryName"))
            {
                result = true;
            }

            return result;
        }

        public static string GetSubProductCategoryName()
        {
            var str = string.Empty;

            var ht = new HttpContextWrapper(System.Web.HttpContext.Current);

            var data = RouteTable.Routes.GetRouteData(ht);

            if (data != null && data.Values.Count > 0 && data.Values.ContainsKey("subCategoryName"))
            {
                str = data.Values["subCategoryName"].ToString();
            }

            return str;
        }

        public static List<ProductCategoryEntity> GetList(int parentId, int count = 15)
        {
            var lst = new ProductCategoryService().GetListActive(parentId, count);

            return lst;
        }

        public static List<ProductCategoryEntity> GetListRelated(int parentId, int count = 5, int cat_current = 0)
        {
            var lst = new ProductCategoryService().GetListActiveRelated(parentId, count, cat_current);

            return lst;
        }

        public static List<ProductCategoryEntity> GetListByType(int productTypeId)
        {
            var lst = new ProductCategoryService().GetListByType(productTypeId);

            return lst;
        }

        public static ProductCategoryEntity GetItem(int itemId)
        {
            var entity = new ProductCategoryService().GetItemFullDetail(itemId);

            return entity;
        }

      
        public static List<BannerEntity> GetListBannerCurrentCategory()
        {
            var lst = new List<BannerEntity>();
            var categoryId = GetProductCategoryId();

            if (!string.IsNullOrEmpty(categoryId))
            {
                lst = new ProductCategoryService().GetListLinkBanners(Convert.ToInt32(categoryId));

                if (!lst.Any())
                {
                    lst = new ProductCategoryService().GetListBanner(Convert.ToInt32(categoryId));
                }
            }
            else
            {
                var productTypeId = GetProductTypeId();

                if (!string.IsNullOrEmpty(productTypeId))
                {
                    lst = new ProductCategoryService().GetListLinkBanners(Convert.ToInt32(productTypeId));

                    if (!lst.Any())
                    {
                        lst = new ProductCategoryService().GetListBanner(Convert.ToInt32(productTypeId));
                    }
                }
            }

            return lst;
        }

        public static List<ProductCategoryEntity> GetListInCategory(int itemId)
        {
            var lst = new ProductCategoryService().GetListInCategory(itemId);

            return lst;
        }

        public ActionResult  catalogProduct()
        {

            //var lst = new ProductCategoryService().catalogProduct();
            return View();

        }
        public List<ProductCategoryEntity> catalogueProduct(int categoryID , bool Catalogue)
        {
            var lst = new List<ProductCategoryEntity>();
           
                lst = new ProductCategoryService().GetChilds(categoryID);

           
            
            var tmp = new List<ProductCategoryEntity>();

            if (Catalogue == true)
            {
                foreach (var item in lst)
                {
                    if (item.BrochureFileName != null)
                    {
                        tmp.Add(item);
                    }
                     var lst2 = new ProductCategoryService().GetChilds(item.ItemID);
                     foreach (var item2 in lst2)
                     {
                         if (item2.BrochureFileName != null)
                         {
                             tmp.Add(item2);
                         }
                     }
                }
                return tmp;
            }

            return lst;

        }

        public static List<int> GetAllCatIDParent(int ItemID)
        {
            List<int> lst = new List<int>();
            var item = new ProductCategoryService().GetItem(ItemID);
            lst.Add(item.ItemID);
            while (item.ParentID != 0)
            {
                //var parentItem = item.ParentCategory;
                item = item.ParentCategory;
                lst.Add(item.ItemID);
                
                //item = new ProductCategoryService().GetItem(parentItem.ItemID);
            }
            return lst;

        }
        public static List<ProductCategoryEntity> GetListSubCategory(int itemId)
        {
            var lst = new ProductCategoryService().GetListSubCategory(itemId);

            return lst;
        }

        public static bool IsAllowShowBanner()
        {
            if (IsProductCategory())
            {
                var itemId = Convert.ToInt32(GetProductCategoryId());

                var result = new ProductCategoryService().IsAllowShowBanner(itemId);

                return result;
            }

            return true;
        }

        public JsonResult GetListProductTypes()
        {
            var lst = new ProductCategoryService().GetListActive(0,10);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListCategory(int parentId)
        {
            var lst = new ProductCategoryService().GetListActive(parentId,10);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListCategoryQuery(int? typeId)
        {
            var parentId = 1;

            if (Request.UrlReferrer != null && HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["type"] != null)
            {
                parentId = Convert.ToInt32(HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["type"]);
            }
            else if (typeId != null)
            {
                parentId = Convert.ToInt16(typeId);
            }

            var lst = new ProductCategoryService().GetListActive(parentId,15);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 0)]
        public JsonResult GetColorDetail(int colorId)
        {
            var result = new ProductColorService().GetItem(colorId);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 0)]
        public JsonResult GetModelItem(int model)
        {
            var obj = _service.GetModelItem(model);

            return Json(obj, JsonRequestBehavior.AllowGet);
        }
    }
}

