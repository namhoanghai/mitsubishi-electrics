﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mitsubishi.UI.Models.Entities;
using Mitsubishi.UI.Models.Services;

namespace Mitsubishi.UI.Controllers
{
    public class CareerController : Controller
    {
        //
        // GET: /Publish/Career/
        public ActionResult Index()
        {
            if (Request.Url != null)
            {
                var url = Request.Url.AbsolutePath;

                var nav = new NavigationService().GetItem(url);

                if (nav != null)
                {
                    ViewBag.CurrentPage = nav.ItemID;
                }
            }

            var itemId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CAREERS"]);

            var entity = new ContentService().GetItem(itemId);

            return View(entity);
        }

        public static List<CareersEntity> GetList()
        {
            var lst = new CareersService().GetList();

            return lst;
        }
	}
}