﻿using System.Configuration;
using System.Web.Mvc;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Services;
using System.Text.RegularExpressions;

namespace Mitsubishi.UI.Controllers
{
    public class UtilsController : Controller
    {
        //
        // GET: /Utils/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SetLanguage(string languageId, string sessionName = GlobalConstants.SESS_LANGUAGE)
        {
            Session[sessionName] = languageId;

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public static string GetCurrentLanguage(string sessionName = GlobalConstants.SESS_LANGUAGE)
        {
            var result = ConfigurationManager.AppSettings["DEFAULT_LANGUAGE"];

            if (System.Web.HttpContext.Current.Session[sessionName] != null)
            {
                result = System.Web.HttpContext.Current.Session[sessionName].ToString();
            }

            return result;
        }
        public static string removeHtml(string input)
        {
            return Regex.Replace(input, "<.*?>", string.Empty);
        }

        public JsonResult GetLanguage(string sessionName)
        {
            var result = ConfigurationManager.AppSettings["DEFAULT_LANGUAGE"];

            if (Session[sessionName] != null)
            {
                result = Session[sessionName].ToString();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}