﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Mitsubishi.UI.Models;
using Mitsubishi.UI.Models.Services;
using Mitsubishi.UI.Models.Entities;
using PagedList; 

namespace Mitsubishi.UI.Controllers
{
    public class ContentController : Controller
    {
        //
        // GET: /Publish/Content/
        public ActionResult Index()
        {
            return View();
        }
        //tin tức
        public ActionResult NewsEvent(string sortOrder, string currentFilter, string searchString, int? page)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            if (Request.Url != null)
            {
                var url = Request.Url.AbsolutePath;

                var nav = new NavigationService().GetItem(url);

                if (nav != null)
                {
                    ViewBag.CurrentPage = nav.ItemID;
                }
            }

            var filter = ConfigurationManager.AppSettings["NEWS"];
            var lst = new ContentService().GetList(filter);


            int pageSize = 11;
            int pageNumber = (page ?? 1);
            return View(lst.ToPagedList(pageNumber, pageSize));

            //return View(lst);
            
        }
        //trang sự kiện
        public ActionResult Event(string sortOrder, string currentFilter, string searchString, int? page)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            if (Request.Url != null)
            {
                var url = Request.Url.AbsolutePath;

                var nav = new NavigationService().GetItem(url);

                if (nav != null)
                {
                    ViewBag.CurrentPage = nav.ItemID;
                }
            }

            var filter = ConfigurationManager.AppSettings["EVENTS"];
            var lst = new ContentService().GetList(filter);


            int pageSize = 11;
            int pageNumber = (page ?? 1);
            return View(lst.ToPagedList(pageNumber, pageSize));

            //return View(lst);

        }

        public List<ContentEntity> RelatedNews(int nid, string cats, int count, string orderby, string order, bool random)
        {

            var lst = new ContentService().GetListRelated(nid, cats, count, orderby, order, random);
           
            return lst;


        }

        public ActionResult NewsDetail()
        {
            var itemId = (string)RouteData.Values["itemId"];

            if (itemId == ConfigurationManager.AppSettings["ABOUT_MITSU"])
            {
                ViewBag.CurrentPage = GlobalConstants.NAV_ABOUT_MITSU;
            }
            else if (itemId == ConfigurationManager.AppSettings["ABOUT_US"])
            {
                ViewBag.CurrentPage = GlobalConstants.NAV_ABOUT_US;
            }
            else
            {
                ViewBag.CurrentPage = GlobalConstants.NAV_NEWS_EVENT;
            }

            var itemKey = (string)RouteData.Values["itemKey"];

            if (!string.IsNullOrEmpty(itemKey))
            {
                var Id = Convert.ToInt32(ConfigurationManager.AppSettings[itemKey]);

                var entity = new ContentService().GetItem(Id);

                if (Request.Url != null)
                {
                    var url = Request.Url.AbsolutePath;

                    var nav = new NavigationService().GetItem(url);

                    if (nav != null)
                    {
                        ViewBag.CurrentPage = nav.ItemID;
                    }
                }

                return View(entity);
            }

            var item = new ContentService().GetItem(Convert.ToInt32(itemId));

            return View(item);
        }

      public ContentEntity GetFirstItemNews(string Ids = "2,3")
        {
          //tin tức, sự kiện
            var entity = new ContentService().GetFirstItem(Ids);

            return entity;
        }

        public static string GetContentID()
        {
            var str = string.Empty;

            var ht = new HttpContextWrapper(System.Web.HttpContext.Current);

            var data = RouteTable.Routes.GetRouteData(ht);


            if (data != null && data.Values.Count > 0 && data.Values.ContainsKey("itemId"))
            {
                str = data.Values["itemId"].ToString();
            }

            return str;
        }

        public ActionResult AboutUs()
        {
            var itemKey = (string)RouteData.Values["itemKey"];

            if (!string.IsNullOrEmpty(itemKey))
            {
                var itemId = Convert.ToInt32(ConfigurationManager.AppSettings[itemKey]);

                var entity = new ContentService().GetItem(itemId);

                if (Request.Url != null)
                {
                    var url = Request.Url.AbsolutePath;

                    var nav = new NavigationService().GetItem(url);

                    if (nav != null)
                    {
                        ViewBag.CurrentPage = nav.ItemID;
                    }
                }

                return View("AboutUs", entity);
            }

            return View("Error");
        }
	}
}