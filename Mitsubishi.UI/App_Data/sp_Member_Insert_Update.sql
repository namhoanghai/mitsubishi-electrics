USE [Mitsubishi]
GO
/****** Object:  StoredProcedure [dbo].[sp_Member_Insert_Update]    Script Date: 12/16/2014 23:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_Member_Insert_Update]
(
	@ItemID int,
	@UserName nvarchar(30),
	@Password varchar(100),
	@FullName nvarchar(255),
	@IsAdmin bit,
	@IsPublisher bit
)
AS
BEGIN
	IF (@ItemID != 0)
		UPDATE Member SET UserName = @UserName, Password = @Password, FullName = @FullName, ModifiedDate = GETDATE(), IsAdmin = @IsAdmin, IsPublisher = @IsPublisher 
		WHERE ItemID = @ItemID
	ELSE
	BEGIN
		SET @ItemID = (SELECT LastID FROM LastIDs WHERE TableName = 'Member')
		
		INSERT INTO Member
		(
			ItemID, UserName, Password, FullName, CreatedDate, ModifiedDate, IsAdmin, IsPublisher
		)
		VALUES
		(
			@ItemID, @UserName, @Password, @FullName, GETDATE(), GETDATE(), @IsAdmin, @IsPublisher
		)
		
		SET @ItemID = @ItemID + 1;

		UPDATE LastIDs SET LastID = @ItemID WHERE TableName = 'Member' 
	END
		
END



