USE [Mitsubishi]
GO
/****** Object:  StoredProcedure [dbo].[sp_Inquiry_Save]    Script Date: 12/20/2014 11:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Inquiry_Save]
(
	@ItemID int,
	@Name nvarchar(100),
	@Company nvarchar(255),
	@Comment nvarchar(MAX),
	@Address nvarchar(255),
	@City nvarchar(100),
	@Country nvarchar(100),
	@Phone varchar(30),
	@Email varchar(100)
)
AS
BEGIN
	IF (@ItemID != 0)
		UPDATE Inquiry SET Name = @Name, Company = @Company, Comment = @Comment, [Address] = @Address,
		City = @City, Country = @Country, Phone = @Phone, Email = @Email, CreatedDate = GETDATE()
		WHERE ItemID = @ItemID
	ELSE
	BEGIN
		SET @ItemID = (SELECT LastID FROM LastIDs WHERE TableName = 'Inquiry')
		
		INSERT INTO Inquiry
		(
			ItemID, Name, Company, Comment, [Address], City, Country, Phone, Email, CreatedDate
		)
		VALUES
		(
			@ItemID, @Name, @Company, @Comment, @Address, @City, @Country, @Phone, @Email, GETDATE()
		)
		
		SET @ItemID = @ItemID + 1;

		UPDATE LastIDs SET LastID = @ItemID WHERE TableName = 'Inquiry' 
	END
		
END



