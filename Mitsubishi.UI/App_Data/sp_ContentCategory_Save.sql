USE [Mitsubishi]
GO
/****** Object:  StoredProcedure [dbo].[sp_ContentCategory_Save]    Script Date: 12/16/2014 23:37:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_ContentCategory_Save]
(
	@ItemID int,
	@Title nvarchar(255),
	@TitleEng nvarchar(255),
	@IsActive bit,
	@SortOrder int,
	@ParentID int
)
AS
BEGIN
	IF (@ItemID != 0)
		UPDATE ContentCategory SET Title = @Title, TitleEng = @TitleEng, IsActive = @IsActive, ModifiedDate = GETDATE(), SortOrder = @SortOrder, ParentID = @ParentID 
		WHERE ItemID = @ItemID
	ELSE
	BEGIN
		SET @ItemID = (SELECT LastID FROM LastIDs WHERE TableName = 'ContentCategory')
		
		INSERT INTO ContentCategory
		(
			ItemID, Title, TitleEng, SortOrder, IsActive, CreatedDate, ModifiedDate, ParentID
		)
		VALUES
		(
			@ItemID, @Title, @TitleEng, @SortOrder, @IsActive, GETDATE(), GETDATE(), @ParentID
		)
		
		SET @ItemID = @ItemID + 1;

		UPDATE LastIDs SET LastID = @ItemID WHERE TableName = 'ContentCategory' 
	END
		
END



