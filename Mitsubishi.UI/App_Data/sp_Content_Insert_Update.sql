USE [Mitsubishi]
GO
/****** Object:  StoredProcedure [dbo].[sp_Content_Insert_Update]    Script Date: 12/16/2014 23:37:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_Content_Insert_Update]
(
	@ItemID int,
	@Title nvarchar(255),
	@TitleEN nvarchar(255),
	@Description ntext,
	@DescriptionEN ntext,
	@Content ntext,
	@ContentEN ntext,
	@Thumbnail nvarchar(255),
	@IsActive bit,
	@SortOrder int,
	@CategoryID int
)
AS
BEGIN
	IF (@ItemID != 0)
		UPDATE Content SET Title = @Title, TitleEN= @TitleEN, IsActive = @IsActive, ModifiedDate = GETDATE(), SortOrder = @SortOrder, Thumbnail = @Thumbnail,
		Description = @Description, DescriptionEN = @DescriptionEN, Content = @Content, ContentEN = @ContentEN, CategoryID = @CategoryID
		WHERE ItemID = @ItemID
	ELSE
	BEGIN
		SET @ItemID = (SELECT LastID FROM LastIDs WHERE TableName = 'Content')
		
		INSERT INTO Content
		(
			ItemID, CategoryID, Title, TitleEN, Description, DescriptionEN, Content, ContentEN, Thumbnail, SortOrder, IsActive, CreatedDate, ModifiedDate
		)
		VALUES
		(
			@ItemID, @CategoryID, @Title, @TitleEN, @Description, @DescriptionEN, @Content, @ContentEN, @Thumbnail, @SortOrder, @IsActive, GETDATE(), GETDATE()
		)
		
		SET @ItemID = @ItemID + 1;

		UPDATE LastIDs SET LastID = @ItemID WHERE TableName = 'Content' 
	END
		
END



