USE [Mitsubishi]
GO
/****** Object:  StoredProcedure [dbo].[sp_Careers_Save]    Script Date: 12/20/2014 11:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Careers_Save]
(
	@ItemID int,
	@Title nvarchar(255),
	@TitleEN nvarchar(255),
	@Description nvarchar(MAX),
	@DescriptionEN nvarchar(MAX),
	@SortOrder int,
	@IsActive bit
)
AS
BEGIN
	IF (@ItemID != 0)
		UPDATE Careers SET Title = @Title, TitleEN = @TitleEN, Description = @Description, 
		DescriptionEN = @DescriptionEN, SortOrder = @SortOrder, IsActive = @IsActive, CreatedDate = GETDATE(), ModifiedDate = GETDATE()
		WHERE ItemID = @ItemID
	ELSE
	BEGIN
		SET @ItemID = (SELECT LastID FROM LastIDs WHERE TableName = 'Careers')
		
		INSERT INTO Careers
		(
			ItemID, Title, TitleEN, Description, DescriptionEN, SortOrder, IsActive, CreatedDate, ModifiedDate
		)
		VALUES
		(
			@ItemID, @Title, @TitleEN, @Description, @DescriptionEN, @SortOrder, @IsActive, GETDATE(), GETDATE()
		)
		
		SET @ItemID = @ItemID + 1;

		UPDATE LastIDs SET LastID = @ItemID WHERE TableName = 'Careers' 
	END
		
END



