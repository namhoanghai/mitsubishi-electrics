USE [Mitsubishi]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductCategory_Insert_Update]    Script Date: 12/16/2014 23:37:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_ProductCategory_Insert_Update]
(
	@ItemID int,
	@TitleEng nvarchar(255),
	@Title nvarchar(255),
	@IsActive bit,
	@ParentID int,
	@ImageName nvarchar(255),
	@SortOrder int,
	@LongDescription ntext,
	@ShortDescription ntext,
	@LongDescriptionEng ntext,
	@ShortDescriptionEng ntext,
	@ProductTypeId int,
	@Banners nvarchar(255)
)
AS
BEGIN
	IF (@ItemID != 0)
		UPDATE ProductCategory SET Title = @Title, TitleEng = @TitleEng, IsActive = @IsActive, ModifiedDate = GETDATE(), ProductTypeId = @ProductTypeId,
		ParentID = @ParentID, ImageName = @ImageName, SortOrder = @SortOrder, LongDescription = @LongDescription, ShortDescription = @ShortDescription,
		LongDescriptionEng = @LongDescriptionEng, ShortDescriptionEng = @ShortDescriptionEng, Banners = @Banners
		WHERE ItemID = @ItemID
	ELSE
	BEGIN
		SET @ItemID = (SELECT LastID FROM LastIDs WHERE TableName = 'ProductCategory')
		
		INSERT INTO ProductCategory
		(
			ItemID, TitleEng, Title, ParentID, ImageName, SortOrder, IsActive, CreatedDate, ModifiedDate, LongDescriptionEng, ShortDescriptionEng, LongDescription, ShortDescription, ProductTypeId, Banners
		)
		VALUES
		(
			@ItemID, @TitleEng, @Title, @ParentID, @ImageName, @SortOrder, @IsActive, GETDATE(), GETDATE(), @LongDescriptionEng, @ShortDescriptionEng, @LongDescription, @ShortDescription, @ProductTypeId, @Banners
		)
		
		SET @ItemID = @ItemID + 1;

		UPDATE LastIDs SET LastID = @ItemID WHERE TableName = 'ProductCategory' 
	END
		
END



