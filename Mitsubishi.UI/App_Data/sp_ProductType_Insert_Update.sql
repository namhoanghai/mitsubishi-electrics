USE [Mitsubishi]
GO
/****** Object:  StoredProcedure [dbo].[sp_ProductType_Insert_Update]    Script Date: 12/16/2014 23:37:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_ProductType_Insert_Update]
(
	@ItemID int,
	@Name nvarchar(255),
	@NameEng nvarchar(255),
	@IsActive bit,
	@ImageName nvarchar(255)
)
AS
BEGIN
	IF (@ItemID != 0)
		UPDATE ProductType SET Name = @Name, NameEng = @NameEng, IsActive = @IsActive, ModifiedDate = GETDATE(), ImageName = @ImageName 
		WHERE ItemID = @ItemID
	ELSE
	BEGIN
		SET @ItemID = (SELECT LastID FROM LastIDs WHERE TableName = 'ProductType')
		
		INSERT INTO ProductType
		(
			ItemID, NameEng, Name, IsActive, CreatedDate, ModifiedDate, ImageName
		)
		VALUES
		(
			@ItemID, @NameEng, @Name, @IsActive, GETDATE(), GETDATE(), @ImageName
		)
		
		SET @ItemID = @ItemID + 1;

		UPDATE LastIDs SET LastID = @ItemID WHERE TableName = 'ProductType' 
	END
		
END



